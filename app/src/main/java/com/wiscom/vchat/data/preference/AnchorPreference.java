package com.wiscom.vchat.data.preference;

import android.content.Context;

import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.model.HostInfo;
import com.wiscom.library.util.SharedPreferenceUtil;

/**
 * 保存播主账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class AnchorPreference {


    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private AnchorPreference() {
    }

    public static final String NAME = "vchat_pre_anchor";
    public static final String ANCHOR_PRICE = "vchat_anchor_price";
    public static final String ANCHOR_INCOME = "vchat_anchor_income";
    public static final String ANCHOR_BALANCE = "vchat_anchor_IncomeBalance";

    public static void saveHostInfo(HostInfo hostInfo) {
        if (null != hostInfo) {
            setPrice(hostInfo.getPrice());
            setIncome(hostInfo.getTotalIncome());
            setCurrentBalance(hostInfo.getIncomeBalance());

        }
    }


    /**
     * 设置主播当前价格
     *
     * @param price
     */
    public static void setPrice(float price) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_PRICE, String.valueOf(price));
    }

    /**
     * @return 主播的当前价格
     */
    public static String getPrice() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_PRICE, "0");
    }

    /**
     * 保存用户账户累计收入金币数
     *
     * @param income
     */
    public static void setIncome(float income) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_INCOME, String.valueOf(income));
    }

    /**
     * @return 用户账户累计收入金币数
     */
    public static String getIncome() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_INCOME, "0");
    }

    /**
     * 保存用户账户当前金币数
     *
     * @param
     */
    public static void setCurrentBalance(float incomeBalance) {
        SharedPreferenceUtil.setStringValue(mContext, NAME, ANCHOR_BALANCE, String.valueOf(incomeBalance));
    }

    /**
     * @return 用户账户当前金币数
     */
    public static String getCurrentBalance() {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, ANCHOR_BALANCE, "0");
    }

}
