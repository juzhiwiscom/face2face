package com.wiscom.vchat.data.model;

import java.util.List;

/**
 * Created by tianzhentao on 2018/1/3.
 */

public class FriendListModel extends BaseModel {
    private List<UserBase> listUser;

    public List<UserBase> getListUser() {
        return listUser;
    }

    public void setListUser(List<UserBase> listUser) {
        this.listUser = listUser;
    }
}
