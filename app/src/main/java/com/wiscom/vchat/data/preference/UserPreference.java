package com.wiscom.vchat.data.preference;

import android.content.Context;
import android.text.TextUtils;

import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.library.util.SharedPreferenceUtil;

/**
 * 保存用户相关信息
 * Created by zhangdroid on 2017/5/31.
 */
public class UserPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private UserPreference() {
    }

    public static final String NAME = "vchat_pre_user";
    /**
     * 是否激活过
     */
    private static final String ACTIVATION = "vchat_activation";
    /**
     * 是否显示计费提示
     */
    private static final String IS_SHOW_CHARGE = "vchat_is_show_charge";

    /**
     * 是否播主
     */
    private static final String IS_ANCHOR = "vchat_is_anchor";
    /**
     * 账号
     */
    private static final String ACCOUNT = "vchat_account";
    /**
     * 密码
     */
    private static final String PASSWORD = "vchat_password";
    /**
     * 用户ID
     */
    private static final String USER_ID = "vchat_user_id";
    /**
     * 用户状态(1、空闲 2、在聊 3、勿扰)
     */
    private static final String STATUS = "vchat_status";
    /**
     * 头像-原图
     */
    private static final String IMG_ORIGINAL = "vchat_img_original";
    /**
     * 头像-中图
     */
    private static final String IMG_MIDDLE = "vchat_img_middle";
    /**
     * 头像-小图
     */
    private static final String IMG_SMALL = "vchat_img_small";
    /**
     * 昵称
     */
    private static final String NICKNAME = "vchat_nickname";
    /**
     * 年龄
     */
    private static final String AGE = "vchat_age";
    /**
     * 性别
     */
    private static final String GENDER = "vchat_gender";
    /**
     * 自我介绍
     */
    private static final String INTRODUCATION = "vchat_introducation";
    /**
     * 国家
     */
    private static final String COUNTRY = "vchat_country";
    /**
     * 国家
     */
    private static final String COUNTRY_ID = "vchat_country_id";
    /**
     * 国家
     */
    private static final String COUNTRY_NUM = "vchat_country_num";
    /**
     * 州
     */
    private static final String STATE = "vchat_state";
    /**
     * 州
     */
    private static final String STATE_ID = "vchat_state_id";
    /**
     * 城市
     */
    private static final String CITY = "vchat_city";
    /**
     * 城市
     */
    private static final String PROVINCE = "vchat_province";
    /**
     * 渠道号
     */
    private static final String FID = "vchat_fid";
    /**
     * 注册国家
     */
    private static final String REGISTER_COUNTRY = "vchat_register_country";
    /**
     * 支持的语言
     */
    private static final String SPOKEN_LANGUAGE = "vchat_spoken_language";
    /**
     * 注册昵称
     */
    private static final String REGISTER_NAME= "vchat_register_nickname";

    // ********************************************* 保存标记 *********************************************

    public static void activation() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, ACTIVATION, true);
    }

    public static boolean isActived() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, ACTIVATION, false);
    }

    /**
     * 不再显示计费提示
     */
    public static void hideCharge() {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_SHOW_CHARGE, false);
    }

    public static boolean isShowCharge() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_SHOW_CHARGE, true);
    }

    // ********************************************* 保存/获取用户资料项信息 *********************************************

    private static void saveIfNotEmpty(String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            SharedPreferenceUtil.setStringValue(mContext, NAME, key, value);
        }
    }

    private static String getValue(String key) {
        return SharedPreferenceUtil.getStringValue(mContext, NAME, key, null);
    }

    public static void saveUserInfo(UserBase userBase) {
        if (null != userBase) {
            setAccount(userBase.getAccount());
            setPassword(userBase.getPassword());
            setIsAnchor(userBase.getUserType());
            setId(String.valueOf(userBase.getGuid()));
            setStatus(userBase.getStatus());
            setOriginalImage(userBase.getIconUrl());
            setMiddleImage(userBase.getIconUrlMiddle());
            setSmallImage(userBase.getIconUrlMininum());
            setNickname(decode(userBase.getNickName()));
            setAge(String.valueOf(userBase.getAge()));
            setGender(String.valueOf(userBase.getGender()));
            setIntroducation(userBase.getOwnWords());
            setCountryNum(userBase.getCountry());
            setCountry(ParamsUtils.getCountryValues(userBase.getCountry()));
            setState(ParamsUtils.getStateValue(userBase.getState()));
            setCity(userBase.getCity());
            setSpokenLanguage(userBase.getSpokenLanguage());
        }
    }


    public static void setAccount(String value) {
        saveIfNotEmpty(ACCOUNT, value);
    }

    public static String getAccount() {
        return getValue(ACCOUNT);
    }

    public static void setPassword(String value) {
        saveIfNotEmpty(PASSWORD, value);
    }

    public static String getPassword() {
        return getValue(PASSWORD);
    }

    public static void setIsAnchor(int userType) {
        SharedPreferenceUtil.setBooleanValue(mContext, NAME, IS_ANCHOR, userType == 1);
    }

    public static boolean isAnchor() {
        return SharedPreferenceUtil.getBooleanValue(mContext, NAME, IS_ANCHOR, false);
    }

    public static void setId(String value) {
        saveIfNotEmpty(USER_ID, value);
    }

    public static String getId() {
        return getValue(USER_ID);
    }

    public static void setStatus(String value) {
        saveIfNotEmpty(STATUS, value);
    }

    public static String getStatus() {
        return getValue(STATUS);
    }

    public static void setOriginalImage(String value) {
        saveIfNotEmpty(IMG_ORIGINAL, value);
    }

    public static String getOriginalImage() {
        return getValue(IMG_ORIGINAL);
    }

    public static void setMiddleImage(String value) {
        saveIfNotEmpty(IMG_MIDDLE, value);
    }

    public static String getMiddleImage() {
        return getValue(IMG_MIDDLE);
    }

    public static void setSmallImage(String value) {
        saveIfNotEmpty(IMG_SMALL, value);
    }

    public static String getSmallImage() {
        return getValue(IMG_SMALL);
    }

    public static void setNickname(String value) {
        saveIfNotEmpty(NICKNAME, value);
    }

    public static String getNickname() {
        return getValue(NICKNAME);
    }

    public static void setAge(String value) {
        saveIfNotEmpty(AGE, value);
    }

    public static String getAge() {
        return getValue(AGE);
    }
    public static String getGender() {
        return getValue(GENDER);
    }

    public static void setGender(String value) {
        saveIfNotEmpty(GENDER, value);
    }

    public static boolean isMale() {
        return "0".equals(SharedPreferenceUtil.getStringValue(mContext, NAME, GENDER, "0"));
    }

    public static void setIntroducation(String value) {
        saveIfNotEmpty(INTRODUCATION, value);
    }

    public static String getIntroducation() {
        return getValue(INTRODUCATION);
    }

    public static void setCountry(String value) {
        saveIfNotEmpty(COUNTRY, value);
    }
    public static String getCountry() {
        return getValue(COUNTRY);
    }
    public static void setCountry_id(String value) {
        saveIfNotEmpty(COUNTRY_ID, value);
    }
    public static String getCountry_id() {
        return getValue(COUNTRY_ID);
    }
    private static void setCountryNum(String country) {
        saveIfNotEmpty(COUNTRY_NUM, country);
    }
    public static String getCountry_num() {
        return getValue(COUNTRY_NUM);
    }

    public static void setState(String value) {
        saveIfNotEmpty(STATE, value);
    }

    public static String getState() {
        return getValue(STATE);
    }

    public static void setCity(String value) {
        saveIfNotEmpty(CITY, value);
    }

    public static String getCity() {
        return getValue(CITY);
    }

    public static void setSpokenLanguage(String value) {
        saveIfNotEmpty(SPOKEN_LANGUAGE, value);
    }

    public static String getSpokenLanguage() {
        return getValue(SPOKEN_LANGUAGE);
    }
    public static void setProvince(String value) {
        saveIfNotEmpty(PROVINCE, value);
    }
    public static String getProvince() {
        return getValue(PROVINCE);
    }
    public static void setFid(String value) {
        saveIfNotEmpty(FID, value);
    }
    public static String getFid() {
        return getValue(FID);
    }

    public static void setRegisterCountry(String value) {
        saveIfNotEmpty(REGISTER_COUNTRY, value);
    }

    public static String getRegisterCountry() {
        return getValue(REGISTER_COUNTRY);
    }
    public static void setRegisterName(String value) {
        saveIfNotEmpty(REGISTER_NAME, value);
    }

    public static String getRegisterName() {
        return getValue(REGISTER_NAME);
    }

    /**
     * 转中文
     * @param unicodeStr
     * @return
     */
    public static String decode(String unicodeStr) {
        if (unicodeStr == null) {
            return null;
        }
        StringBuffer retBuf = new StringBuffer();
        int maxLoop = unicodeStr.length();
        for (int i = 0; i < maxLoop; i++) {
            if (unicodeStr.charAt(i) == '\\') {
                if ((i < maxLoop - 5) && ((unicodeStr.charAt(i + 1) == 'u') || (unicodeStr.charAt(i + 1) == 'U')))
                    try {
                        retBuf.append((char) Integer.parseInt(unicodeStr.substring(i + 2, i + 6), 16));
                        i += 5;
                    } catch (NumberFormatException localNumberFormatException) {
                        retBuf.append(unicodeStr.charAt(i));
                    }
                else
                    retBuf.append(unicodeStr.charAt(i));
            } else {
                retBuf.append(unicodeStr.charAt(i));
            }
        }
        return retBuf.toString();
    }
}
