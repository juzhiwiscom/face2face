package com.wiscom.vchat.data.model;

/**
 * 发起/接受视频呼叫结果对象
 * Created by zhangdroid on 2017/6/23.
 */
public class RequestVideoCall extends BaseModel {
    private UserBase remoteMatchUser;//匹配到的用户信息
    private String beanStatus;//{钻石状态字段，针对条件搜索}, -1 钻石不足 1 钻石充足
    private String matchResultType;//1 真实匹配成功 2 虚拟视频返回 -1 结果为空 等待
    private String matchVideo;//{字符串类型，虚拟视频URL} 若匹配到虚拟视频，返回地址
    private int wait_time;//{客户端未匹配到等待时长}, int 整型 默认值：5
    private int matchTimes;//{客户端已调用匹配次数,下次请求时带入参数回传服务器}, int 整型

    public String getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(String beanStatus) {
        this.beanStatus = beanStatus;
    }

    public UserBase getRemoteMatchUser() {
        return remoteMatchUser;
    }

    public void setRemoteMatchUser(UserBase remoteMatchUser) {
        this.remoteMatchUser = remoteMatchUser;
    }

    public String getMatchResultType() {
        return matchResultType;
    }

    public void setMatchResultType(String matchResultType) {
        this.matchResultType = matchResultType;
    }

    public String getMatchVideo() {
        return matchVideo;
    }

    public void setMatchVideo(String matchVideo) {
        this.matchVideo = matchVideo;
    }

    public int getWait_time() {
        return wait_time;
    }

    public void setWait_time(int wait_time) {
        this.wait_time = wait_time;
    }

    public int getMatchTimes() {
        return matchTimes;
    }

    public void setMatchTimes(int matchTimes) {
        this.matchTimes = matchTimes;
    }
}
