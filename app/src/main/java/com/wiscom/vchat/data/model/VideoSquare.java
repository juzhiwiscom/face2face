package com.wiscom.vchat.data.model;

/**
 * Created by tianzhentao on 2018/4/9.
 */

public class VideoSquare {
    private UserBase userBase;
    private int isFollow;// 是否已关注
    private int isBlock;// 是否已拉黑
    private UserVideoShow tUserVideoShow;//视频信息
    private int isPraise;// 是否点赞 0 未点赞 1 点赞

    public int getIsPraise() {
        return isPraise;
    }

    public void setIsPraise(int isPraise) {
        this.isPraise = isPraise;
    }

    public UserVideoShow getUserVideoShow() {
        return tUserVideoShow;
    }

    public void setUserVideoShow(UserVideoShow userVideoShow) {
        this.tUserVideoShow = userVideoShow;
    }

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public int getIsBlock() {
        return isBlock;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }
}
