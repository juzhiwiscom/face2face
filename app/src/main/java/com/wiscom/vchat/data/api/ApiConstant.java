package com.wiscom.vchat.data.api;

/**
 * 该类保存项目中用到的所有接口定义常量
 * <li>1、该类中配置所有接口定义</li>
 * <li>2、接口配置时请放入相应模块注释下方，并写明接口作用，统一管理</li>
 * <li>3、接口命名规则请参照：URL_模块名_接口名</li>
 * Created by zhangdroid on 2017/5/17.
 */
public final class ApiConstant {

    // ***************************** 测试服 ***************************

    public static final String URL_BASE = "http://121.42.144.2:9092/apollo";
//    public static final String URL_BASE = "http://192.168.199.128:8080/apollo";//大伟本地
//    public static final String URL_BASE = "http://192.168.199.214:8090/apollo"; // 希磊本地
//    public static final String URL_BASE = "http://47.88.78.64:9092/apolloplatform"; // 希磊本地

//public static final String PAYMENT = "http://121.42.144.2:9093/apollopaycenter/totalBack/getGooglePay.pay";




    // ***************************** 正式服 ***************************
// public static final String URL_BASE = "http://android.ulive.site:9092/apolloplatform";

    // ***************************** 支付 ***************************

    public static final String PAYMENT = "http://pay.ulive.site:9093/apollopaycenter/totalBack/getGooglePay.pay";

    // ***************************** 协议 ***************************

    /**
     * 用户协议
     */
    public static final String URL_AGREEMENT_USER = "http://www.easyplay.site/apollo/terms_cn.htm";
    /**
     * 隐私协议
     */
    public static final String URL_AGREEMENT_PRIVACY = "http://www.easyplay.site/apollo/private_cn.htm";
    /**
     * 播主协议
     */
    public static final String URL_AGREEMENT_ANCHOR = "http://www.easyplay.site/apollo/hosttermcn.html";
    /**
     * 提现协议
     */
    public static final String URL_AGREEMENT_WITHDRAW = "http://www.easyplay.site/apollo/withdrawtermcn.html";

    // ***************************** 激活/注册/登陆 ***************************

    /**
     * 激活
     */
    public static final String URL_ACTIVATION = URL_BASE + "/sys/clientactivation.json";
    /**
     * 注册
     */
    public static final String URL_REGISTER = URL_BASE + "/user/register.json";
    /**
     * 注册(带有图片和邮箱)
     */
    public static final String URL_PHOTO_REGISTER = URL_BASE + "/user/photoregister.json";
    /**
     * 登录
     */
    public static final String URL_LOGIN = URL_BASE + "/user/login.json";
    /**
     * 找回密码
     */
    public static final String URL_FIND_PWD = URL_BASE + "/user/getpwd.json";
    /**
     * 位置信息
     */
    public static final String URL_LOCATION = URL_BASE + "/sys/uploadlocation.json";


    // ***************************** 首页 ***************************

    /**
     * 女神（收入排行）
     */
    public static final String URL_HOMEPAGE_INCOME = URL_BASE + "/search/byincome.json";
    /**
     * 活跃播主/聊友（操作时间排行）
     */
    public static final String URL_HOMEPAGE_ACTIVE = URL_BASE + "/search/byactive.json";
    /**
     * 新晋聊友（注册时间排行）/新人（申请播主时间排行）
     */
    public static final String URL_HOMEPAGE_NEW = URL_BASE + "/search/bytime.json";

    // ***************************** 消息 ***************************

    /**
     * 视频通话记录
     */
    public static final String URL_MESSAGE_VIDEO_RECORD = URL_BASE + "/chat/getcallrecords.json";
    /**
     * 聊天支付拦截（文字）
     */
    public static final String URL_MESSAGE_INTERRUPT_TEXT = URL_BASE + "/msg/sendtextmessage.json";
    /**
     * 聊天支付拦截（语音）
     */
    public static final String URL_MESSAGE_INTERRUPT_VOICE = URL_BASE + "/msg/sendaudiomessage.json";
    /**
     * 聊天支付拦截（图片）
     */
    public static final String URL_MESSAGE_INTERRUPT_IMAGE = URL_BASE + "/msg/sendimagemessage.json";

    // ***************************** 关注 ***************************

    /**
     * 关注
     */
    public static final String URL_FOLLOW = URL_BASE + "/follow/dofollow.json";
    /**
     * 取消关注
     */
    public static final String URL_UN_FOLLOW = URL_BASE + "/follow/unfollow.json";
    /**
     * 关注列表
     */
    public static final String URL_FOLLOW_LIST = URL_BASE + "/follow/getfollows.json";
    /**
     * 是否关注某个用户
     */
    public static final String URL_IS_FOLLOW = URL_BASE + "/follow/isfollow.json";

    // ***************************** 好友 ***************************

    /**
     * 获得个人信息
     */
    public static final String URL_GET_FRIEND_LIST = URL_BASE + "/friend/friends.json";
    /**
     * 根据账户查找用户
     */
    public static final String URL_GET_FRIEND_FROM_ID = URL_BASE + "/search/vbyaccount.json";
    /**
     * 获取申请人
     */
    public static final String URL_GET_FRIEND_APPLY = URL_BASE + "/friend/applicant.json";

    /**
     * 申请添加好友
     */
    public static final String APPLY_ADD_FRIEND = URL_BASE + "/friend/apply.json";
    /**
     * 同意添加好友
     */
    public static final String AGREE_ADD_FRIEND = URL_BASE + "/friend/agree.json";

    /**
     * 拒绝添加好友
     */
    public static final String REFUSE_ADD_FRIEND = URL_BASE + "/friend/disagree.json";
    /**
     * 添加三个好友后支付
     */
    public static final String FRIEND_APPLY_PAY = URL_BASE + "/friend/applypay.json";

    // ***************************** 个人中心 ***************************

    /**
     * 获得个人信息
     */
    public static final String URL_PERSON_MY_INFO = URL_BASE + "/setting/myinfo.json";

    /**
     * 获得支付渠道信息
     */
    public static final String URL_GET_PAY_WAY = URL_BASE + "/pay/payway.json";
    /**
     * 修改个人资料
     */
    public static final String URL_UPLOAD_MYINFO = URL_BASE + "/setting/updatemyinfo.json";
    /**
     * 上传头像或者图片
     */
    public static final String URL_UPLOAD_PHOTO = URL_BASE + "/photo/uploadimg.json";
    /**
     * 提现记录
     */
    public static final String URL_WITHDRAW_RECORD= URL_BASE + "/pay/getwithdrawrecords.json";
    /**
     * 提现申请
     */
    public static final String URL_WITHDRAW_APPLY = URL_BASE + "/pay/withdraw.json";

    // ***************************** 公用 ***************************

    /**
     * 获取某个用户详情
     */
    public static final String URL_GET_USER_INFO = URL_BASE + "/space/userinfo.json";
    /**
     * 修改用户状态
     */
    public static final String URL_MODIFFY_USER_STATUS = URL_BASE + "/setting/modifyuserstatus.json";

    // ***************************** 视频 ***************************

    /**
     * 获取声网信令系统token
     */
    public static final String URL_GET_TOKEN = URL_BASE + "/sys/getsignalingkey.json";
    /**
     * 获取声网channel key
     */
    public static final String URL_GET_CHANNEL_KEY = URL_BASE + "/sys/getchannelkey.json";
    /**
     * 发起/接受视频呼叫前通知服务器
     */
    public static final String URL_VIDEO_CALL = URL_BASE + "/chat/callcreate.json";
    /**
     * 视频接通后心跳（50秒）
     */
    public static final String URL_VIDEO_HEART_BEAT = URL_BASE + "/chat/callheartbeat.json";
    /**
     * 视频结束后通知服务器
     */
    public static final String URL_VIDEO_STOP = URL_BASE + "/chat/callclose.json";
 /**
  * 获取系统视频用户
  */
 public static final String URL_SYSYTEM_USER = URL_BASE + "/match/matchvideo.json";

    // ***************************** 视频上传与主播认证 ***************************

    /**
     * 获取facebook的账号
     */
    public static final String URL_GET_FACEBOOK_ACCOUNT = URL_BASE + "/sys/init.json";
    /**
     * 提交审核
     */
    public static final String URL_SUBMIT = URL_BASE + "/setting/submitapply.json";
    /**
     * 上传视频秀
     */
    public static final String URL_UPLOAD_VEDIO = URL_BASE + "/setting/uploadvideoshow.json";
    /**
     * 调整主播价格
     * **/
    public static final String URL_UPDATA_PRICE= URL_BASE + "/setting/updatehostprice.json";
    /**
     * 推荐秀：最新视频
     */
    public static final String URL_RECOMMEND_NEW_JOIN= URL_BASE + "/videoshow/bynew.json";
    /**
     * 推荐秀：最热视频
     */
    public static final String URL_RECOMMEND_HOT_JOIN= URL_BASE + "/videoshow/byhot.json";
   /**
    * 视频点赞
    * **/
   public static final String URL_VIDEPO_PRAISE= URL_BASE + "/videoshow/prise.json";

    // ***************************** 视频接口 ***************************
    /**
     * 发起视频请求，从服务器获取另一个用户的信息，组成频道
     */
    public static final String URL_QUEST_VIDEO_CALL = URL_BASE + "/search/byrandom.json";
    /**
     * 随机视频匹配
     * **/
    public static final String RANDOM_VEDIO = URL_BASE + "/match/matchrandom.json";
    /**
     * 随机视频通话中切换
     * **/
    public static final String RANDOM_VEDIO_SWITCH = URL_BASE + "/match/matchswitch.json";
    /**
     * 随机匹配成功建立连接
     * **/
    public static final String VEDIO_SUCCESS = URL_BASE + "/match/matchcreate.json";
    /**
     * 系统用户视频成功建立连接
     * **/
    public static final String SYSTEM_VIDEO_SUCCESS = URL_BASE + "/match/saverecord.json";
    /**
     * 视频接通后心跳（10秒）
     */
    public static final String VIDEO_HEART_BEAT = URL_BASE + "/match/matchactive.json";
    /**
     * 退出随机匹配
     * **/
    public static final String EXIT_MATCH = URL_BASE + "/match/matchexit.json";
    /**
     * 埋点：统计不同位置进入支付界面的次数
     */
    public static final String USER_ACTIVE_TAG = URL_BASE + "/sys/useractivetag.json";
    /**
     * 举报
     **/
    public static final String URL_REPORT = URL_BASE + "/space/report.json";
    /**
     * 修改密码
     **/
    public static final String URL_CHANGE_PASSWORD = URL_BASE + "/setting/modifypwd.json";

 // ***************************** 视频接口 ***************************

   /**
    * 礼物字典
    */
   public static final String URL_GIFTS_DIC = URL_BASE + "/gift/getgifts.json";

}
