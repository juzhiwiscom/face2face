package com.wiscom.vchat.data.model;

import java.util.List;

/**
 * 支付渠道信息对象
 * Created by zhangdroid on 2017/6/10.
 */
public class PayWay extends BaseModel {
    private List<PayDict> dictPayList;

    public List<PayDict> getDictPayList() {
        return dictPayList;
    }

    public void setDictPayList(List<PayDict> dictPayList) {
        this.dictPayList = dictPayList;
    }
}
