package com.wiscom.vchat.data.api;

import android.content.Context;
import android.text.TextUtils;

import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.DateTimeUtil;
import com.wiscom.library.util.ToastUtil;
import com.wiscom.okhttp.OkHttpHelper;
import com.wiscom.okhttp.callback.JsonCallback;
import com.wiscom.okhttp.callback.StringCallback;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.AgoraChannelKey;
import com.wiscom.vchat.data.model.AgoraToken;
import com.wiscom.vchat.data.model.ApplyAddFriendMode;
import com.wiscom.vchat.data.model.ApplyPayModel;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.CallRecord;
import com.wiscom.vchat.data.model.FaceBookAccount;
import com.wiscom.vchat.data.model.FindPassword;
import com.wiscom.vchat.data.model.FollowUser;
import com.wiscom.vchat.data.model.FriendListModel;
import com.wiscom.vchat.data.model.IsFollow;
import com.wiscom.vchat.data.model.LocationInfo;
import com.wiscom.vchat.data.model.Login;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.PayWay;
import com.wiscom.vchat.data.model.RecommendNewJoinList;
import com.wiscom.vchat.data.model.Register;
import com.wiscom.vchat.data.model.RequestVideoCall;
import com.wiscom.vchat.data.model.SearchUserList;
import com.wiscom.vchat.data.model.SystemUser;
import com.wiscom.vchat.data.model.UpLoadMyInfo;
import com.wiscom.vchat.data.model.UpLoadMyPhoto;
import com.wiscom.vchat.data.model.UploadInfoParams;
import com.wiscom.vchat.data.model.UserDetailforOther;
import com.wiscom.vchat.data.model.VedioScreenInfo;
import com.wiscom.vchat.data.model.VideoCall;
import com.wiscom.vchat.data.model.VideoHeartBeat;
import com.wiscom.vchat.data.model.VideoStop;
import com.wiscom.vchat.data.model.WithdrawRecordList;
import com.wiscom.vchat.data.preference.PlatformPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.ui.pay.util.EncryptUtil;
import com.wiscom.vchat.ui.pay.util.Purchase;

import java.io.File;
import java.util.Map;

import okhttp3.Call;

/**
 * 该类中实现项目中用到的所有接口
 * Created by zhangdroid on 2017/5/20.
 */
public class ApiManager {


    /**
     * 请求成功
     */
    private static final String CODE_SUCCESS = "1";
    /**
     * 请求结果为空
     */
    private static final String CODE_EMPTY = "-8";
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private ApiManager() {
    }

    /**
     * 检查公用参数是否为空
     */
    private static void checkCommonParams() {
        Map<String, String> map = OkHttpHelper.getInstance().getCommonParams();
        if (null == map || map.isEmpty() || TextUtils.isEmpty(map.get("platformInfo"))) {
            // 设置API公用参数
            OkHttpHelper.getInstance().addCommonParam("platformInfo", PlatformPreference.getPlatformJsonString());
        }
    }

    // ***************************** 激活/注册/登陆 ***************************

    public static void activation(final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_ACTIVATION)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())||"2".equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    public static void register(String nickname, boolean isMale, String age, File file,final IGetDataListener<Register> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_PHOTO_REGISTER)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("photoType","2")// 2为头像，1为普通图片
                    .addFile("file", file.getName(), file)
                    .addHeader("nickName", nickname)
                    .addHeader("gender", isMale ? "0" : CODE_SUCCESS)
//                    .addParam("age", age)
                    .build()
                    .execute(new JsonCallback<Register>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.register_error), false);
                        }

                        @Override
                        public void onSuccess(Register response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.register_error), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    public static void login(String account, String password, final IGetDataListener<Login> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_LOGIN)
                    .addParam("account", account)
                    .addParam("password", password)
                    .build()
                    .execute(new JsonCallback<Login>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.login_error), false);
                        }

                        @Override
                        public void onSuccess(Login response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                if(response.getIsSucceed().equals("-3")){
                                    listener.onError(mContext.getString(R.string.account_password_err), false);
                                }else{
                                    listener.onError(mContext.getString(R.string.login_error), false);
                                }
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    public static void findPassword(final IGetDataListener<FindPassword> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_FIND_PWD)
                    .build()
                    .execute(new JsonCallback<FindPassword>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.find_pwd_error), false);
                        }

                        @Override
                        public void onSuccess(FindPassword response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(mContext.getString(R.string.find_pwd_error), true);
        }
    }

    // ***************************** 首页 ***************************

    /**
     * 获取首页推荐用户
     *
     * @param url      推荐用户类型url
     * @param pageNum  页码
     * @param pageSize 每页用户数
     * @param criteria 搜索条件{@link com.wiscom.vchat.data.model.SearchCriteria}
     * @param listener
     */
    public static void getHomepageRecommend(String url, int pageNum, String pageSize, String criteria, final IGetDataListener<SearchUserList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(url)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .addParam("criteria", TextUtils.isEmpty(criteria) ? "" : criteria)
                    .build()
                    .execute(new JsonCallback<SearchUserList>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(SearchUserList response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 消息 ***************************

    /**
     * 获取视频通话记录
     *
     * @param pageNum  页码
     * @param pageSize 每页用户数
     * @param listener
     */
    public static void getVideoCallRecord(int pageNum, String pageSize, final IGetDataListener<CallRecord> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_VIDEO_RECORD)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .build()
                    .execute(new JsonCallback<CallRecord>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(CallRecord response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（文字）
     *
     * @param uId
     * @param listener
     */
    public static void interruptText(long uId, String content, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_TEXT)
                    .addParam("remoteUid", String.valueOf(uId))
                    .addParam("content", content)
                    .addParam("fromType", "2")
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（语音）
     *
     * @param uId
     * @param listener
     */
    public static void interruptVoice(long uId, File file, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_VOICE)
                    .addHeader("remoteUid", String.valueOf(uId))
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 聊天支付拦截（图片）
     *
     * @param uId
     * @param listener
     */
    public static void interruptImage(long uId, File file, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MESSAGE_INTERRUPT_IMAGE)
                    .addHeader("remoteUid", String.valueOf(uId))
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 关注 ***************************

    public static void getFollowList(final int pageNum, final IGetDataListener<FollowUser> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_FOLLOW_LIST)
                    .build()
                    .execute(
                            new JsonCallback<FollowUser>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(FollowUser response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed()) && pageNum == 1) {
                                        listener.onResult(response, false);
                                    } else if (pageNum > 1 || CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    // ***************************** 好友 ***************************
    /**
     * 获取好友列表
     *
     * @param pageNum  页码
     * @param listener
     */
    public static void getFriendList(final int pageNum, final IGetDataListener<FriendListModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FRIEND_LIST)
                    .addHeader("pageNum", String.valueOf(pageNum))
                    .addHeader("pageSize", "20")
                    .build()
                    .execute(
                            new JsonCallback<FriendListModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(FriendListModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed()) && pageNum == 1) {
                                        listener.onResult(response, false);
                                    } else if (pageNum > 1 || CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 根据id搜索好友
     *
     * @param listener
     */
    public static void getFriendFromId(String mAccount,final IGetDataListener<MyInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FRIEND_FROM_ID)
                    .addParam("account",mAccount)
                    .build()
                    .execute(new JsonCallback<MyInfo>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
//                            listener.onError(mContext.getString(R.string.my_info_error), false);
                        }

                        @Override
                        public void onSuccess(MyInfo response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
//                                listener.onError(mContext.getString(R.string.my_info_error), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取申请人
     *
     * @param pageNum  页码
     * @param listener
     */
    public static void getFriendApply(final int pageNum, final IGetDataListener<FriendListModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FRIEND_APPLY)
                    .addHeader("pageNum", String.valueOf(pageNum))
                    .addHeader("pageSize", "20")
                    .build()
                    .execute(
                            new JsonCallback<FriendListModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(null, false);
                                }

                                @Override
                                public void onSuccess(FriendListModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed()) && pageNum == 1) {
                                        listener.onResult(response, false);
                                    } else if (pageNum > 1 || CODE_EMPTY.equals(response.getIsSucceed())) {
                                        listener.onResult(response, true);
                                    } else {
                                        listener.onError(null, false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 申请添加好友
     *
     * @param user_uid
     * @param listener
     */
    public static void applyAddFriend(String user_uid, final IGetDataListener<ApplyAddFriendMode> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.APPLY_ADD_FRIEND)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<ApplyAddFriendMode>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.add_friend_fail), false);
                        }

                        @Override
                        public void onSuccess(ApplyAddFriendMode response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())||response.getIsSucceed().equals("2")) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.add_friend_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 同意添加好友
     *
     * @param user_uid
     * @param listener
     */
    public static void agreeAddFriend(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.AGREE_ADD_FRIEND)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.add_friend_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
//                                listener.onError(mContext.getString(R.string.add_friend_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 拒绝添加好友
     *
     * @param user_uid
     * @param listener
     */
    public static void refuseAddFriend(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.REFUSE_ADD_FRIEND)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.add_friend_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
//                                listener.onError(mContext.getString(R.string.add_friend_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 添加三个好友后扣费
     *
     * @param listener
     */
    public static void applypay(final IGetDataListener<ApplyPayModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.FRIEND_APPLY_PAY)
                    .build()
                    .execute(new JsonCallback<ApplyPayModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.add_friend_fail), false);
                        }

                        @Override
                        public void onSuccess(ApplyPayModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.add_friend_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 个人中心 ***************************

    public static void getMyInfo(final IGetDataListener<MyInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_PERSON_MY_INFO)
                    .build()
                    .execute(new JsonCallback<MyInfo>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.my_info_error), false);
                        }

                        @Override
                        public void onSuccess(MyInfo response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.my_info_error), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    public static void upLoadMyInfo(UploadInfoParams uploadInfoParams, final IGetDataListener<UpLoadMyInfo> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_MYINFO)
                    .addParam("uploadInfoParams", Util.getUploadInfoParamsJsonString(uploadInfoParams))
                    .build()
                    .execute(new JsonCallback<UpLoadMyInfo>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError("上传失败", false);
                                 }

                                 @Override
                                 public void onSuccess(UpLoadMyInfo response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError("上传失败", false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError("上传失败", false);
        }
    }

    /**
     * 上传图片或者头像
     *
     * @param file      图片的路径
     * @param photoType true为上传头像，false为上传普通图片
     * @param listener
     */
    public static void upLoadMyPhotoOrAvator(File file, boolean photoType, final IGetDataListener<UpLoadMyPhoto> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_PHOTO)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addHeader("photoType", photoType == true ? "2" : "1")// 2为头像，1为普通图片
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new JsonCallback<UpLoadMyPhoto>() {

                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError("上传失败", false);
                                 }

                                 public void onSuccess(UpLoadMyPhoto response, int id) {
                                     if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                         listener.onResult(response, false);
                                     } else {
                                         listener.onError("上传失败", false);
                                     }
                                 }
                             }
                    );
        } else {
            listener.onError("上传失败", false);
        }
    }

    /**
     * 修改用户状态
     *
     * @param status   1、空闲 2、在聊 3、勿扰
     * @param listener
     */
    public static void modifyUserStatus(String status, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_MODIFFY_USER_STATUS)
                    .addParam("status", TextUtils.isEmpty(status) ? "" : status)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 支付 ***************************

    /**
     * 获取支付渠道信息
     *
     * @param serviceType  服务类型{@link com.wiscom.vchat.C.pay}
     * @param listener
     */
    public static void getPayWay(String serviceType, final IGetDataListener<PayWay> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_PAY_WAY)
//                    .addParam("fromTag", TextUtils.isEmpty(fromTag) ? "" : fromTag)
                    .addParam("serviceType", TextUtils.isEmpty(serviceType) ? "" : serviceType)

                    .build()
                    .execute(new JsonCallback<PayWay>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(PayWay response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 内购支付成功后向后台发送支付结果
     *
     * @param purchase
     * @param serviceId
     * @param payType
     * @param listener
     */
    public static void payment(Purchase purchase, String serviceId, String payType, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.PAYMENT)
                    .addParam("platformInfo",PlatformPreference.getPlatformJsonString())
                    .addParam("token", PlatformPreference.getToken())
                    .addParam("userId", UserPreference.getId())
                    .addParam("clientTime", DateTimeUtil.getCurrentTime())
                    .addParam("userGoogleId", "")
                    .addParam("transactionId", purchase.getOrderId())
                    .addParam("payTime", String.valueOf(purchase.getPurchaseTime()))
                    .addParam("googleId", purchase.getPackageName())
                    .addParam("payType", payType)
                    .addParam("serviceId", serviceId)
                    .addParam("transactionToken", EncryptUtil.encrypt("pid", purchase.getOrderId()))
                    //后台验证支付和续费使用 具体key 跟后台协商
                    .addParam("purchaseToken", purchase.getToken())
                    .addParam("packageName", purchase.getPackageName())
                    .addParam("productId", purchase.getSku())
                    .build()
                    .execute(new StringCallback() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 内购支付成功后向后台发送支付结果
     *
     * @param serviceId
     * @param payType
     * @param listener
     */
    public static void payment(String payTime, String serviceId, String payType,String getPackageName,String getOrderId, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.PAYMENT)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("token", "")
                    .addParam("userId", "13519")
                    .addParam("clientTime", "")
                    .addParam("userGoogleId", "")
                    .addParam("transactionId", "GPA.1234-5678-9012-34567")
                    .addParam("payTime", payTime)
                    .addParam("googleId","com.online.face2face")
                    .addParam("payType", payType)
                    .addParam("serviceId", serviceId)
                    .addParam("transactionToken", EncryptUtil.encrypt("pid",getOrderId))
                    .build()
                    .execute(new StringCallback() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 获取提现记录
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @param listener
     */
    public static void getWithdrawRecord(int pageNum, String pageSize, final IGetDataListener<WithdrawRecordList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_WITHDRAW_RECORD)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .build()
                    .execute(new JsonCallback<WithdrawRecordList>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(WithdrawRecordList response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                response.getListRecord();

                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 对方空间页 ***************************

    public static void getUserInfo(String user_uid, final IGetDataListener<UserDetailforOther> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_USER_INFO)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build().execute(
                    new JsonCallback<UserDetailforOther>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError("加载失败", false);
                        }

                        @Override
                        public void onSuccess(UserDetailforOther response, int id) {
                            listener.onResult(response, false);
                        }
                    }
            );
        }
    }

    //关注
    public static void follow(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_FOLLOW)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError("关注失败", false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError("关注失败", false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    //取消关注
    public static void unFollow(String user_uid, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UN_FOLLOW)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError("取消关注失败", false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError("取消关注失败", false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 是否关注某个用户
     *
     * @param uId      用户id
     * @param listener
     */
    public static void isFollow(String uId, final IGetDataListener<IsFollow> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_IS_FOLLOW)
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(new JsonCallback<IsFollow>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(IsFollow response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 视频 ***************************

    /**
     * 获取声网信令系统token，用于登陆信令系统
     *
     * @param listener
     */
    public static void getAgoraToken(final IGetDataListener<AgoraToken> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_TOKEN)
                    .build()
                    .execute(new JsonCallback<AgoraToken>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(AgoraToken response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 获取声网channel key，用于加入频道
     *
     * @param listener
     */
    public static void getAgoraChannelKey(String channelId, final IGetDataListener<AgoraChannelKey> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_CHANNEL_KEY)
                    .addParam("channel", TextUtils.isEmpty(channelId) ? "" : channelId)
                    .build()
                    .execute(new JsonCallback<AgoraChannelKey>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(AgoraChannelKey response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 发起/接受视频呼叫
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoCall(String uId, final IGetDataListener<VideoCall> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_CALL)
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(new JsonCallback<VideoCall>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(VideoCall response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 发起/接受视频呼叫,获取另一个用户信息，组建频道（Tian)
     *
     * @param listener
     */
    public static void requestVideoCall(boolean isVideoSwitch,VedioScreenInfo screenGenter, int matchTimes,final IGetDataListener<RequestVideoCall> listener) {
        String url;
        if(isVideoSwitch){
             url=ApiConstant.RANDOM_VEDIO_SWITCH;
        }else{
            url=ApiConstant.RANDOM_VEDIO;
        }
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(url)
                    .addParam("matchCriteria",Util.getScreenInfo(screenGenter))
                    .addParam("matchTimes", String.valueOf(matchTimes))
                    .build()
                    .execute(new JsonCallback<RequestVideoCall>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(RequestVideoCall response, int id) {
                            String isSucceed = response.getIsSucceed();
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 随机匹配成功建立连接
     *
     * @param user_uid      对方用户id
     * @param connectType   连接类型 1、免费 2、条件搜索 3、主动呼叫
     * @param listener
     */
    public static void vedioCreatSuccess(String user_uid,int connectType , final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.VEDIO_SUCCESS)
                    .addParam("remoteUid", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .addParam("connectType", String.valueOf(connectType))
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            String isSucceed = response.getIsSucceed();
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 系统用户视频成功建立连接
     *
     * @param user_uid      对方用户id
     * @param listener
     */
    public static void systemVideoCreatSuccess(String user_uid, int connectType,final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.SYSTEM_VIDEO_SUCCESS)
                    .addParam("remoteId", TextUtils.isEmpty(user_uid) ? "" : user_uid)
                    .addParam("connectType", String.valueOf(connectType))
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            String isSucceed = response.getIsSucceed();
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 视频通话心跳（视频接通后轮询）
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoHeartBeat(String uId, final IGetDataListener<VideoHeartBeat> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.VIDEO_HEART_BEAT)
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(new JsonCallback<VideoHeartBeat>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(VideoHeartBeat response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 退出随机匹配界面
     *
     * @param listener
     */
    public static void exitMatch(final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.EXIT_MATCH)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {

                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    /**
     * 挂断视频通话
     *
     * @param uId      对方用户id
     * @param listener
     */
    public static void videoStop(String uId, final IGetDataListener<VideoStop> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEO_STOP)
                    .addParam("remoteUid", TextUtils.isEmpty(uId) ? "" : uId)
                    .build()
                    .execute(new JsonCallback<VideoStop>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(VideoStop response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 获取系统视频用户
     *
     * @param listener
     */
    public static void getSystemUser(VedioScreenInfo screenGenter,final IGetDataListener<SystemUser> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SYSYTEM_USER)
                    .addParam("matchCriteria",Util.getScreenInfo(screenGenter))
                    .build()
                    .execute(new JsonCallback<SystemUser>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(SystemUser response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 获取礼物字典
     */
    public static void getGiftList(final IGetDataListener<String> listener) {
//        if (NetUtil.isNetworkAvailable(mContext)) {
//            checkCommonParams();
//            OkHttpHelper.post()
//                    .url(ApiConstant.URL_GIFTS_DIC)
//                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
//                    .build()
//                    .execute(new StringCallback() {
//                                 @Override
//                                 public void onError(Call call, Exception e, int id) {
//                                     listener.onError(null, false);
//                                 }
//
//                                 @Override
//                                 public void onSuccess(String response, int id) {
//                                     if (!TextUtils.isEmpty(response)) {
//                                         GiftsDictiorary giftsDictiorary = new Gson().fromJson(response, GiftsDictiorary.class);
//                                         if (giftsDictiorary != null) {
//                                             if (CODE_SUCCESS.equals(giftsDictiorary.getIsSucceed())) {
//                                                 listener.onResult(response, false);
//                                             } else if (CODE_EMPTY.equals(giftsDictiorary.getIsSucceed())) {
//                                                 listener.onResult(response, true);
//                                             } else {
//                                                 listener.onError(null, false);
//                                             }
//                                         }
//                                     }
//                                 }
//                             }
//                    );
//        } else {
//            listener.onError(null, true);
//        }
    }

    /**
     * 埋点：统计不同位置进入支付界面的次数
     *
     * @param remoteId    {非必须 对方ID 长整型}
     * @param userId      :{非必须 本方用户ID 长整型}
     * @param basetag
     * @param extendtag

     */
    public static void intoPayActivityNum(String remoteId, String userId,String basetag,String extendtag,final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.USER_ACTIVE_TAG)
                    .addParam("remoteId", TextUtils.isEmpty(remoteId) ? "" : remoteId)
                    .addParam("userId", TextUtils.isEmpty(userId) ? "" : userId)
                    .addParam("basetag", TextUtils.isEmpty(basetag) ? "" : userId)
                    .addParam("extendtag", TextUtils.isEmpty(extendtag) ? "" : userId)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            String isSucceed = response.getIsSucceed();
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    // ***************************** 视频上传与主播认证 ***************************

    public static void getFaceBookAccount(final IGetDataListener<FaceBookAccount> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_GET_FACEBOOK_ACCOUNT)
                    .build()
                    .execute(new JsonCallback<FaceBookAccount>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(FaceBookAccount response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }

    public static void submitCheck(String videoNumber, File file, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_SUBMIT)
                    .addHeader("videoSeconds", "10")
                    .addHeader("videoNumber", videoNumber)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("file", file.getName(), file)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);
                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 上传视频秀
     * **/
    public static void uploadVedio(File file,File imageFile, final IGetDataListener<String> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPLOAD_VEDIO)
                    .addHeader("videoSeconds", "10")
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addFile("icon", imageFile.getName(), imageFile)
                    .addFile("video", file.getName(), file)
                    .build()
                    .execute(new StringCallback() {
                                 @Override
                                 public void onError(Call call, Exception e, int id) {
                                     listener.onError(null, false);
                                 }

                                 @Override
                                 public void onSuccess(String response, int id) {
                                     listener.onResult(response, false);

                                 }
                             }
                    );
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 推荐秀：最新加入的用户视频:URL_RECOMMEND_NEW_JOIN
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @param listener
     */
    public static void recommendNewJoin(int pageNum, String pageSize, final IGetDataListener<RecommendNewJoinList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_RECOMMEND_NEW_JOIN)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .build()
                    .execute(new JsonCallback<RecommendNewJoinList>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(RecommendNewJoinList response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                response.getVideoSquareList();

                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 推荐秀：最热加入的用户视频
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @param listener
     */
    public static void recommendHotJoin(int pageNum, String pageSize, final IGetDataListener<RecommendNewJoinList> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_RECOMMEND_HOT_JOIN)
                    .addParam("pageNum", String.valueOf(pageNum))
                    .addParam("pageSize", pageSize)
                    .build()
                    .execute(new JsonCallback<RecommendNewJoinList>() {

                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(RecommendNewJoinList response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                response.getVideoSquareList();

                                listener.onResult(response, false);
                            } else if (CODE_EMPTY.equals(response.getIsSucceed())) {
                                listener.onResult(response, true);
                            } else {
                                listener.onError(null, false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 视频点赞接口
     * **/
    public static void clickPraise(String videoId, final IGetDataListener<String> listener){
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_VIDEPO_PRAISE)
                    .addParam("videoId", videoId)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }

    }
    /**
 * 调整主播的价格
 * **/
    public static void savePrice(String mprice, final IGetDataListener<String> listener){
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPDATA_PRICE)
                    .addParam("price", mprice)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }

    }
    /**
     * 提现申请
     * **/
    public static void withdraw(String getMoney, String getEmail, String getName, String getSur, String getNation,
                                final IGetDataListener<BaseModel> listener){
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_WITHDRAW_APPLY)
                    .addHeader("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("amount", getMoney)
                    .addParam("account", getEmail)
                    .addParam("withdrawType", "1")
                    .addParam("firstName", getName)
                    .addParam("lastName", getSur)
                    .addParam("country", getNation)
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(null, false);
                            }
                        }


                    });
        } else {
            listener.onError(null, true);
        }

    }
    /**
     * 谷歌定位
     * **/
    public static void googleLocation(String mprice, final IGetDataListener<String> listener){
        if (NetUtil.isNetworkAvailable(mContext)) {
            OkHttpHelper.post()
                    .url(ApiConstant.URL_UPDATA_PRICE)
                    .addParam("price", mprice)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(null, false);
                        }

                        @Override
                        public void onSuccess(String response, int id) {
                            listener.onResult(response, false);
                        }
                    });
        } else {
            listener.onError(null, true);
        }

    }
    /**
     * 位置信息
     *
     * @param info 位置信息
     * @param listener
     */
    public static void setLocation(LocationInfo info, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_LOCATION)
                    .addParam("locationInfo", Util.getUploadLocation(info))
                    .build()
                    .execute(new JsonCallback<BaseModel>() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            listener.onError(mContext.getString(R.string.add_friend_fail), false);
                        }

                        @Override
                        public void onSuccess(BaseModel response, int id) {
                            if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                listener.onResult(response, false);
                            } else {
                                listener.onError(mContext.getString(R.string.add_friend_fail), false);
                            }
                        }
                    });
        } else {
            listener.onError(null, true);
        }
    }
    /**
     * 举报
     *
     * @param listener
     */
    public static void report(String userId, final String resonCode, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_REPORT)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("content", "0")
                    .addParam("reasonCode", resonCode)
                    .addParam("remoteUid", userId)
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.upload_fail), false);
                                }

                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                        ToastUtil.showShortToast(mContext,mContext.getString(R.string.report_success));
                                    } else {
                                        listener.onError(mContext.getString(R.string.upload_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.upload_fail), false);
        }
    }
    /**
     * 更改密码
     *
     * @param listener
     */
    public static void changePassword(String oldPasswprd, String newPassword, final IGetDataListener<BaseModel> listener) {
        if (NetUtil.isNetworkAvailable(mContext)) {
            checkCommonParams();
            OkHttpHelper.post()
                    .url(ApiConstant.URL_CHANGE_PASSWORD)
                    .addParam("platformInfo", PlatformPreference.getPlatformJsonString())
                    .addParam("password", oldPasswprd)
                    .addParam("newPassword", newPassword)
                    .addParam("confirmPassword", newPassword)
                    .build()
                    .execute(
                            new JsonCallback<BaseModel>() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    listener.onError(mContext.getString(R.string.change_password_fail), false);
                                }

                                public void onSuccess(BaseModel response, int id) {
                                    if (CODE_SUCCESS.equals(response.getIsSucceed())) {
                                        listener.onResult(response, false);
                                        listener.onError(mContext.getString(R.string.change_password_success), false);
                                    } else {
                                        listener.onError(mContext.getString(R.string.change_password_fail), false);
                                    }
                                }
                            }
                    );
        } else {
            listener.onError(mContext.getString(R.string.change_password_fail), false);
        }
    }


}
