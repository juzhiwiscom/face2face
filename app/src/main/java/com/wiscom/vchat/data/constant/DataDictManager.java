package com.wiscom.vchat.data.constant;

import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.common.Util;

import java.util.List;

/**
 * 公用数据字典常量管理类。
 * <p>所有数据字典常量从后台接口获取，若接口请求失败或返回数据为空，则使用本地数据字典</p>
 * Created by zhangdroid on 2017/6/9.
 */
public class DataDictManager {

    private DataDictManager() {
    }

    /**
     * @return 获得国家常量集合
     */
    public static List<String> getCountryList() {
        List<String> countryListString = ParamsUtils.getMapListString(Util.getCountryString());
         return countryListString;
    }

    /**
     * @return 获得语言常量集合
     */
    public static List<String> getLanguageList() {
        List<String> languageString = ParamsUtils.getMapListString(Util.getLanguageString());
        return languageString;
    }

    /**
     * @return 获得州信息的常量集合
     */
    public static List<String> getStatteList() {
        List<String> stateString = ParamsUtils.getMapListString(Util.getStateString());
        return stateString;
    }

}
