package com.wiscom.vchat.data.model;

import java.util.List;

/**
 * 用户详细信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class UserDetail {
    private UserBase userBase;// 用户基础信息
    private List<UserPhoto> userPhotos;// 用户图片列表
    private List<UserVideoShow> userVideoShows;//用户视频秀集合
    private HostInfo hostInfo;// 播主信息
    private UserBean userBean;// 普通用户金币信息
    private String isFollow;// 1为已经关注，0没有关注
    private int isFriend;//是否是好友：1是 0不是

    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public UserBase getUserBase() {
        return userBase;
    }

    public void setUserBase(UserBase userBase) {
        this.userBase = userBase;
    }

    public List<UserPhoto> getUserPhotos() {
        return userPhotos;
    }

    public void setUserPhotos(List<UserPhoto> userPhotos) {
        this.userPhotos = userPhotos;
    }

    public HostInfo getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfo hostInfo) {
        this.hostInfo = hostInfo;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }

    public List<UserVideoShow> getUserVideoShows() {
        return userVideoShows;
    }

    public void setUserVideoShows(List<UserVideoShow> userVideoShows) {
        this.userVideoShows = userVideoShows;
    }
}
