package com.wiscom.vchat.data.model;

/**
 * Created by tianzhentao on 2018/3/30.
 */

public class ApplyPayModel extends BaseModel{
   private String beanStatus;
    private String beanCounts;

    public String getBeanCounts() {
        return beanCounts;
    }

    public void setBeanCounts(String beanCounts) {
        this.beanCounts = beanCounts;
    }

    public String getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(String beanStatus) {
        this.beanStatus = beanStatus;
    }
}
