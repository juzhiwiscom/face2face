package com.wiscom.vchat.data.model;

/**
 * 播主信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class HostInfo {


    private long guid;// id
    private long userId;// 用户id
    private String hostType;// 主播类型
    private float price;// 主播价格（视频每分钟）
    private float totalMsg;// 收到总消息数量
    private float totalMinute;// 总聊天时长
    private float totalIncome;// 总收入
    private float incomeBalance;// 当前金币数（收入余额）
    private String addTime;


    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getHostType() {
        return hostType;
    }

    public void setHostType(String hostType) {
        this.hostType = hostType;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotalMsg() {
        return totalMsg;
    }

    public void setTotalMsg(float totalMsg) {
        this.totalMsg = totalMsg;
    }

    public float getTotalMinute() {
        return totalMinute;
    }

    public void setTotalMinute(float totalMinute) {
        this.totalMinute = totalMinute;
    }

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }

    public float getIncomeBalance() {
        return incomeBalance;
    }

    public void setIncomeBalance(float incomeBalance) {
        this.incomeBalance = incomeBalance;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

}
