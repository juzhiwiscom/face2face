package com.wiscom.vchat.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转视频聊天页面需要传递的参数
 * Created by zhangdroid on 2017/6/20.
 */
public class VideoParcelable implements Parcelable {
    /**
     * 用户id（对方）
     */
    public long guid;
    /**
     * 用户account（对方）
     */
    public String account;
    /**
     * 用户昵称（对方）
     */
    public String nickname;
    /**
     * 用户国家（对方）
     */
    public String country;
    /**
     * 用户性别（对方）
     */
    public String gender;
    /**
     * 用户头像（对方）
     */
    public String imageurl;
    /**
     * 频道id
     */
    public String channelId;
    /**
     * 播主价格
     */
    public String hostPrice;
    /**
     * 进入视频通话的来源界面：1受邀请界面，2邀请别人加入视频的界面，3随机视频的界面
     */
    public String fromView;

    public VideoParcelable(long guid, String account, String nickname, String channelId, String hostPrice,String fromView) {
        this.guid = guid;
        this.account = account;
        this.nickname = nickname;
        this.channelId = channelId;
        this.hostPrice = hostPrice;
        this.fromView=fromView;
    }

    public VideoParcelable(long guid, String account, String nickname, String channelId, String hostPrice,String country,String genter,String imageurl,String fromView) {
        this.guid = guid;
        this.account = account;
        this.nickname = nickname;
        this.channelId = channelId;
        this.hostPrice = hostPrice;
        this.country=country;
        this.gender=genter;
        this.imageurl=imageurl;
        this.fromView=fromView;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.guid);
        dest.writeString(this.account);
        dest.writeString(this.nickname);
        dest.writeString(this.channelId);
        dest.writeString(this.hostPrice);
        dest.writeString(this.country);
        dest.writeString(this.gender);
        dest.writeString(this.imageurl);
        dest.writeString(this.fromView);
    }

    protected VideoParcelable(Parcel in) {
        this.guid = in.readLong();
        this.account = in.readString();
        this.nickname = in.readString();
        this.channelId = in.readString();
        this.hostPrice = in.readString();
        this.country = in.readString();
        this.gender = in.readString();
        this.imageurl = in.readString();
        this.fromView = in.readString();
    }

    public static final Parcelable.Creator<VideoParcelable> CREATOR = new Parcelable.Creator<VideoParcelable>() {
        @Override
        public VideoParcelable createFromParcel(Parcel source) {
            return new VideoParcelable(source);
        }

        @Override
        public VideoParcelable[] newArray(int size) {
            return new VideoParcelable[size];
        }
    };
}
