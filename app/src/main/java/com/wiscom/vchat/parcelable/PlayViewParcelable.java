package com.wiscom.vchat.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/6/20.
 * 在视频录制与视频上传界面传递url信息
 */

public class PlayViewParcelable implements Parcelable{
    public String urlData;
    public String thumbnailUrl;
    public String guid;
    public String nickName;
    public String videoUrl;
    public String icon;
    public String account;
    public String praiseCount;
    public String videoId;
    public String isPraise;

    public PlayViewParcelable(String videoUrl, String thumbnailUrl, String nickName, String guid,String icon,String account,String praiseCount,String videoId,String isPraise) {
        this.videoUrl=videoUrl;
        this.thumbnailUrl=thumbnailUrl;
        this.guid=guid;
        this.nickName=nickName;
        this.icon=icon;
        this.account=account;
        this.praiseCount=praiseCount;
        this.videoId=videoId;
        this.isPraise=isPraise;

    }
    public PlayViewParcelable(String urlData) {
        this.urlData = urlData;
    }

    protected PlayViewParcelable(Parcel in) {
        urlData = in.readString();
        thumbnailUrl = in.readString();
        guid = in.readString();
        nickName = in.readString();
        videoUrl = in.readString();
        icon = in.readString();
        account = in.readString();
        praiseCount=in.readString();
        videoId=in.readString();
        isPraise=in.readString();

    }

    public static final Creator<PlayViewParcelable> CREATOR = new Creator<PlayViewParcelable>() {
        @Override
        public PlayViewParcelable createFromParcel(Parcel in) {
            return new PlayViewParcelable(in);
        }

        @Override
        public PlayViewParcelable[] newArray(int size) {
            return new PlayViewParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(urlData);
        dest.writeString(thumbnailUrl);
        dest.writeString(guid);
        dest.writeString(nickName);
        dest.writeString(videoUrl);
        dest.writeString(icon);
        dest.writeString(account);
        dest.writeString(praiseCount);
        dest.writeString(videoId);
        dest.writeString(isPraise);

    }
}
