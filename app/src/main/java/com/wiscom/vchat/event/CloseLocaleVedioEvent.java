package com.wiscom.vchat.event;

/**
 * Created by tianzhentao on 2018/1/27.
 */

public class CloseLocaleVedioEvent {
    public boolean close;
    public CloseLocaleVedioEvent(boolean close) {
        this.close=close;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }
}
