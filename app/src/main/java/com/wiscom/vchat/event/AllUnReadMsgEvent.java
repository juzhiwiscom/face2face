package com.wiscom.vchat.event;

/**
 * Created by tianzhentao on 2018/1/27.
 */

public class AllUnReadMsgEvent {
    public int allMsg;
    public AllUnReadMsgEvent(int allMsg) {
        this.allMsg=allMsg;
    }

    public int getAllMsg() {
        return allMsg;
    }

    public void setAllMsg(int allMsg) {
        this.allMsg = allMsg;
    }
}
