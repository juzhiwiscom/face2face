package com.wiscom.vchat.view.beauty.core;

public enum MediaType {
	/**
	 * 录播
	 */
	MEDIA_RECORD,
	/**
	 * 直播
	 */
	MEDIA_PUSH
}
