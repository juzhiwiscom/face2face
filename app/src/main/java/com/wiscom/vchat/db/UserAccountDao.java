package com.wiscom.vchat.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.wiscom.vchat.data.model.HuanXinUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class UserAccountDao {
    private MyDbHelper dbHelper;

    public UserAccountDao(Context context) {
        dbHelper = new MyDbHelper(context);
    }

    //添加用戶
    public void addAccount(HuanXinUser user) {
        //插入数据之前先要判断数据库中是否存在这条数据
        HuanXinUser accountByHyID = getAccountByHyID(user.getHxId());
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserAccountTable.COL_HXNAME, user.getHxName());
        values.put(UserAccountTable.COL_HXICON, user.getHxIcon());
        values.put(UserAccountTable.COL_HXID, user.getHxId());
        values.put(UserAccountTable.COL_ISREAD, user.getIsRead());
        values.put(UserAccountTable.COL_ACCOUNT, user.getAccount());
//        values.put(UserAccountTable.COL_LAST_MSG, user.getLastMsg());
//        values.put(UserAccountTable.COL_LAST_MSG_TIME, user.getLastMsgTime());

        if (accountByHyID!=null) {
            readableDatabase.update(UserAccountTable.TABLE_NAME, values, UserAccountTable.COL_HXID+" = ?", new String[]{user.getHxId()});
        }else{
            readableDatabase.replace(UserAccountTable.TABLE_NAME, null, values);
        }
    }



    //根据环信ID获取用户
    public HuanXinUser getAccountByHyID(String hyxi) {
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME + " where " + UserAccountTable.COL_HXID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{hyxi});
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
//                huanXinUser.setLastMsg(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_LAST_MSG)));
//                huanXinUser.setLastMsgTime(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_LAST_MSG_TIME)));
            }
            cursor.close();
        }

        return huanXinUser;
    }
    //获取所有用户的集合
    public List<HuanXinUser> getAllAcount(){
        List<HuanXinUser> huanXinUsers=new ArrayList<>();
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME;
        Cursor cursor = readableDatabase.rawQuery(sql, null);
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
                huanXinUsers.add(huanXinUser);
            }
            cursor.close();
        }
     return  huanXinUsers;
    }
    //获取信息是否已经读取的状态值
    public int getState(String hyxi){
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        String sql = "select * from " + UserAccountTable.TABLE_NAME + " where " + UserAccountTable.COL_HXID + "=?";
        Cursor cursor = readableDatabase.rawQuery(sql, new String[]{hyxi});
        HuanXinUser huanXinUser = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                huanXinUser = new HuanXinUser();
                huanXinUser.setHxName(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXNAME)));
                huanXinUser.setHxIcon(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXICON)));
                huanXinUser.setHxId(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_HXID)));
                huanXinUser.setIsRead(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ISREAD)));
                huanXinUser.setAccount(cursor.getString(cursor.getColumnIndex(UserAccountTable.COL_ACCOUNT)));
            }
            cursor.close();
        }
        if(huanXinUser!=null){
            String isRead = huanXinUser.getIsRead();
            if(!TextUtils.isEmpty(isRead)){
                int i = Integer.parseInt(isRead);
                return i;
            }
        }
        return 1;
    }
    //修改信息是否已经读取的状态值
    public void setState(HuanXinUser user,boolean flag){
        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();
        ContentValues values = new ContentValues();
        if(true){//已经读取
            values.put(UserAccountTable.COL_ISREAD, "0");
        }else{//尚未读取
            values.put(UserAccountTable.COL_ISREAD, "1");
        }
            readableDatabase.update(UserAccountTable.TABLE_NAME, values, UserAccountTable.COL_HXID+" = ?", new String[]{user.getHxId()});
    }

}
