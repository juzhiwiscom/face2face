package com.wiscom.vchat.db;

/**
 * Created by Administrator on 2017/7/14.
 */

public class UserAccountTable {
    public static final String TABLE_NAME="vchat_tab_account";
    public static final String COL_HXID="vchat_hxid";
    public static final String COL_HXNAME="vchat_hxName";
    public static final String COL_HXICON="vchat_hxIcon";
    public static final String COL_ISREAD="vchat_isRead";
    public static final String COL_ACCOUNT="vchat_account";
    //    public static final String COL_LAST_MSG="lastMsg";
//    public static final String COL_LAST_MSG_TIME="lastMsgTime";
public static final String CREATE_TABLE="create table "
        +TABLE_NAME+" ("
        +COL_HXID+" text primary key,"
        +COL_HXNAME+" text,"
        +COL_HXICON+" text,"
        +COL_ACCOUNT+" text,"
        +COL_ISREAD+" text);";
}
