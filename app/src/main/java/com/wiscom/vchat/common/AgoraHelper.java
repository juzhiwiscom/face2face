package com.wiscom.vchat.common;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.text.TextUtils;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.wiscom.vchat.C;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.AgoraChannelKey;
import com.wiscom.vchat.data.model.AgoraParams;
import com.wiscom.vchat.data.model.AgoraToken;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.AgoraEvent;
import com.wiscom.vchat.event.AgoraMediaEvent;
import com.wiscom.vchat.event.InviteSuccessEvent;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.parcelable.VideoParcelable;
import com.wiscom.vchat.ui.video.VideoInviteActivity;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.agora.AgoraAPIOnlySignal;
import io.agora.IAgoraAPI;
import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;

/**
 * 声网帮助类
 * Created by zhangdroid on 2017/6/14.
 */
public class AgoraHelper {
    private static final String TAG = AgoraHelper.class.getSimpleName();
    // 信令事件常量定义
    public static final int EVENT_CODE_INVITE_FAILED = 0;// 发起呼叫失败
    public static final int EVENT_CODE_INVITE_SUCCESS = 1;// 发起的呼叫已经接通
    public static final int EVENT_CODE_INVITE_CANCEL = 2;// 取消已经发起的呼叫
    public static final int EVENT_CODE_INVITE_ACCEPT = 3;// 对方接受呼叫
    public static final int EVENT_CODE_INVITE_REFUSE = 4;// 对方拒绝呼叫
    public static final int EVENT_CODE_INVITE_END_PEER = 5;// 发起方取消了呼叫
    public static final int EVENT_CODE_JOIN_CHANNEL_FAILED = 6;// 加入频道失败
    public static final int EVENT_CODE_LEAVE_CHANNEL_SELF = 7;// 自己离开频道
    public static final int EVENT_CODE_SEND_MSG_SUCCESS = 8;// 发送点对点消息成功
    public static final int EVENT_CODE_SEND_MSG_FAILED = 9;// 发送点对点消息失败
    public static final int EVENT_CODE_RECEIVE_MSG = 10;// 收到对方发送的消息
    // 媒体事件常量定义
    public static final int EVENT_CODE_SETUP_REMOTE_VIDEO = 0;// 创建远端视频
    public static final int EVENT_CODE_MEDIA_JOIN_SUCCESS = 1;// 成功加入视频
    public static final int EVENT_CODE_USER_OFFLINE = 2;// 视频中用户离线或掉线
    public static final int EVENT_CODE_VIDEO_ERROR = 3;// 视频中发生错误
    public static final int EVENT_CODE_VIDEO_USER_NO_CURRENT_VIEW = 4;// 对方不在随机视频的界面


    private static AgoraHelper sInstance;
    // 信令
    private static AgoraAPIOnlySignal sAgoraAPIOnlySignal;
    // 媒体
    private static RtcEngine sRtcEngine;

    private AgoraHelper() {
        // 创建AgoraAPIOnlySignal
        sAgoraAPIOnlySignal = AgoraAPIOnlySignal.getInstance(BaseApplication.getGlobalContext(),
                C.KEY_AGORA);
        sAgoraAPIOnlySignal.callbackSet(sCallBack);
        // 创建RtcEngine
        try {
            sRtcEngine = RtcEngine.create(BaseApplication.getGlobalContext(), C.KEY_AGORA, sRtcEngineEventHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 设置通信模式
        sRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
    }

    public static AgoraHelper getInstance() {
        if (null == sInstance) {
            synchronized (AgoraHelper.class) {
                if (null == sInstance) {
                    sInstance = new AgoraHelper();
                }
            }
        }
        return sInstance;
    }

    /**
     * 信令API回调
     */
    public static final IAgoraAPI.ICallBack sCallBack = new IAgoraAPI.ICallBack() {

        @Override
        public void onReconnecting(int i) {
            LogUtil.i(TAG, "onReconnecting#reconnect time is " + i);
        }

        @Override
        public void onReconnected(int i) {
            LogUtil.i(TAG, "onReconnected");
        }

        @Override
        public void onLoginSuccess(int i, int i1) {
            LogUtil.i(TAG, "onLoginSuccess");
        }

        @Override
        public void onLogout(int i) {
            LogUtil.i(TAG, "onLogout");
        }

        @Override
        public void onLoginFailed(int i) {
            LogUtil.i(TAG, "onLoginFailed#code = " + i);
        }

        @Override
        public void onChannelJoined(String s) {
            LogUtil.i(TAG, "onChannelJoined#channelID = " + s);
            // 加入频道成功
        }

        @Override
        public void onChannelJoinFailed(String s, int i) {
            LogUtil.i(TAG, "onChannelJoinFailed#channelID = " + s + " code = " + i);
            // 加入频道失败
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_JOIN_CHANNEL_FAILED);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onChannelLeaved(String s, int i) {
            LogUtil.i(TAG, "onChannelLeaved#channelID = " + s + " code = " + i);
            // 离开频道
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_LEAVE_CHANNEL_SELF);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onChannelUserJoined(String s, int i) {
            LogUtil.i(TAG, "onChannelUserJoined#account = " + s + " uid = " + i);
            // 其他用户加入频道
        }

        @Override
        public void onChannelUserLeaved(String s, int i) {
            LogUtil.i(TAG, "onChannelUserLeaved#account = " + s + " uid = " + i);
            // 其他用户离开频道
        }

        @Override
        public void onChannelUserList(String[] strings, int[] ints) {
        }

        @Override
        public void onChannelQueryUserNumResult(String s, int i, int i1) {
        }

        @Override
        public void onChannelAttrUpdated(String s, String s1, String s2, String s3) {
        }

        @Override
        public void onInviteReceived(String channelId, String s1, int i, String s2) {
            LogUtil.i(TAG, "onInviteReceived#channelId = " + channelId + " account = " + s1 + " userInfo = " + s2);
//           将声网中的接受邀请的方法onInviteReceived打开
            boolean booleanValue = SharedPreferenceUtil.getBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
            if(booleanValue){
//                用户正在视频中，其他邀请拒绝
                AgoraHelper.getInstance().refuseInvite(channelId, s1);
//                AgoraHelper.getInstance().leaveChannel(channelId);
            }else{
                // 收到对方呼叫邀请请求，此处显示被邀请页面
                AgoraParams agoraParams = new AgoraParams(0, "", "","","","");
                if (!TextUtils.isEmpty(s2)) {
                    agoraParams = new Gson().fromJson(s2, AgoraParams.class);
                }
//            判断是来自随机视频的邀请还是来自呼叫界面的邀请
                String fromView = agoraParams.getFromView();
//            频道id：channelId；对方账户；对方uid；对方昵称；对方头像
                VideoInviteParcelable videoInviteParcelable = new VideoInviteParcelable(true, agoraParams.getuId(), s1, agoraParams.getNickname(), agoraParams.getImgageUrl(),agoraParams.getCountry(),agoraParams.getGenter());
                videoInviteParcelable.setChannelId(channelId);
                String account = videoInviteParcelable.account;
                long uId = videoInviteParcelable.uId;
                String nickname = videoInviteParcelable.nickname;
                String imgageUrl = videoInviteParcelable.imgageUrl;
                String country = videoInviteParcelable.country;
                String genter = videoInviteParcelable.genter;
                if(fromView.equals(C.Video.GO_CALL)){
//                来自某个用户的呼叫界面
//                接受邀请，并跳转到被邀请界面
                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoInviteActivity.class, videoInviteParcelable);
                }else{
//                来自随机视频界面
//                判断当前界面是否是VideoActivity
                    ActivityManager am = (ActivityManager) BaseApplication.getGlobalContext().getSystemService(Context.ACTIVITY_SERVICE);
                    List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
                    if (!tasks.isEmpty()) {
                        ComponentName topActivity = tasks.get(0).topActivity;
                        if(topActivity.toString().contains("VideoActivity")){
                            AgoraHelper.getInstance().acceptInvite(channelId, s1);
                            AgoraHelper.getInstance().joinChannel(channelId);
//                    发送消息，finish调当前videoActivity界面
                            VideoParcelable videoParcelable = new VideoParcelable(uId, account, nickname, channelId, "1",country,genter,imgageUrl,C.Video.RANDOM_VIDEO);
                            EventBus.getDefault().post(new InviteSuccessEvent(videoParcelable));
//                    LaunchHelper.getInstance().launch(BaseApplication.getGlobalContext(), VideoActivity.class,
//                            new VideoParcelable(uId, account, nickname, s, "1"));

                        }else{
//                        用户不在随机视频的界面
//                       相当于拒绝邀请，离开频道
                            AgoraHelper.getInstance().refuseInvite(channelId, account);
                            AgoraHelper.getInstance().leaveChannel(channelId);
                        }
                    }
                }
            }

        }
        @Override
        public void onInviteReceivedByPeer(String s, String s1, int i) {
            LogUtil.i(TAG, "onInviteReceivedByPeer#channelId = " + s);
            // 对方收到呼叫邀请，此处表示呼叫成功
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_SUCCESS);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteAcceptedByPeer(String s, String s1, int i, String s2) {
            LogUtil.i(TAG, "onInviteAcceptedByPeer#channelId = " + s);
            // 对方接受呼叫邀请，此处开始视频（加入频道）
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_ACCEPT);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteRefusedByPeer(String s, String s1, int i, String s2) {
            LogUtil.i(TAG, "onInviteRefusedByPeer#channelId = " + s);
            // 对方拒绝呼叫邀请
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_REFUSE);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteFailed(String s, String s1, int i, int i1, String s2) {
            LogUtil.i(TAG, "onInviteFailed#channelId = " + s+"错误码："+i1);
            // 主动发起呼叫失败，此处关闭邀请页面并提示用户
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_FAILED);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteEndByPeer(String s, String s1, int i, String s2) {
            LogUtil.i(TAG, "onInviteEndByPeer#channelId = " + s);
            // 对方取消呼叫邀请，此处关闭被邀请页面
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_END_PEER);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteEndByMyself(String s, String s1, int i) {
            LogUtil.i(TAG, "onInviteEndByMyself#channelId = " + s);
            // 主动取消呼叫邀请，此处关闭邀请页面
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_INVITE_CANCEL);
            agoraEvent.setChannelId(s);
            agoraEvent.setAccount(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onInviteMsg(String s, String s1, int i, String s2, String s3, String s4) {
        }

        @Override
        public void onMessageSendError(String s, int i) {
            LogUtil.i(TAG, "onMessageSendError#messageID = " + s + " code = " + i);
            // 发送消息失败
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_SEND_MSG_FAILED);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageSendSuccess(String s) {
            LogUtil.i(TAG, "onMessageSendSuccess#messageID = " + s);
            // 发送消息成功
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_SEND_MSG_SUCCESS);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageAppReceived(String s) {
        }

        @Override
        public void onMessageInstantReceive(String s, int i, String s1) {
            LogUtil.i(TAG, "onMessageInstantReceive#account = " + s + " message = " + s1);
            // 收到消息
            AgoraEvent agoraEvent = new AgoraEvent();
            agoraEvent.setEventCode(EVENT_CODE_RECEIVE_MSG);
            agoraEvent.setMessage(s1);
            EventBus.getDefault().post(agoraEvent);
        }

        @Override
        public void onMessageChannelReceive(String s, String s1, int i, String s2) {
        }

        @Override
        public void onLog(String s) {
        }

        @Override
        public void onInvokeRet(String s, int i, String s1, String s2) {

        }

//        @Override
//        public void onInvokeRet(String s, int i, String s1, String s2) {
//        }

        @Override
        public void onMsg(String s, String s1, String s2) {
        }

        @Override
        public void onUserAttrResult(String s, String s1, String s2) {
        }

        @Override
        public void onUserAttrAllResult(String s, String s1) {
        }

        @Override
        public void onError(String s, int i, String s1) {
            LogUtil.i(TAG, "信令调用错误：s="+s+"--i=="+i+"--s1=="+s1);
        }

        @Override
        public void onQueryUserStatusResult(String s, String s1) {
        }

        @Override
        public void onDbg(String s, String s1) {

        }
//        @Override
//        public void onDbg(String s, String s1) {
//        }











    };

    /**
     * 媒体API回调
     */
    private static final IRtcEngineEventHandler sRtcEngineEventHandler = new IRtcEngineEventHandler() {

        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            LogUtil.i(TAG, "onJoinChannelSuccess#channelId = " + channel + " uid = " + uid);
            // 进入频道成功，此处开启视频轮询
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_MEDIA_JOIN_SUCCESS, 0));
        }

        @Override
        public void onLeaveChannel(RtcStats stats) {
            super.onLeaveChannel(stats);
            LogUtil.i(TAG, "离开频道成功");

        }

        @Override
        public void onError(int err) {
            super.onError(err);
            LogUtil.i(TAG, "视频中发生错误，错误码为err=" + err);
//            视频中发生错误
            if(err!=18){
//                err=18:离开频道错误，说明已经离开过了频道，不再发送消息
                EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_VIDEO_ERROR, 0));
            }
        }

        @Override
        public void onFirstRemoteVideoDecoded(int uid, int width, int height, int elapsed) {
            LogUtil.i(TAG, "onFirstRemoteVideoDecoded#uid = " + uid);
            // 初次解码，在这里初始化远端视频
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_SETUP_REMOTE_VIDEO, uid));
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
            LogUtil.i(TAG, "onUserOffline#uid = " + uid + " reason code " + reason);
            // 用户离线或掉线
            EventBus.getDefault().post(new AgoraMediaEvent(EVENT_CODE_USER_OFFLINE, uid));
        }
    };

    /**
     * 登录信令系统
     */
    public void login() {
        ApiManager.getAgoraToken(new IGetDataListener<AgoraToken>() {
            @Override
            public void onResult(AgoraToken agoraToken, boolean isEmpty) {
                if (null != agoraToken) {
                    String token = agoraToken.getToken();
                    if (!TextUtils.isEmpty(token)) {
                        // 登陆信令系统
                        if(sAgoraAPIOnlySignal!=null){
                            sAgoraAPIOnlySignal.login(C.KEY_AGORA, UserPreference.getAccount(), token, 0, null);
                        }else{
                            getInstance();
                            if(sAgoraAPIOnlySignal!=null){
                                sAgoraAPIOnlySignal.login(C.KEY_AGORA, UserPreference.getAccount(), token, 0, null);
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    /**
     * 登出声网信令系统
     */
    public void logout() {
        if (sAgoraAPIOnlySignal.isOnline() == 1) {
            sAgoraAPIOnlySignal.logout();
        }
    }

    /**
     * 发起邀请
     *
     * @param channelId 被邀请加入的频道id
     * @param account   被邀请人account
     * @param userInfo  被邀请人信息（JSON字符串）{@link AgoraParams}
     */
    public void startInvite(String channelId, String account, String userInfo) {
        LogUtil.i(TAG, "startInvite#channelId = " + channelId + " account = " + account + " userInfo = " + userInfo);
        // channelID使用邀请人id+被邀请人id组合形式
        if(sAgoraAPIOnlySignal==null)
            getInstance();
        sAgoraAPIOnlySignal.channelInviteUser2(channelId, account, userInfo);
    }

    /**
     * 结束邀请
     *
     * @param channelId 被取消邀请加入的频道id
     * @param account   被邀请人account
     */
    public void stopInvite(String channelId, String account) {
        LogUtil.i(TAG, "stopInvite#channelId = " + channelId + " account = " + account);
        // channelID使用邀请人id+被邀请人id组合形式
        if(sAgoraAPIOnlySignal==null)
            getInstance();
        sAgoraAPIOnlySignal.channelInviteEnd(channelId, account, 0);
    }

    /**
     * 接受邀请
     *
     * @param channelId 频道id
     * @param account   邀请人的account
     */
    public void acceptInvite(String channelId, String account) {
        LogUtil.i(TAG, "acceptInvite#channelId = " + channelId + " account = " + account);
        if(sAgoraAPIOnlySignal==null)
            getInstance();
        sAgoraAPIOnlySignal.channelInviteAccept(channelId, account, 0);
    }

    /**
     * 拒绝邀请
     *
     * @param channelId 频道id
     * @param account   邀请人的account
     */
    public void refuseInvite(String channelId, String account) {
        LogUtil.i(TAG, "refuseInvite#channelId = " + channelId + " account = " + account);
        if(sAgoraAPIOnlySignal==null)
            getInstance();
        sAgoraAPIOnlySignal.channelInviteRefuse(channelId, account, 0, null);
    }

    /**
     * 信令系统发送点对点消息
     *
     * @param account 接受消息方account
     * @param msg     消息内容
     */
    public void sendTextMessage(String account, String msg) {
        if(sAgoraAPIOnlySignal==null)
            getInstance();
        sAgoraAPIOnlySignal.messageInstantSend(account, 0, msg, "0");
    }

    /**
     * 加入指定的频道
     *
     * @param channelId 频道id
     */
    public void joinChannel(final String channelId) {
        LogUtil.i(TAG, "joinChannel#channelId = " + channelId);
        System.out.print("errerrerr==joinChannel="+channelId);
        sAgoraAPIOnlySignal.channelJoin(channelId);
        ApiManager.getAgoraChannelKey(channelId, new IGetDataListener<AgoraChannelKey>() {
            @Override
            public void onResult(AgoraChannelKey agoraChannelKey, boolean isEmpty) {
                if (null != agoraChannelKey) {
                    String channelKey = agoraChannelKey.getKey();
                    if (!TextUtils.isEmpty(channelKey)) {
                        // 此处第四个参数必须为用户guid，后台生成密钥时使用的也是guid，保持一致
                        int code = sRtcEngine.joinChannel(channelKey, channelId, null, Integer.parseInt(UserPreference.getId()));
                        LogUtil.i(TAG, "joinChannel#code = " + RtcEngine.getErrorDescription(code));
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                sAgoraAPIOnlySignal.channelLeave(channelId);
                // 发送事件，提示用户加入频道失败
            }
        });
    }

    /**
     * 退出指定的频道
     *
     * @param channelId 频道id
     */
    public void leaveChannel(String channelId) {
        LogUtil.i(TAG, "leaveChannel#channelId = " + channelId);
        sAgoraAPIOnlySignal.channelLeave(channelId);
        sRtcEngine.leaveChannel();
//                    将声网中的接受邀请的方法onInviteReceived打开
        SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(),"VideoSuccess","closeReceiveInviter",false);
//        sRtcEngine.stopPreview();
    }
    /**
     * 退出视频预览
     *
     */
    public void stopPreview() {
        sRtcEngine.stopPreview();

    }

    /**
     * 设置本地视频
     *
     * @param context             上下文对象
     * @param localVideoContainer 包含显示本地视频的容器
     */
    public void setupLocalVideo(boolean isStick,Context context, FrameLayout localVideoContainer) {
        LogUtil.i(TAG, "setupLocalVideo");
        // 设置视频属性并开启视频
        if(sRtcEngine!=null){
            sRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_720P, false);
            sRtcEngine.enableVideo();
            // 创建SurfaceView并设置本地视频
            SurfaceView surfaceView = RtcEngine.CreateRendererView(context);
            localVideoContainer.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT));
            if(isStick){
                //surfceview放置在顶层，即始终位于最上层
                surfaceView.setZOrderOnTop(true);
            }
            sRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, 0));
            sRtcEngine.startPreview();
        }else{
            // 创建AgoraAPIOnlySignal
            sAgoraAPIOnlySignal = AgoraAPIOnlySignal.getInstance(BaseApplication.getGlobalContext(),
                    C.KEY_AGORA);
            sAgoraAPIOnlySignal.callbackSet(sCallBack);
            // 创建RtcEngine
            try {
                sRtcEngine = RtcEngine.create(BaseApplication.getGlobalContext(), C.KEY_AGORA, sRtcEngineEventHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 设置通信模式
            sRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
            sRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_720P, false);
            sRtcEngine.enableVideo();
            // 创建SurfaceView并设置本地视频
            SurfaceView surfaceView = RtcEngine.CreateRendererView(context);
            localVideoContainer.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT));
            sRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, 0));
            sRtcEngine.startPreview();
        }

    }

    /**
     * 创建远端视频
     *
     * @param context              上下文对象
     * @param remoteVideoContainer 包含显示远端视频的容器
     * @param uId
     */
    public void setupRemoteVideo(Context context, FrameLayout remoteVideoContainer, int uId) {
        LogUtil.i(TAG, "setupRemoteVideo#uId = " + uId);
        if (remoteVideoContainer.getChildCount() >= 1) {
            return;
        }
        SurfaceView surfaceView = RtcEngine.CreateRendererView(context);
        remoteVideoContainer.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        sRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_ADAPTIVE, uId));
        surfaceView.setTag(uId); // 设置tag
    }

    /**
     * 切换摄像头
     */
    public void switchCamera() {
        sRtcEngine.switchCamera();
    }

    /**
     * 销毁对象，释放资源
     */
    public void destoryRtcEngine() {
        RtcEngine.destroy();
        sAgoraAPIOnlySignal = null;
        sRtcEngine = null;
    }
    /**
     * 销毁对象，释放本地视频
     */
    public void destoryEngine() {
        RtcEngine.destroy();
        sAgoraAPIOnlySignal = null;
        sRtcEngine = null;
    }

}
