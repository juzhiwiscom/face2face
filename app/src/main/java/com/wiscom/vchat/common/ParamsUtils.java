package com.wiscom.vchat.common;

import android.text.TextUtils;

import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.library.util.LogUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/28.
 */

public class ParamsUtils {
    /**
     * 将map的key排序后，生成String
     *
     * @param map
     */
    private static String getMapValue(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String value = iterator.next().getValue();
            if(!TextUtils.isEmpty(value)){
                return value;
            }else{

                return null;
            }
    }
    /**
     * 将map的valuse排序后，生成String
     *
     * @param map
     */
    private static String getMapKey(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        String key = iterator.next().getKey();
            if(!TextUtils.isEmpty(key)){
                return key;
            }else{

                return null;
            }
    }
    /**
     * 从map集合中获取listist集合生成list
     */
    public static List<String> getMapListString(List<Map<String,String>> list){
        List<String> list2=new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Map<String, String> stringStringMap = list.get(i);
            String mapValue = getMapValue(stringStringMap);
            list2.add(mapValue);
        }
        return list2;
    }
    /**
     * 根据map集合的value获取国家的key
     */
    public static String getCountryKey(String country){
        String mapKeyByValue=null;
        List<Map<String, String>> countryString = Util.getCountryString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if(getMapValue(stringStringMap).equals(country)){
                mapKeyByValue = iterator.next().getKey();
            }
        }
        return mapKeyByValue;
    }
    /**
     * 根据map集合的key获取国家的value
     */
    public static String getCountryValues(String country){
        int countryNum = 79;
        try {
            countryNum = Integer.parseInt(country);
        }catch (Exception e){

        }
        if(countryNum>58){
            country=countryNum+1+"";
        }
        String mapValueBykey=null;
        List<Map<String, String>> countryString = Util.getCountryString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if(getMapKey(stringStringMap).equals(country)){
                mapValueBykey = iterator.next().getValue();
                UserPreference.setCountry_id(country);
//                LogUtil.v("getCountryValues==","key="+getMapKey(stringStringMap)+"~~Currentvalue============value="+mapValueBykey);
            }
//            else{
//                LogUtil.v("getCountryValues==","key="+getMapKey(stringStringMap)+"~~value="+iterator.next().getValue());
//            }
        }
        if(mapValueBykey!=null){
            String LG = Locale.getDefault().getLanguage();
            if(LG.equals("zh")){
//            将英文转成中文
                String localeCountry = Locale.getDefault().getCountry();
                for (int i = 0; i < Util.getEnglishCountryList().size(); i++) {
                    if(mapValueBykey.equals(Util.getEnglishCountryList().get(i))){
                        mapValueBykey = Util.getCountryEasyList().get(i);
                    }
                }
//                加入繁体字时添加下面内容
            if(localeCountry.equals("TW")){
//                说明是繁体
                for (int i = 0; i < Util.getCountryComplexList().size(); i++) {
                    if(mapValueBykey.equals(Util.getCountryComplexList().get(i))){
                        mapValueBykey = Util.getEnglishCountryList().get(i);
                    }
                }
            }
            }
        }
        return mapValueBykey;
    }
    /**
     * 根据map集合的key获取语言的value
     */
    public static String getLanguageValues(String country){
        StringBuffer sb=new StringBuffer();
        List<Map<String, String>> countryString = Util.getLanguageString();
        if(country.contains(",")){
            List<String> languageList = getLanguageList(country);
            for (String languageKey : languageList) {
                for (int i = 0; i < countryString.size(); i++) {
                    Map<String, String> stringStringMap = countryString.get(i);
                    Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
                    if(getMapKey(stringStringMap).equals(languageKey)){
                        sb.append(iterator.next().getValue()).append(",");
                    }
                }
            }
        }else{
            for (int i = 0; i < countryString.size(); i++) {
                Map<String, String> stringStringMap = countryString.get(i);
                Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
                if(getMapKey(stringStringMap).equals(country)){
                    sb.append(iterator.next().getValue()).append(",");
                }
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }
    /**
     * 根据选择的语言获取对应的key
     */
    public static String getLanguageListKey(List<String> list){
        StringBuffer sb=new StringBuffer();
        List<Map<String, String>> countryString = Util.getLanguageString();
        for (String language : list) {
            for (int i = 0; i < countryString.size(); i++) {
                Map<String, String> stringStringMap = countryString.get(i);
                Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
                if(getMapValue(stringStringMap).equals(language)){
                    sb.append(iterator.next().getKey()).append(",");
                }
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }
    /**
     * 根据选择的语言获取对应的key初始化的時候
     */
    public static String getLanguageListString(String language){
        StringBuffer sb=new StringBuffer();
        List<Map<String, String>> countryString = Util.getLanguageString();
        if(language.contains(",")){
            List<String> languageList = getLanguageList(language);
            for (String s : languageList) {
                for (int i = 0; i < countryString.size(); i++) {
                    Map<String, String> stringStringMap = countryString.get(i);
                    Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
                    if(getMapValue(stringStringMap).equals(s)){
                        sb.append(iterator.next().getKey()).append(",");
                    }
                }
            }
        }else{
            for (int i = 0; i < countryString.size(); i++) {
                Map<String, String> stringStringMap = countryString.get(i);
                Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
                if(getMapValue(stringStringMap).equals(language)){
                    sb.append(iterator.next().getKey()).append(",");
                }
            }
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }
    /**
     * 根据map集合的value获取州的key
     */
    public static String getStateKey(String sate){
        String mapKeyByValue=null;
        List<Map<String, String>> countryString = Util.getStateString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if(getMapValue(stringStringMap).equals(sate)){
                mapKeyByValue = iterator.next().getKey();
            }
        }
        return mapKeyByValue;
    }
    /**
     * 根据map集合的key获取州的value
     */
    public static String getStateValue(String sate){
        String mapKeyByValue=null;
        List<Map<String, String>> countryString = Util.getStateString();
        for (int i = 0; i < countryString.size(); i++) {
            Map<String, String> stringStringMap = countryString.get(i);
            Iterator<Map.Entry<String, String>> iterator = stringStringMap.entrySet().iterator();
            if(getMapKey(stringStringMap).equals(sate)){
                mapKeyByValue = iterator.next().getValue();
                LogUtil.v("getStateValues==","key="+getMapKey(stringStringMap)+"~~Currentvalue============value="+mapKeyByValue);

            }else{
                LogUtil.v("getStateValues==","key="+getMapKey(stringStringMap)+"~~value="+iterator.next().getValue());

            }
        }
        return mapKeyByValue;
    }
    /**
     * 把带有逗号的String字符串转成List集合
     */
    public static List<String> getLanguageList(String languagekey){
        List<String> list=new ArrayList<>();
        String[] split = languagekey.split(",");
        for (int i = 0; i < split.length; i++) {
            list.add(split[i]);
        }
        return list;
    }
}
