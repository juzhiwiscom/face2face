package com.wiscom.vchat.common;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.model.Country;
import com.wiscom.vchat.data.model.LocationInfo;
import com.wiscom.vchat.data.model.State;
import com.wiscom.vchat.data.model.UploadInfoParams;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.data.model.VedioScreenInfo;
import com.wiscom.vchat.data.preference.PlatformPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.library.util.Utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * 公用工具类
 * Created by zhangdroid on 2017/6/3.
 */
public class Util {

    private static String country_id;

    private Util() {
    }

//    /**
//     * 将List<UserPhoto>转成List<String>
//     *
//     * @param list
//     * @return
//     */
//    public static List<String> convertPhotoUrl(List<UserPhoto> list) {
//        List<String> stringList = new ArrayList<>();
//        if (!Utils.isListEmpty(list)) {
//            for (UserPhoto item : list) {
//                if (null != item) {
//                    stringList.add(item.getFileUrl());
//                }
//            }
//        }
//        return stringList;
//    }
//
//    /**
//     * 获得UploadInfoParamsJSON字符串
//     */
//    public static String getUploadInfoParamsJsonString(UploadInfoParams uploadInfoParams) {
//        // 创建以uploadInfoParams为Key的JSON字符串
//        Map<String, UploadInfoParams> map = new HashMap<>();
//        map.put("uploadInfoParams", uploadInfoParams);
//        return new Gson().toJson(map);
//    }
    /**
     * 随机视频传入的筛选条件（暂时性别）
     */
    public static String getScreenInfo(VedioScreenInfo screenInfo) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, VedioScreenInfo> map = new HashMap<>();
        map.put("matchCriteria", screenInfo);
        return new Gson().toJson(map);
    }

//    /**
//     * 获取汉字字符串的汉语拼音，英文字符不变
//     */
//    public static String getPinYin(String chines) {
//        StringBuffer sb = new StringBuffer();
//        sb.setLength(0);
//        char[] nameChar = chines.toCharArray();
//        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
//        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
//        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
//        for (int i = 0; i < nameChar.length; i++) {
//            if (nameChar[i] > 128) {
//                try {
//                    sb.append(PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat)[0]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                sb.append(nameChar[i]);
//            }
//        }
//        return sb.toString();
//    }

    /**
     * 计时：将秒数转换成时间字符串
     *
     * @param seconds
     * @return
     */
//    public static String convertSecondsToString(int seconds) {
//        StringBuilder stringBuilder = new StringBuilder();
//        if (seconds < 60) {// 一分钟内
//            stringBuilder.append("00:")
//                    .append(pad(seconds));
//        } else if (seconds >= 60 && seconds < 60 * 60) {// 1小时内
//            stringBuilder.append(pad(seconds / 60))
//                    .append(":")
//                    .append(pad(seconds % 60));
//        } else if (seconds >= 60 * 60 && seconds < 24 * 60 * 60) {// 1天内
//            stringBuilder.append(pad(seconds / 60 * 60))
//                    .append(":")
//                    .append(pad((seconds % 60 * 60) / 60))
//                    .append(":")
//                    .append(pad((seconds % 60 * 60) % 60));
//        }
//        return stringBuilder.toString();
//    }

//    /**
//     * 小于10的数前面补0
//     *
//     * @param number
//     * @return
//     */
//    public static String pad(int number) {
//        if (number < 10) {
//            return "0" + number;
//        } else {
//            return String.valueOf(number);
//        }
//    }
//    /**
//     * 读取assets目录下的json文件
//     *
//     * @param context  上下文对象
//     * @param fileName assets目录下的json文件路径
//     */
//    public static String getJsonStringFromAssets(Context context, String fileName) {
//        String jsonStr = null;
//        if (!TextUtils.isEmpty(fileName)) {
//            BufferedReader bufferedReader = null;
//            try {
//                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
//                String line;
//                StringBuilder buffer = new StringBuilder();
//                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
//                    buffer.append(line);
//                }
//                jsonStr = buffer.toString();
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    if (bufferedReader != null) {
//                        bufferedReader.close();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return jsonStr;
//    }
//    /**
//     * 从assets目录下读取国家和语音 json数据结构是一样的，因此就写成Country一个类
//     */
//    public static List<Country> getCountryFromJson(String fileName) {
//        // 解析国家json文件
//        List<Country> countryStateList =stringToList(getJsonStringFromAssets(getContext(),fileName),Country.class);
//        if (countryStateList!=null) {
//        return  countryStateList;
//        }
//        return null;
//    }
//    /**
//     * 从assets目录下读取州信息
//     */
//    public static List<State> getStateFromJson(String fileName) {
//        // 解析国家json文件
//        List<State> countryStateList =stringToList(getJsonStringFromAssets(getContext(),fileName),State.class);
//        if (countryStateList!=null) {
//        return  countryStateList;
//        }
//        return null;
//    }

//    /**
//     * 获取所有国家的数据
//     * @return
//     */
//    public static List<String> getCountryString(){
//        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
//        List<String> list=new ArrayList<>();
//        if (!TextUtils.isEmpty(userCountry) && "America".equals(userCountry)) {
//            List<Country> countryFromJson = Util.getCountryFromJson("CountryEnglish.json");
//            for (Country country : countryFromJson) {
//                list.add(country.getName());
//            }
//        }else{
//            List<Country> countryFromJson = Util.getCountryFromJson("CountryChinese.json");
//            for (Country country : countryFromJson) {
//                list.add(country.getName());
//            }
//        }
//        return list;
//    }
    /**
     * 根据后台给的国家编号，获取国家
     * @return
     */
    public static String getCountry(String countryNum) {
            List<String> list=new ArrayList<>();
            countryNum=Integer.parseInt(countryNum)+"";
            String country=null;
            List<Country> countryFromJson = Util.getCountryFromJson("CountryEnglish.json");
            for (Country countryList : countryFromJson) {
                if(countryList.getGuid().equals(countryNum)){
                    country=countryList.getName();
                    UserPreference.setCountry_id(countryList.getGuid());
                }
            }
        return country;
    }


//    /**
//     * 获取所有语言的数据
//     * @return
//     */
//    public static List<String> getLanguageString(){
//        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
//        List<String> list=new ArrayList<>();
//        if (!TextUtils.isEmpty(userCountry) && "America".equals(userCountry)) {
//            List<Country> countryFromJson = Util.getCountryFromJson("LanguageEnglish.json");
//            for (Country country : countryFromJson) {
//                list.add(country.getName());
//            }
//        }else{
//            List<Country> countryFromJson = Util.getCountryFromJson("LanguageChinese.json");
//            for (Country country : countryFromJson) {
//                list.add(country.getName());
//            }
//        }
//        return list;
//    }

//    public static Context getContext() {
//        return BaseApplication.getGlobalContext();
//    }
//    public static boolean isListEmpty(List<?> list) {
//        return list == null || list.size() == 0;
//    }

//    /**
//     * 通过gson把json字符串转成list集合
//     * @param json
//     * @param cls
//     * @param <T>
//     * @return
//     */
//    public static <T>  List<T> stringToList(String json ,Class<T> cls  ){
//        Gson gson = new Gson();
//        List<T> list = new ArrayList<T>();
//        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
//        for(final JsonElement elem : array){
//            list.add(gson.fromJson(elem, cls));
//        }
//        return list ;
//    }
//    public static String getLacalCountry(){
//        String country=null;
//        if(UserPreference.getCountry()!=null){
//            country=UserPreference.getCountry();
//        }else{
//            String CT = Locale.getDefault().getCountry();
//            switch (CT){
//                case "TW":
//                    country = "Taiwan";
//                    break;
//                case "HK":
//                    country = "Hong Kong";
//                    break;
//                case "CN":
//                    country = "China";
//                    break;
//                case "US":
//                    country = "America";
//                    break;
//
//            }
//        }
//
//        return country;
//    }
//    public static String getLacalLanguage(){
//        String language=null;
//        String LG = Locale.getDefault().getLanguage();
//        switch (LG){
//            case "zh":
//                language = "Chinese";
//                break;
//            case "en":
//                language = "English";
//                break;
//
//        }
//        return language;
//    }
    /**
     * 根据国家获得国旗列表
     *
     * @return
     */
    public static int getBanner(String country) {
        String LG = Locale.getDefault().getLanguage();
        if(LG.equals("zh")){
//            将中文转成英文
            String locatyCountry = Locale.getDefault().getCountry();
            if(locatyCountry.equals("TW")){
//                说明是繁体
                for (int i = 0; i < getCountryComplexList().size(); i++) {
                    if(country.equals(getCountryComplexList().get(i))){
                        country = getEnglishCountryList().get(i);
                    }
                }
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if(country.equals(getCountryEasyList().get(i))){
                        country = getEnglishCountryList().get(i);
                    }
                }
            }else {
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if(country.equals(getCountryEasyList().get(i))){
                        country = getEnglishCountryList().get(i);
                    }
                }
            }
        }
        if(TextUtils.isEmpty(country)){
            return R.mipmap.banner_us;
        }
       if(country.equals("United States")||country.equals("America")){
           return R.mipmap.banner_us;
       }else if(country.equals("Australia")){
           return R.mipmap.banner_aus;
       }else if(country.equals("India")){
           return R.mipmap.banner_india;
       }else if(country.equals("Indonesia")){
           return R.mipmap.banner_indon;
       }else if(country.equals("United Kingdom")){
           return R.mipmap.banner_uk;
       }else if(country.equals("Canada")){
           return R.mipmap.banner_can;
       }else if(country.equals("New Zealand")){
           return R.mipmap.banner_nz;
       }else if(country.equals("Ireland")){
           return R.mipmap.banner_ire;
       }else if(country.equals("South Africa")){
           return R.mipmap.banner_sa;
       }else if(country.equals("Singapore")){
           return R.mipmap.banner_sin;
       }else if(country.equals("Pakistan")){
           return R.mipmap.banner_pak;
       }else if(country.equals("Philippines")){
           return R.mipmap.banner_phi;
       }else if(country.equals("Hong Kong")){
           return R.mipmap.banner_hk;
       } else if(country.equals("Tai Wan")){
           return R.mipmap.banner_tw;
       }else if(country.equals("China")){
           return R.mipmap.banner_china;
       }
        return R.mipmap.banner_us;
    }
    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getEnglishCountryList() {
        List<String> list = new ArrayList<String>();

//        list.add("America");
        list.add("United States");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("United Kingdom");
        list.add("Canada");
        list.add("New Zealand");
        list.add("Ireland");
        list.add("South Africa");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("Hong Kong");
        list.add("Tai Wan");
        list.add("China");

        return list;
    }
    /**
     * 获得繁体字的国家列表
     *
     * @return
     */
    public static List<String> getCountryComplexList() {
        List<String> list = new ArrayList<String>();
        list.add("美國");
        list.add("澳大利亞");
        list.add("印度");
        list.add("印尼");
        list.add("英國");
        list.add("加拿大");
        list.add("新西蘭");
        list.add("愛爾蘭");
        list.add("南非");
        list.add("新加坡");
        list.add("巴基斯坦");
        list.add("菲律賓");
        list.add("香港");
        list.add("台灣");
        list.add("中國");
        return list;
    }
    /**
     * 获得简体中文的国家列表
     *
     * @return
     */
    public static List<String> getCountryEasyList() {
        List<String> list = new ArrayList<String>();
        list.add("美国");
        list.add("澳大利亚");
        list.add("印度");
        list.add("印尼");
        list.add("英国");
        list.add("加拿大");
        list.add("新西兰");
        list.add("爱尔兰");
        list.add("南非");
        list.add("新加坡");
        list.add("巴基斯坦");
        list.add("菲律宾");
        list.add("香港");
        list.add("台湾");
        list.add("中国");
        return list;
    }
    /**
     * 获得所有的国家列表
     *
     * @return
     */
    public static List<String> getAllCountryList() {
        List<String> list = new ArrayList<String>();
        list.add("United States");
        list.add("Australia");
        list.add("India");
        list.add("Indonesia");
        list.add("United Kingdom");
        list.add("Canada");
        list.add("New Zealand");
        list.add("Ireland");
        list.add("South Africa");
        list.add("Singapore");
        list.add("Pakistan");
        list.add("Philippines");
        list.add("Hong Kong");
        list.add("Tai Wan");
        list.add("China");
        list.add("美国");
        list.add("澳大利亚");
        list.add("印度");
        list.add("印尼");
        list.add("英国");
        list.add("加拿大");
        list.add("新西兰");
        list.add("爱尔兰");
        list.add("南非");
        list.add("新加坡");
        list.add("巴基斯坦");
        list.add("菲律宾");
        list.add("香港");
        list.add("台湾");
        list.add("中国");
        list.add("美國");
        list.add("澳大利亞");
        list.add("印度");
        list.add("印尼");
        list.add("英國");
        list.add("加拿大");
        list.add("新西蘭");
        list.add("愛爾蘭");
        list.add("南非");
        list.add("新加坡");
        list.add("巴基斯坦");
        list.add("菲律賓");
        list.add("香港");
        list.add("台灣");
        list.add("中國");
        return list;
    }
    /**
     * 获取英语国家
     *
     * @return
     */
    public static String setEnglishCountry(String locationCountry) {
        String LG = Locale.getDefault().getLanguage();
        if (LG.equals("zh")) {
//            将中文转成英文
            String country = Locale.getDefault().getCountry();
            if (country.equals("TW")) {
//                说明是繁体
                for (int i = 0; i < getCountryComplexList().size(); i++) {
                    if (locationCountry.equals(getCountryComplexList().get(i))) {
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if (locationCountry.equals(getCountryEasyList().get(i))) {
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
            } else {
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if (locationCountry.equals(getCountryEasyList().get(i))) {
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
            }
        }
        return locationCountry;
    }
    /**
     * 获取不同语言的国家转换为本地语言的国家
     *
     * @return
     */
    public static String getLacalCountry(String locationCountry) {
        if(locationCountry!=null){
            String LG = Locale.getDefault().getLanguage();
            if (LG.equals("zh")) {
                String country = Locale.getDefault().getCountry();
                if (country.equals("TW")) {
//                说明本地语言是繁体
                    for (int i = 0; i < getAllCountryList().size(); i++) {
                        if(locationCountry.equals(getAllCountryList().get(i))){
                            if(i<15){
//                    将英语转换成繁体语言
                                locationCountry= getAllCountryList().get(i+30);
                            }else if(i>14 && i<30){
//                            将简体中文转成繁体
                                locationCountry=getAllCountryList().get(i+15);
                            }
                        }
                    }
                } else {
//                说明本地语言是简体
                    for (int i = 0; i < getAllCountryList().size(); i++) {
                        if(locationCountry.equals(getAllCountryList().get(i))){
                            if(i<15){
//                    将英语转换成简体语言
                                locationCountry= getAllCountryList().get(i+15);
                            }else if(i>29){
//                            将繁体中文转成简体
                                locationCountry=getAllCountryList().get(i-15);
                            }
                        }
                    }
                }
            }else{
//                说明本地语言是英语
                for (int i = 0; i < getAllCountryList().size(); i++) {
                    if(locationCountry.equals(getAllCountryList().get(i))){
                        if(i>14 && i<30){
//                    将简体转换成英语语言
                            locationCountry= getAllCountryList().get(i-15);
                        }else if(i>29){
//                            将繁体中文转成英语
                            locationCountry=getAllCountryList().get(i-30);
                        }
                    }
                }
            }
        }

        return locationCountry;
    }
    public static void setCountryAndFid(String locationCountry) {
        String LG = Locale.getDefault().getLanguage();
        if(LG.equals("zh")){
//            将中文转成英文
            String country = Locale.getDefault().getCountry();
//            for (int i = 0; i < getCountryEasyList().size(); i++) {
//                if(locationCountry.equals(getCountryEasyList().get(i))){
//                    locationCountry = getEnglishCountryList().get(i);
//                }
//            }
            if(country.equals("TW")){
//                说明是繁体
                for (int i = 0; i < getCountryComplexList().size(); i++) {
                    if(locationCountry.equals(getCountryComplexList().get(i))){
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if(locationCountry.equals(getCountryEasyList().get(i))){
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
            }else {
                for (int i = 0; i < getCountryEasyList().size(); i++) {
                    if(locationCountry.equals(getCountryEasyList().get(i))){
                        locationCountry = getEnglishCountryList().get(i);
                    }
                }
            }
        }
        if (locationCountry.equals("America") || locationCountry.equals("United States")) {
            UserPreference.setFid(F_IDAM);// 美国渠道号
        } else if (locationCountry.equals("India")) {
            UserPreference.setFid(F_IDIN);// 印度渠道号
        } else if (locationCountry.equals("Australia")) {
            UserPreference.setFid(F_IDNSW);// 澳大利亚
        }else if (locationCountry.equals("Indonesia")) {
            UserPreference.setFid(F_IDJT);// 印尼
        }else if (locationCountry.equals("United Kingdom")||locationCountry.equals("UnitedKingdom")) {
            UserPreference.setFid(F_IDGL);// 英国
        }else if (locationCountry.equals("Canada")) {
            UserPreference.setFid(F_IDTO);// 加拿大渠道号
        }else if (locationCountry.equals("NewZealand")) {
            UserPreference.setFid(F_IDAL);// 新西兰渠道号
        }else if (locationCountry.equals("Ireland")) {
            UserPreference.setFid(F_IDDL);// 爱尔兰渠道号
        }else if (locationCountry.equals("SouthAfrica")||locationCountry.equals("South Africa")) {
            UserPreference.setFid(F_IDJG);// 南非渠道号
        }else if (locationCountry.equals("Singapore")) {
            UserPreference.setFid(F_IDOD);// 新加坡渠道号
        }else if (locationCountry.equals("Pakistan")) {
            UserPreference.setFid(F_IDKC);// 巴基斯坦渠道号
        }else if (locationCountry.equals("Philippines")) {
            UserPreference.setFid(F_IDMA);// 菲律宾渠道号
        }else if (locationCountry.equals("HongKong")||locationCountry.equals("Hong Kong")) {
            UserPreference.setFid(F_IDWC);// 香港渠道号
        }else if (locationCountry.equals("TaiWan")||locationCountry.equals("Tai Wan")) {
            UserPreference.setFid(F_IDTW);// 台湾渠道号
        }else if (locationCountry.equals("China")) {
            UserPreference.setFid(F_IDCH);// 中国渠道号
        }

    }
    /**
     * 渠道号
     */
    public static final String FID = "11154";
    /**
     * 渠道号美国
     */
    public static String F_IDAM = "30201";
    /**
     * 渠道号 印度
     */
    static String F_IDIN = "30212";
    /*
    * 澳大利亚
    * */
    static String F_IDNSW="30211";
    /*
    * 印尼
    * */
    static String F_IDJT="30213";
    /*
    * 英国
    * */
    static String F_IDGL="30214";
    /*
    * 加拿大
    * */
    static String F_IDTO="30215";
    /*
    * 新西兰
    * */
    static String F_IDAL="30216";
    /*
    * 爱尔兰
    * */
    static String F_IDDL="30217";
    /*
    * 南非
    * */
    static String F_IDJG="30218";
    /*
    * 新加坡
    * */
    static String F_IDOD="30220";
    /*
    * 巴基斯坦
    * */
    static String F_IDKC="30224";
    /*
    * 菲律宾
    * */
    static String F_IDMA="30225";
    /*
    * 香港
    * */
    static String F_IDWC="30233";
    /*
    * 台湾
    * */
    static String F_IDTW="30208";
    /*
    * 中国
    * */
    static String F_IDCH="30200";




    /**
     * 将List<UserPhoto>转成List<String>
     *
     * @param list
     * @return
     */
    public static List<String> convertPhotoUrl(List<UserPhoto> list) {
        List<String> stringList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (UserPhoto item : list) {
                if (null != item) {
                    stringList.add(item.getFileUrl());
                }
            }
        }
        return stringList;
    }

    /**
     * 获得UploadInfoParamsJSON字符串
     */
    public static String getUploadInfoParamsJsonString(UploadInfoParams uploadInfoParams) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, UploadInfoParams> map = new HashMap<>();
        map.put("uploadInfoParams", uploadInfoParams);
        return new Gson().toJson(map);
    }
    /**
     * 获得定位信息
     */
    public static String getUploadLocation(LocationInfo uploadInfoParams) {
        // 创建以uploadInfoParams为Key的JSON字符串
        Map<String, LocationInfo> map = new HashMap<>();
        map.put("locationInfo", uploadInfoParams);
        return new Gson().toJson(map);
    }

    /**
     * 获取汉字字符串的汉语拼音，英文字符不变
     */
    public static String getPinYin(String chines) {
        StringBuffer sb = new StringBuffer();
        sb.setLength(0);
        char[] nameChar = chines.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < nameChar.length; i++) {
            if (nameChar[i] > 128) {
                try {
                    sb.append(PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat)[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                sb.append(nameChar[i]);
            }
        }
        return sb.toString();
    }

    /**
     * @return 随机返回默认图片
     */
    public static int getDefaultImage() {
//        int[] defaultImgs = {R.drawable.default_bear, R.drawable.default_dog, R.drawable.default_owl,
//                R.drawable.default_penguin, R.drawable.default_pig};
        int[] defaultImgs = {R.drawable.default_bear, R.drawable.default_bear, R.drawable.default_bear,
                R.drawable.default_bear, R.drawable.default_bear};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * @return 随机返回默认圆形图片
     */
    public static int getDefaultImageCircle() {
//        int[] defaultImgs = {R.drawable.default_bear_circle, R.drawable.default_dog_circle, R.drawable.default_owl_circle,
//                R.drawable.default_penguin_circle, R.drawable.default_pig_circle};
        int[] defaultImgs = {R.drawable.default_bear_circle, R.drawable.default_bear_circle, R.drawable.default_bear_circle,
                R.drawable.default_bear_circle, R.drawable.default_bear_circle};
        return defaultImgs[new Random().nextInt(defaultImgs.length)];
    }

    /**
     * 计时：将秒数转换成时间字符串
     *
     * @param seconds
     * @return
     */
    public static String convertSecondsToString(int seconds) {
        StringBuilder stringBuilder = new StringBuilder();
        if (seconds < 60) {// 一分钟内
            stringBuilder.append("00:")
                    .append(pad(seconds));
        } else if (seconds >= 60 && seconds < 60 * 60) {// 1小时内
            stringBuilder.append(pad(seconds / 60))
                    .append(":")
                    .append(pad(seconds % 60));
        } else if (seconds >= 60 * 60 && seconds < 24 * 60 * 60) {// 1天内
            stringBuilder.append(pad(seconds / 60 * 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) / 60))
                    .append(":")
                    .append(pad((seconds % 60 * 60) % 60));
        }
        return stringBuilder.toString();
    }

    /**
     * 小于10的数前面补0
     *
     * @param number
     * @return
     */
    public static String pad(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }
    /**
     * 读取assets目录下的json文件
     *
     * @param context  上下文对象
     * @param fileName assets目录下的json文件路径
     */
    public static String getJsonStringFromAssets(Context context, String fileName) {
        String jsonStr = null;
        if (!TextUtils.isEmpty(fileName)) {
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
                String line;
                StringBuilder buffer = new StringBuilder();
                while (!TextUtils.isEmpty((line = bufferedReader.readLine()))) {
                    buffer.append(line);
                }
                jsonStr = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonStr;
    }
    /**
     * 从assets目录下读取国家和语音 json数据结构是一样的，因此就写成Country一个类
     */
    public static List<Country> getCountryFromJson(String fileName) {
        // 解析国家json文件
        List<Country> countryStateList =stringToList(getJsonStringFromAssets(getContext(),fileName),Country.class);
        if (countryStateList!=null) {
            return  countryStateList;
        }
        return null;
    }
    /**
     * 从assets目录下读取州信息
     */
    public static List<State> getStateFromJson(String fileName) {
        // 解析国家json文件
        List<State> countryStateList =stringToList(getJsonStringFromAssets(getContext(),fileName),State.class);
        if (countryStateList!=null) {
            return  countryStateList;
        }
        return null;
    }

    /**
     * 获取所有国家的数据
     * @return
     */
    public static List<Map<String,String>> getCountryString(){
        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
        List<Map<String,String>> list=new ArrayList<>();
        if (!TextUtils.isEmpty(userCountry) &&  ("America".equals(userCountry)||"India".equals(userCountry)||"UnitedKingdom".equals(userCountry)||"HongKong".equals(userCountry)||"United States".equals(userCountry))) {
            List<Country> countryFromJson = Util.getCountryFromJson("CountryEnglish.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else if(!TextUtils.isEmpty(userCountry) && "Taiwan".equals(userCountry)){
            List<Country> countryFromJson = Util.getCountryFromJson("CountryTraditionalChinese.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else if(!TextUtils.isEmpty(userCountry) && "China".equals(userCountry)){
            List<Country> countryFromJson = Util.getCountryFromJson("CountryChinese.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else{
            List<Country> countryFromJson = Util.getCountryFromJson("CountryEnglish.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }
        return list;
    }
    /**
     * 获取所有语言的数据 语言与国家的数据结构相同，因此同用一个对象国家Country
     * @return
     */
    public static List<Map<String,String>> getLanguageString(){
        String userCountry = PlatformPreference.getPlatformInfo().getCountry();
        List<Map<String,String>> list=new ArrayList<>();
        if (!TextUtils.isEmpty(userCountry) && ("America".equals(userCountry)||"India".equals(userCountry)||"UnitedKingdom".equals(userCountry)||"HongKong".equals(userCountry)||"United States".equals(userCountry)||"UnitedStates".equals(userCountry))) {
            List<Country> countryFromJson = Util.getCountryFromJson("LanguageEnglish.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else if(!TextUtils.isEmpty(userCountry) && "Taiwan".equals(userCountry)){
            List<Country> countryFromJson = Util.getCountryFromJson("LanguageChineseTraditional.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else if(!TextUtils.isEmpty(userCountry) && "China".equals(userCountry)){
            List<Country> countryFromJson = Util.getCountryFromJson("LanguageChinese.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }else {
            List<Country> countryFromJson = Util.getCountryFromJson("LanguageEnglish.json");
            for (Country country : countryFromJson) {
                Map<String,String> map=new HashMap<>();
                map.put(country.getGuid(),country.getName());
                list.add(map);
            }
        }
        return list;
    }
    /**
     * 获取所有州的数据
     * @return
     */
    public static List<Map<String,String>> getStateString(){
        String userCountry = PlatformPreference.getPlatformInfo().getCountry();

        List<Map<String,String>> list=new ArrayList<>();
        if (!TextUtils.isEmpty(userCountry) && ("America".equals(userCountry)||"India".equals(userCountry)||"UnitedKingdom".equals(userCountry)||"HongKong".equals(userCountry)||"United States".equals(userCountry)||"UnitedStates".equals(userCountry))) {
            List<State> countryFromJson = Util.getStateFromJson("StateEnglish.json");
            for (State state : countryFromJson) {
                if(state.getCountry_id().equals(UserPreference.getCountry_id())){
                    Map<String,String> map=new HashMap<>();
                    map.put(state.getGuid(),state.getName());
                    list.add(map);
                }
            }
        }else if(!TextUtils.isEmpty(userCountry) && ("Taiwan".equals(userCountry)||"台湾".equals(userCountry)||"臺灣".equals(userCountry))){
            List<State> countryFromJson = Util.getStateFromJson("StateTraditionalChinese.json");
            for (State state : countryFromJson) {
                if(state.getCountry_id().equals(UserPreference.getCountry_id())){
                    Map<String,String> map=new HashMap<>();
                    map.put(state.getGuid(),state.getName());
                    list.add(map);
                }
            }
        }else if(!TextUtils.isEmpty(userCountry) && ("China".equals(userCountry)||"中国".equals(userCountry)||"新加坡".equals(userCountry)||"Singapore".equals(userCountry))){
            List<State> countryFromJson = Util.getStateFromJson("StateChinese.json");
            for (State state : countryFromJson) {
                if(state.getCountry_id().equals(UserPreference.getCountry_id())){
                    Map<String,String> map=new HashMap<>();
                    map.put(state.getGuid(),state.getName());
                    list.add(map);
                }
            }
        }else{
            List<State> countryFromJson = Util.getStateFromJson("StateEnglish.json");
            for (State state : countryFromJson) {
                if(state.getCountry_id().equals(UserPreference.getCountry_id())){
                    Map<String,String> map=new HashMap<>();
                    map.put(state.getGuid(),state.getName());
                    list.add(map);
                }
            }
        }
        return list;
    }

    public static Context getContext() {
        return BaseApplication.getGlobalContext();
    }
    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 通过gson把json字符串转成list集合
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T>  List<T> stringToList(String json ,Class<T> cls  ){
        Gson gson = new Gson();
        List<T> list = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for(final JsonElement elem : array){
            list.add(gson.fromJson(elem, cls));
        }
        return list ;
    }
    public static String getLacalCountry(){
        String country=null;
        String CT = Locale.getDefault().getCountry();
        switch (CT){
            case "TW":
                country = "Taiwan";
                break;
            case "HK":
                country = "Taiwan";
                break;
            case "CN":
                country = "China";
                break;
            case "US":
                country = "United States";
                break;
            case "IN":
                country = "India";
                break;
            case "UK":
                country = "UnitedKingdom";
                break;
            case "SG":
                country = "Singapore";
                break;
        }
        if(country==null){
            country = "United States";
        }
        return country;
    }
    public static String getLacalLanguage(){
        String language="English";
//        String LG = Locale.getDefault().getLanguage();
//        switch (LG){
//            case "zh":
//                if(getLacalCountry().equals("China")){
//                    language="Simplified";
//                }else if(getLacalCountry().equals("Taiwan")){
//                    language="Traditional";
//                }
//                break;
//            case "en":
//                language = "English";
//                break;
//
//        }
        String country = UserPreference.getCountry();
        if(country!=null){
            country=setEnglishCountry(country);

            if(country.equals("Singapore")||country.equals("China")){//新加坡 汉语
                language="Simplified";
            }else if(country.equals("Taiwan")){
                language="Traditional";
            }else{
                language = "English";
            }
        }else {
            String LG = Locale.getDefault().getLanguage();
            switch (LG) {
                case "zh":
                    if (getLacalCountry().equals("China")||getLacalCountry().equals("Singapore")) {
                        language = "Simplified";
                    } else if (getLacalCountry().equals("Taiwan")) {
                        language = "Traditional";
                    }
                    break;
                case "en":
                    language = "English";
                    break;

            }
        }
        return language;
    }
    /**
     * 获得国家列表
     *
     * @return
     */
    public static List<String> getCountryList() {
        List<String> list = new ArrayList<String>();
        list.add(BaseApplication.getGlobalContext().getString(R.string.united_utates));
        list.add(BaseApplication.getGlobalContext().getString(R.string.australia));
        list.add(BaseApplication.getGlobalContext().getString(R.string.india));
        list.add(BaseApplication.getGlobalContext().getString(R.string.indonesia));
        list.add(BaseApplication.getGlobalContext().getString(R.string.united_kingdom));
        list.add(BaseApplication.getGlobalContext().getString(R.string.canada));
        list.add(BaseApplication.getGlobalContext().getString(R.string.new_zealand));
        list.add(BaseApplication.getGlobalContext().getString(R.string.ireland));
        list.add(BaseApplication.getGlobalContext().getString(R.string.south_africa));
        list.add(BaseApplication.getGlobalContext().getString(R.string.singapore));
        list.add(BaseApplication.getGlobalContext().getString(R.string.pakistan));
        list.add(BaseApplication.getGlobalContext().getString(R.string.philippines));
        list.add(BaseApplication.getGlobalContext().getString(R.string.hong_kong));
        list.add(BaseApplication.getGlobalContext().getString(R.string.tai_wan));
        list.add(BaseApplication.getGlobalContext().getString(R.string.china));
        return list;
    }
    public static String getMD5(String val,String salt){
        MessageDigest md5 = null;
        byte[] m=null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update((val+salt).getBytes());
            m = md5.digest();//加密
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return getString(m);
    }
    private static String getString(byte[] b){
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < b.length; i ++){
            sb.append(Long.toString((int) b[i] & 0xff, 16));
        }
        return sb.toString();
    }

    public static String getLacalLanguage(String country) {
        String language="English";
        if(country!=null){
            if(country.equals("Singapore")){//新加坡 汉语
                language="Simplified";
            }else if(country.equals("Taiwan")){
                language="Traditional";
            }else{
                language = "English";
            }
        }
        return language;
    }
    /**
     * 隐藏键盘
     */
//    public static void hideKeyboard(Context context, View view) {
//        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }


    /**
     * 列表加载更多
     */
    public static View getLoadingView(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        relativeLayout.setGravity(Gravity.CENTER);
        int padding = dp2px(context, 15);
        relativeLayout.setPadding(0, padding, 0, padding);
        AnimationDrawable animationDrawable = getFrameAnim(getLoadingDrawableList(context), true, 75);
        animationDrawable.start();
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageView.setImageDrawable(animationDrawable);
        relativeLayout.addView(imageView);
        return relativeLayout;
    }
    /**
     * 获得AnimationDrawable，图片动画
     *
     * @param list         动画需要播放的图片集合
     * @param isRepeatable 是否可以重复
     * @param duration     帧间隔（毫秒）
     * @return
     */
    public static AnimationDrawable getFrameAnim(List<Drawable> list, boolean isRepeatable, int duration) {
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(!isRepeatable);
        if (!isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                animationDrawable.addFrame(list.get(i), duration);
            }
        }
        return animationDrawable;
    }

    public static List<Drawable> getLoadingDrawableList(Context context) {
        List<Drawable> drawableList = new ArrayList<Drawable>();
        drawableList.add(getDrawable(context, R.drawable.loading_01));
//        drawableList.add(getDrawable(context, R.drawable.loading_02));
//        drawableList.add(getDrawable(context, R.drawable.loading_03));
//        drawableList.add(getDrawable(context, R.drawable.loading_04));
//        drawableList.add(getDrawable(context, R.drawable.loading_05));
//        drawableList.add(getDrawable(context, R.drawable.loading_06));
//        drawableList.add(getDrawable(context, R.drawable.loading_07));
//        drawableList.add(getDrawable(context, R.drawable.loading_08));
//        drawableList.add(getDrawable(context, R.drawable.loading_09));
//        drawableList.add(getDrawable(context, R.drawable.loading_10));
//        drawableList.add(getDrawable(context, R.drawable.loading_11));
//        drawableList.add(getDrawable(context, R.drawable.loading_12));
        return drawableList;
    }
    private static Drawable getDrawable(Context context, int resId) {
        return context.getResources().getDrawable(resId);
    }



}
