package com.wiscom.vchat.common;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.model.HuanXinUser;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.db.DbModle;
import com.wiscom.vchat.event.AllUnReadMsgEvent;
import com.wiscom.vchat.event.MessageArrive;
import com.wiscom.vchat.event.UnreadMsgChangedEvent;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.ui.chat.ChatActivity;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.NotificationHelper;
import com.wiscom.library.util.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * 环信帮助类（环信注册由后台完成，在调用注册接口时后台直接注册环信，返回用户account和密码）
 * Created by zhangdroid on 2017/6/29.
 */
public class HyphenateHelper {
    private static final String TAG = HyphenateHelper.class.getSimpleName();
    private static HyphenateHelper sInstance;
    // 环信消息监听器
    private EMMessageListener mMessageListener;
    private boolean isChatActivity=true;
    private HyphenateHelper() {
    }

    /**
     * 环信登录回调
     */
    public interface OnLoginCallback {

        /**
         * 环信登录成功
         */
        void onSuccess();

        /**
         * 环信登录失败
         */
        void onFailed();
    }

    public static HyphenateHelper getInstance() {
        if (null == sInstance) {
            synchronized (HyphenateHelper.class) {
                if (null == sInstance) {
                    sInstance = new HyphenateHelper();
                }
            }
        }
        return sInstance;
    }

    /**
     * 消息发送监听器
     */
    public interface OnMessageSendListener {
        /**
         * 发送成功
         *
         * @param emMessage 发送的消息对象
         */
        void onSuccess(EMMessage emMessage);

        /**
         * 发送失败
         */
        void onError();
    }

    /**
     * 初始化环信
     */
    public void init(Context context) {
        Log.i(TAG, "init");
        EMOptions emOptions = new EMOptions();
        emOptions.setSortMessageByServerTime(true);
        emOptions.setRequireAck(false);
        // GCM
        emOptions.setGCMNumber("");

//
//        int pid = android.os.Process.myPid();
//        String processAppName = getAppName(pid,context);
//// 如果APP启用了远程的service，此application:onCreate会被调用2次
//// 为了防止环信SDK被初始化2次，加此判断会保证SDK被初始化1次
//// 默认的APP会在以包名为默认的process name下运行，如果查到的process name不是APP的process name就立即返回
//
//        if (processAppName == null ||!processAppName.equalsIgnoreCase(context.getApplicationContext().getPackageName())) {
//            Log.e(TAG, "enter the service process!");
//
//            // 则此application::onCreate 是被service 调用的，直接返回
//            return;
//        }




//        emOptions.setMipushConfig();
        EMClient.getInstance().init(context.getApplicationContext(), emOptions);
        // 开启debug模式，上线的时候需要关闭
        EMClient.getInstance().setDebugMode(true);
        // 设置连接监听
        setConnectionListener();
        // 设置消息监听
        setGlobalMessageListener();
    }
    private String getAppName(int pID,Context context) {
        String processName = null;
        ActivityManager am = (ActivityManager) context.getApplicationContext().getSystemService(ACTIVITY_SERVICE);
        List l = am.getRunningAppProcesses();
        Iterator i = l.iterator();
        PackageManager pm = context.getApplicationContext().getPackageManager();
        while (i.hasNext()) {
            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i.next());
            try {
                if (info.pid == pID) {
                    processName = info.processName;
                    return processName;
                }
            } catch (Exception e) {
                // Log.d("Process", "Error>> :"+ e.toString());
            }
        }
        return processName;
    }


    /**
     * 登录
     *
     * @param account  用户account
     * @param password 密码
     */
    public void login(String account, String password, final OnLoginCallback onLoginCallback) {
        if (isLoggedIn()) {
            return;
        }
        Log.i(TAG, "login#account = " + account + " password = " + password);
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            if (null != onLoginCallback) {
                onLoginCallback.onFailed();
            }
            return;
        }
        EMClient.getInstance().login(account, password, new EMCallBack() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "login#onSuccess()");
                // 登录成功后加载聊天会话
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        EMClient.getInstance().chatManager().loadAllConversations();
                    }
                }).start();
                if (null != onLoginCallback) {
                    onLoginCallback.onSuccess();
                }
            }

            @Override
            public void onError(int code, String error) {
                Log.i(TAG, "login#onError() code = " + code + " error = " + error);
                if (null != onLoginCallback) {
                    onLoginCallback.onFailed();
                }
            }

            @Override
            public void onProgress(int progress, String status) {
            }
        });
    }

    /**
     * 登出
     */
    public void logout() {
        Log.i(TAG, "logout");
        // 绑定了GCM，参数必须为true，登出后会解绑GCM
        EMClient.getInstance().logout(true);
        // 移除消息接收监听
        removeMessageListener(mMessageListener);
    }

    /**
     * @return 是否登录过
     */
    public boolean isLoggedIn() {
        return EMClient.getInstance().isLoggedInBefore();
    }

    /**
     * 设置连接状态监听
     */
    private void setConnectionListener() {
        Log.i(TAG, "setConnectionListener");
        EMClient.getInstance().addConnectionListener(new EMConnectionListener() {
            @Override
            public void onConnected() {
                Log.i(TAG, "onConnected");
            }

            @Override
            public void onDisconnected(int errorCode) {
                Log.i(TAG, "onDisconnected#errorCode = " + errorCode);
                if (errorCode == EMError.USER_REMOVED) {
                    // 帐号已经被移除
                } else if (errorCode == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                    // 帐号在其他设备登录
                    if (EMClient.getInstance().isLoggedInBefore()) {
                        logout();
                    }
                }
            }
        });
    }

    /**
     * 设置全局消息监听
     */
    private void setGlobalMessageListener() {
        mMessageListener = new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                // 接收到新消息
                for (EMMessage emMessage : messages) {
                    if(isChatActivity){
                        showNotification(emMessage);
                    }
                    String userName = emMessage.getStringAttribute("userName", "");
                    String userPic = emMessage.getStringAttribute("userPic", "");
                    String userAccount = emMessage.getStringAttribute("userAccount", "");
                    String userGuid = emMessage.getStringAttribute("userGuid", "");
//                    String lastMsg = emMessage.getStringAttribute("lastMsg", "");
//                    String lastMsgTime = emMessage.getStringAttribute("lastMsgTime", "");
                    List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
                    String unReadNum="1";
                    for (int i = 0; i < allAcount.size(); i++) {
                        if(allAcount.get(i).getHxId().equals(userGuid)){
                            if(!allAcount.get(i).getIsRead().equals("0")){
                               unReadNum= Integer.parseInt(allAcount.get(i).getIsRead())+1+"";
                            }
                        }
                    }


//                    此处环信的消息是正确的，将消息内容添加到数据库中，以后在ChatActivity展示数据时，从数据库中取（暂时未做）
                    HuanXinUser user =new HuanXinUser(userGuid,userName,userPic,userAccount,unReadNum);
                    DbModle.getInstance().getUserAccountDao().addAccount(user);//把发信人的信息保存在数据库中
                }
                EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                //消息过来时刷新聊天列表
                EventBus.getDefault().post(new MessageArrive());
            }

            @Override
            public void onCmdMessageReceived(List<EMMessage> messages) {
            }

            @Override
            public void onMessageRead(List<EMMessage> messages) {
            }

            @Override
            public void onMessageDelivered(List<EMMessage> messages) {
            }

            public void onMessageRecalled(List<EMMessage> list) {

            }

            @Override
            public void onMessageChanged(EMMessage message, Object change) {
            }
        };
        setMessageListener(mMessageListener);
    }

    private void showNotification(EMMessage emMessage) {

        if (null != emMessage) {
            Context context = BaseApplication.getGlobalContext();
            String userName = emMessage.getStringAttribute("userName", "");
            String userPic = emMessage.getStringAttribute("userPic", "");
            String userAccount = emMessage.getStringAttribute("userAccount", "");
            String userGuid = emMessage.getStringAttribute("userGuid", "");
//            获取原来的消息数量
            int readMsgNum = SharedPreferenceUtil.getIntValue(context, "Read_Msg_Num", userGuid, 0);
            int allReadMsgNum = SharedPreferenceUtil.getIntValue(context, "Read_Msg_Num", "All_Read_Msg_Num", 0);
            EventBus.getDefault().post(new AllUnReadMsgEvent(allReadMsgNum+1));

//            在原来的基础上增加一条未读消息
            SharedPreferenceUtil.setIntValue(context,"Read_Msg_Num",userGuid,readMsgNum+1);
//            总的未读消息
            SharedPreferenceUtil.setIntValue(context,"Read_Msg_Num","All_Read_Msg_Num",allReadMsgNum+1);
//                刷新好友列表
            EventBus.getDefault().post(new UpdateFriendListEvent());
            // 根据消息类型显示不同的内容
            String message = "";
            if (emMessage.getType() == EMMessage.Type.TXT) {// 文字
                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
                if (null != emTextMessageBody) {
                    message = emTextMessageBody.getMessage();
                }
            } else if (emMessage.getType() == EMMessage.Type.VOICE) {// 语音
                message = context.getString(R.string.chat_type_voice);
            } else if (emMessage.getType() == EMMessage.Type.IMAGE) {// 图片
                message = context.getString(R.string.chat_type_image);
            }
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra(LaunchHelper.INTENT_KEY_COMMON, new ChatParcelable(285, emMessage.getFrom(), userName, userPic));
            NotificationHelper.getInstance(context)
                    .setTicker(context.getString(R.string.chat_new_msg_received, userName))
                    .setIcon(R.mipmap.logo2)
                    .setTitle(userName)
                    .setMessage(message)
                    .setTime(emMessage.getMsgTime())
                    .setDefaults()
                    .setPendingIntentActivity(Integer.parseInt(emMessage.getFrom()), intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    .notify(Integer.parseInt(emMessage.getFrom()));
        }
    }

    /**
     * 设置消息监听
     */
    public void setMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().addMessageListener(listener);
        }
    }

    /**
     * 移除接收消息监听器
     */
    public void removeMessageListener(EMMessageListener listener) {
        if (null != listener) {
            EMClient.getInstance().chatManager().removeMessageListener(listener);
        }
    }

    /**
     * 发送消息（仅单聊）
     *
     * @param emMessage
     * @param listener
     */
    private void sendMessage(final EMMessage emMessage, final OnMessageSendListener listener) {
        if (null != emMessage) {
            Log.i(TAG, "sendMessage#emMessage = " + emMessage.toString());
            emMessage.setChatType(EMMessage.ChatType.Chat);
            emMessage.setMessageStatusCallback(new EMCallBack() {
                @Override
                public void onSuccess() {
                    if (null != listener) {
                        listener.onSuccess(emMessage);
                    }
                }

                @Override
                public void onError(int code, String error) {
                    if (null != listener) {
                        listener.onError();
                    }
                }

                @Override
                public void onProgress(int progress, String status) {
                }
            });
//            String message = "";
//            String msgTime="";
//            if (emMessage.getType() == EMMessage.Type.TXT) {// 文字
//                EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
//                 msgTime = emMessage.getMsgTime()+"";
//                if (null != emTextMessageBody) {
//                    message = emTextMessageBody.getMessage();
//                }
//            }else {
////                语音图片，暂时省略
//            }
            //设置透传的消息
            emMessage.setAttribute("userName", UserPreference.getNickname());
            emMessage.setAttribute("userPic",UserPreference.getMiddleImage());
            emMessage.setAttribute("userAccount",UserPreference.getAccount());
            emMessage.setAttribute("userGuid",UserPreference.getId());
//            if(!TextUtils.isEmpty(message) && message!=null){
//                emMessage.setAttribute("lastMsg",message);
//                emMessage.setAttribute("lastMsgTime",msgTime);
//
//            }
            EMClient.getInstance().chatManager().sendMessage(emMessage);
        }
    }

    /**
     * 发送文字消息（仅单聊）
     *
     * @param account 接收方account
     * @param text    消息内容
     */
    public void sendTextMessage(String account, String text, OnMessageSendListener listener) {
        Log.i(TAG, "sendTextMessage#account = " + account + " text = " + text);
        sendMessage(EMMessage.createTxtSendMessage(text, account), listener);
    }

    /**
     * 发送语音消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 语音文件路径
     * @param duration 语音时间长度(单位秒)
     */
    public void sendVoiceMessage(String account, String filePath, int duration, OnMessageSendListener listener) {
        Log.i(TAG, "sendVoiceMessage#account = " + account + " filePath = " + filePath + " duration = " + duration);
        sendMessage(EMMessage.createVoiceSendMessage(filePath, duration, account), listener);
    }

    /**
     * 发送图片消息（仅单聊）
     *
     * @param account  接收方account
     * @param filePath 图片文件路径
     */
    public void sendImageMessage(String account, String filePath, OnMessageSendListener listener) {
        Log.i(TAG, "sendImageMessage#account = " + account + " filePath = " + filePath);
        // 第二个参数表示是否发送原图，图片超过100k会默认压缩后发送
        sendMessage(EMMessage.createImageSendMessage(filePath, false, account), listener);
    }

    private EMConversation getConversation(String account) {
        // 查询单聊会话
        return EMClient.getInstance().chatManager().getConversation(account, EMConversation.EMConversationType.Chat);
    }

    /**
     * 初始化某个用户的聊天记录
     *
     * @param account  用户account
     * @param pageSize 每页显示的聊天记录条数
     * @return
     */
    public List<EMMessage> initConversation(String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 标记所有未读消息为已读
            emConversation.markAllMessagesAsRead();
            List<EMMessage> messageList = emConversation.getAllMessages();
            int count = messageList.size();
            int allMsgCount = emConversation.getAllMsgCount();

            if(allMsgCount==2){
                allMsgCount= allMsgCount-1;
            }
            if (count < allMsgCount && count < 20) {// 本地聊天记录数超过当前内存列表长度并且不够显示一页，加载pageSize条记录
                // 获取最上面一条消息的id，用于查询pageSize条记录
//                传入null，获取最新的pageSize条信息
                List<EMMessage> emMessages = emConversation.loadMoreMsgFromDB(null, pageSize);
                if(emMessages.size()<count){
                    emMessages=messageList;
                }
                return emMessages;
//
            } else { // 获取此会话的所有消息（默认20条）
                return messageList;
            }
        }
        return null;
//        // 查询单聊会话
//        EMConversation emConversation = getConversation(account);
//        if (null != emConversation) {
//            // 标记所有未读消息为已读
//            emConversation.markAllMessagesAsRead();
//            List<EMMessage> messageList = emConversation.getAllMessages();
//            int count = messageList.size();
//            if (count < emConversation.getAllMsgCount() && count < 20) {// 本地聊天记录数超过当前内存列表长度并且不够显示一页，加载pageSize条记录
//                // 获取最上面一条消息的id，用于查询pageSize条记录
//                return emConversation.loadMoreMsgFromDB(messageList.get(count - 1).getMsgId(), pageSize);
//            } else { // 获取此会话的所有消息（默认20条）
//                return messageList;
//            }
//        }
//        return null;
    }

    /**
     * 查询更多聊天记录，以最后一条消息查询
     *
     * @param account
     * @param pageSize
     * @return
     */
    public List<EMMessage> loadMoreMessages(String account, int pageSize) {
        // 查询单聊会话
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            // 查询最后一条消息id前pageSize条消息记录
            return emConversation.loadMoreMsgFromDB(emConversation.getLastMessage().getMsgId(), pageSize);
        }
        return null;
    }

    /**
     * @return 与某个用户全部聊天记录条数
     */
    public int getAllMsgCount(String account) {
        EMConversation emConversation = getConversation(account);
        if (null != emConversation) {
            return emConversation.getAllMsgCount();
        }
        return 0;
    }

    /**
     * 下载消息附件
     *
     * @param emMessage
     */
    public void downloadAttachment(EMMessage emMessage) {
        if (null != emMessage) {
            EMClient.getInstance().chatManager().downloadAttachment(emMessage);
        }
    }

    /**
     * @return 全部未读消息数
     */
    public int getUnreadMsgCount() {
        return EMClient.getInstance().chatManager().getUnreadMessageCount();
    }

    /**
     * @return 所有会话（聊天记录）
     */
    public Map<String, EMConversation> getAllConversations() {
        return EMClient.getInstance().chatManager().getAllConversations();
    }
    /**
     * 判断是否在ChatActivity这个界面
     */
    public void isChatActivity() {
        isChatActivity=false;
    }
    /**
     * 离开ChatActivity这个界面时把标记改为true
     */
    public void isNotChatActivity() {
        isChatActivity=true;
    }

}
