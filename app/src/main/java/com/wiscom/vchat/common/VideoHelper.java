package com.wiscom.vchat.common;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.VideoCall;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.chat.ChatActivity;
import com.wiscom.vchat.ui.pay.PayActivity;
import com.wiscom.vchat.ui.video.VideoInviteActivity;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.ToastUtil;

/**
 * 视频邀请与被邀请帮助类
 * Created by zhangdroid on 2017/6/15.
 */
public class VideoHelper {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private VideoHelper() {
    }

    /**
     * 发起视频邀请
     *
//     * @param status                用户状态{@link com.wiscom.vchat.C.homepage}，废弃（发起呼叫前通过后台查询状态）
     * @param videoInviteParcelable 视频邀请需要传递的参数
     * @param fragmentManager       用来显示对话框
     */
    public static void startVideoInvite(final VideoInviteParcelable videoInviteParcelable, final FragmentManager fragmentManager) {
//        if (UserPreference.isShowCharge()) {
//            showChargeTip(fragmentManager, new OnChargeTipClickListenr() {
//                @Override
//                public void onClick() {
//                    checkUserStatus(videoInviteParcelable, fragmentManager);
//                }
//            });
//        } else {
//            checkUserStatus(videoInviteParcelable, fragmentManager);
//        }
        LaunchHelper.getInstance().launch(mContext, VideoInviteActivity.class, videoInviteParcelable);

    }

    private static void checkUserStatus(final VideoInviteParcelable videoInviteParcelable, final FragmentManager fragmentManager) {
        ApiManager.videoCall(String.valueOf(videoInviteParcelable.uId), new IGetDataListener<VideoCall>() {
            @Override
            public void onResult(VideoCall videoCall, boolean isEmpty) {
                switch (videoCall.getRemoteStatus()) {
                    case C.homepage.STATE_FREE:// 空闲
                        if (UserPreference.isAnchor()) {// 播主直接发起视频
                            videoInviteParcelable.setHostPrice(videoCall.getHostPrice());
                            LaunchHelper.getInstance().launch(mContext, VideoInviteActivity.class, videoInviteParcelable);
                        } else {
//                            if (!"-1".equals(videoCall.getBeanStatus())) { // 余额充足
                                videoInviteParcelable.setHostPrice(videoCall.getHostPrice());
                                LaunchHelper.getInstance().launch(mContext, VideoInviteActivity.class, videoInviteParcelable);
//                            } else { // 余额不足
//                                showBalanceNotEnoughTip(fragmentManager);
//                            }
                        }
                        break;

                    case C.homepage.STATE_BUSY:// 忙线中
                        if (UserPreference.isAnchor()) {
                            ToastUtil.showLongToast(mContext, mContext.getString(R.string.homepage_busy_all));
                        } else {
                            ApiManager.videoCall(String.valueOf(videoInviteParcelable.uId), new IGetDataListener<VideoCall>() {
                                @Override
                                public void onResult(VideoCall videoCall, boolean isEmpty) {
                                    if (!"-1".equals(videoCall.getBeanStatus())) { // 余额充足
                                        showBusyTip(fragmentManager, videoInviteParcelable);
                                    } else { // 余额不足
                                        showBalanceNotEnoughTip(fragmentManager);
                                    }
                                }

                                @Override
                                public void onError(String msg, boolean isNetworkError) {
                                    showBusyTip(fragmentManager, videoInviteParcelable);
                                }
                            });
                        }
                        break;

                    case C.homepage.STATE_NO_DISTRUB:// 勿扰
                        if (UserPreference.isAnchor()) {
                            ToastUtil.showLongToast(mContext, mContext.getString(R.string.homepage_busy_all));
                        } else {
                            ToastUtil.showLongToast(mContext, mContext.getString(R.string.homepage_no_distrub));
                        }
                        break;
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.video_invite_failed));
            }
        });
    }

    /**
     * 开始视频前显示计费提示框
     *
     * @param fragmentManager
     * @param onChargeTipClickListenr
     */
    private static void showChargeTip(FragmentManager fragmentManager, final OnChargeTipClickListenr onChargeTipClickListenr) {
        AlertDialog.show(fragmentManager, mContext.getString(R.string.alert), mContext.getString(R.string.homepage_charge_tip),
                mContext.getString(R.string.homepage_charge_nomore), mContext.getString(R.string.positive), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                        if (null != onChargeTipClickListenr) {
                            onChargeTipClickListenr.onClick();
                        }
                    }

                    @Override
                    public void onPositiveClick(View view) {
                        UserPreference.hideCharge();
                        if (null != onChargeTipClickListenr) {
                            onChargeTipClickListenr.onClick();
                        }
                    }
                });
    }

    /**
     * 显示余额不足提示框
     *
     * @param fragmentManager
     */
    public static void showBalanceNotEnoughTip(FragmentManager fragmentManager) {
        AlertDialog.show(fragmentManager, mContext.getString(R.string.alert), mContext.getString(R.string.homepage_balance_deficiency),
                mContext.getString(R.string.homepage_balance_recharge), mContext.getString(R.string.homepage_balance_wait), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                    }

                    @Override
                    public void onPositiveClick(View view) {
                        // 充值
                        LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.FROM_TAG_HOMEPAGE));
                    }
                });
    }

    /**
     * 显示用户忙线提示框
     *
     * @param fragmentManager
     * @param videoInviteParcelable
     */
    private static void showBusyTip(FragmentManager fragmentManager, final VideoInviteParcelable videoInviteParcelable) {
        AlertDialog.show(fragmentManager, mContext.getString(R.string.alert), mContext.getString(R.string.homepage_busy_tip),
                mContext.getString(R.string.homepage_leave_msg), mContext.getString(R.string.negative), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                    }

                    @Override
                    public void onPositiveClick(View view) {
                        // 留言，跳转到聊天页面
                        LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(videoInviteParcelable.uId, videoInviteParcelable.account,
                                videoInviteParcelable.nickname, videoInviteParcelable.imgageUrl));
                    }
                });
    }

    public interface OnChargeTipClickListenr {
        void onClick();
    }

}
