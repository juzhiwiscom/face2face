package com.wiscom.vchat.mvp;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BasePresenter {
    void start();

    void finish();
}
