package com.wiscom.vchat.ui.video.presenter;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.IsFollow;
import com.wiscom.vchat.data.model.VideoHeartBeat;
import com.wiscom.vchat.data.model.VideoMsg;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.AgoraEvent;
import com.wiscom.vchat.event.AgoraMediaEvent;
import com.wiscom.vchat.event.FinishVideoEvent;
import com.wiscom.vchat.ui.video.contract.VideoContract;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/6/13.
 */
public class VideoPresenter implements VideoContract.IPresenter {
    private VideoContract.IView mVideoView;
    private Context mContext;
    private String mChannelId;// 视频频道id
    private String mNickname;// 对方用户昵称
    private String mGuid;// 对方用户id
    private boolean isChatView=false;
    private int connectType;
    private String fromView;
    private boolean creatSuccess=false;
    private String resonCode="1";
    private boolean requestHeartBeat=true;

    public VideoPresenter(VideoContract.IView view, String channelId, String nickname, String id,String fromView) {
        this.mVideoView = view;
        this.mContext = view.obtainContext();
        this.mChannelId = channelId;
        this.mNickname = nickname;
        this.mGuid = id;
        this.fromView=fromView;
    }
    public VideoPresenter(VideoContract.IView view){
        this.mVideoView = view;
        this.mContext = view.obtainContext();


    }

    @Override
    public void start() {
        // 设置本地视频
        AgoraHelper.getInstance().setupLocalVideo(true,mContext, mVideoView.getLocalVideoView());
    }

    @Override
    public void finish() {
        mVideoView = null;
        mContext = null;
    }

    @Override
    public void handleVideoEvent(final AgoraMediaEvent event) {
        if (null != event) {
            LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","媒体频道eventCode="+event.eventCode);
            switch (event.eventCode) {
                case AgoraHelper.EVENT_CODE_SETUP_REMOTE_VIDEO:// 创建远端视频
                    AgoraHelper.getInstance().setupRemoteVideo(mContext, mVideoView.getRemoteVideoView(), event.uId);
//                   关闭handler,停止网络接口的请求
                    creatSuccess=true;
//                    安卓8.0重新添加本地视频
                    String release = Build.VERSION.RELEASE;
                    String substring = release.substring(0,1);
                    boolean equals = substring.equals("8");
                    if(equals){
                        mVideoView.getLocalVideoView().removeAllViews();
                        AgoraHelper.getInstance().setupLocalVideo(true,mContext, mVideoView.getLocalVideoView());
                    }
                    // 开始轮询
                    if (null != mVideoView) {
//                        // 开始视频计时
//                        mVideoView.videoTimeing();
                                if(creatSuccess){//判断对方是否在随机视频界面
//                                   匹配建立成功连接,如果来自主动呼叫收到的邀请，则不调用调用此接口
                                    LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","开始连接fromView="+fromView);
                                    if(fromView!=null && !fromView.equals(C.Video.ACCEPT_INVITE)){
                                        randomMatchSuccess();
                                    }
                                    doVideoHeartBeat();
                                }
                    }
//                    将声网中的接受邀请的方法onInviteReceived关闭
                    SharedPreferenceUtil.setBooleanValue(mContext,"VideoSuccess","closeReceiveInviter",true);
                    break;

                case AgoraHelper.EVENT_CODE_MEDIA_JOIN_SUCCESS:// 成功加入视频频道
                    break;

                case AgoraHelper.EVENT_CODE_USER_OFFLINE:// 用户离线或掉线
                    goCallEvent(mContext.getString(R.string.video_leave));
                    AgoraHelper.getInstance().leaveChannel(mChannelId);
                    break;

                case AgoraHelper.EVENT_CODE_VIDEO_ERROR:// 视频发生错误
                    goCallEvent(mContext.getString(R.string.video_error));

                    AgoraHelper.getInstance().leaveChannel(mChannelId);
                    break;
            }
        }
    }

    public void randomMatchSuccess() {
//        判断是否来自聊天界面或者历史记录的视频
        if(fromView.equals(C.Video.GO_CALL)){
            connectType=3;
        }else if(BeanPreference.getScreenGender()==0 || BeanPreference.getScreenGender()==1){
            connectType=2;
        }else {
            connectType=1;
        }
        ApiManager.vedioCreatSuccess(mGuid,connectType, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接成功，开始记时");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接失败");
            }
        });
    }

    @Override
    public void getIsFollow(String uId) {
        ApiManager.isFollow(uId, new IGetDataListener<IsFollow>() {
            @Override
            public void onResult(IsFollow isFollow, boolean isEmpty) {
                mVideoView.setFollowVisibility("0".equals(isFollow.getIsFollow()));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoView.setFollowVisibility(true);
            }
        });
    }

    @Override
    public void follow(String uId) {
    }

    @Override
    public void report(final String uId) {
          resonCode ="";
        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_report, null);
        bottomDialog.setContentView(contentView);
        final TextView tvViolent = (TextView) contentView.findViewById(R.id.report_violent);
        final TextView tvAdvert = (TextView) contentView.findViewById(R.id.report_advert);
        final TextView tvgender = (TextView) contentView.findViewById(R.id.report_fake_gender);
        final TextView tvBawdry = (TextView) contentView.findViewById(R.id.report_bawdry);
        final TextView tvClown = (TextView) contentView.findViewById(R.id.report_clown);
        final TextView tvPoli = (TextView) contentView.findViewById(R.id.report_politice);
        final TextView tvOther = (TextView) contentView.findViewById(R.id.report_other);
        final TextView tvCancel = (TextView) contentView.findViewById(R.id.report_cancel);
        final TextView tvReport = (TextView) contentView.findViewById(R.id.report_report);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = Util.dp2px(mContext,220);
        params.height =Util.dp2px(mContext,260);
        params.topMargin=Util.dp2px(mContext,150);
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
        bottomDialog.show();
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
            }
        });
        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startReport(uId, resonCode);
                bottomDialog.dismiss();
            }
        });
        tvViolent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="2";
            }
        });
        tvAdvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="2";
            }
        });
        tvgender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvgender.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvBawdry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvClown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvClown.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvPoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="4";
            }
        });
        tvOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOther.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="3";
            }
        });

    }

    @Override
    public void switchCamera() {
        AgoraHelper.getInstance().switchCamera();
    }

    @Override
    public void closeVideo() {
        if(mChannelId!=null){
            AgoraHelper.getInstance().leaveChannel(mChannelId);
        }
    }

    @Override
    public void sendMsg(String account) {
        String message = mVideoView.getInputMessage();
        if (!TextUtils.isEmpty(message)) {
            AgoraHelper.getInstance().sendTextMessage(account, message);
        } else {
            mVideoView.showTip(mContext.getString(R.string.video_send_tip));
        }
    }

    @Override
    public void handleAgoraEvent(AgoraEvent event) {
        LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","信令频道eventCode="+event.eventCode);
        switch (event.eventCode) {
            case AgoraHelper.EVENT_CODE_JOIN_CHANNEL_FAILED:// 加入频道失败
                goCallEvent(mContext.getString(R.string.video_join_failed));
                AgoraHelper.getInstance().leaveChannel(mChannelId);
                break;

            case AgoraHelper.EVENT_CODE_LEAVE_CHANNEL_SELF:// 离开频道
                break;

            case AgoraHelper.EVENT_CODE_SEND_MSG_SUCCESS:// 发送消息成功
                // 更新消息列表并清空已发送消息
                VideoMsg videoMsgSend = new VideoMsg();
                videoMsgSend.setNickname(UserPreference.getNickname());
                videoMsgSend.setMessage(mVideoView.getInputMessage());
                mVideoView.updateMsgList(videoMsgSend);
                mVideoView.clearInput();
                break;

            case AgoraHelper.EVENT_CODE_SEND_MSG_FAILED:// 发送消息失败
                goCallEvent(mContext.getString(R.string.video_send_failed));

                break;

            case AgoraHelper.EVENT_CODE_RECEIVE_MSG:// 收到对方消息
                VideoMsg videoMsgReceived = new VideoMsg();
                videoMsgReceived.setNickname(mNickname);
                videoMsgReceived.setMessage(event.message);
                mVideoView.updateMsgList(videoMsgReceived);
                break;
            case AgoraHelper.EVENT_CODE_INVITE_REFUSE:// 对方不在随机视频界面
                AgoraHelper.getInstance().leaveChannel(mChannelId);
                creatSuccess=false;
                break;
        }
    }

    private void goCallEvent(String msg) {
        if(fromView!=null &&(fromView.equals(C.Video.GO_CALL)||fromView.equals(C.Video.ACCEPT_INVITE))){
            EventBus.getDefault().post(new FinishVideoEvent());
            mVideoView.showTip(msg);
        }
    }

    @Override
    public String convertSecondsToString(int seconds) {
        return Util.convertSecondsToString(seconds);
    }

    /**
     * 视频通话心跳，向后台服务器上报心跳（轮询周期10秒）
     */
    private void doVideoHeartBeat() {
        ApiManager.videoHeartBeat(mGuid, new IGetDataListener<VideoHeartBeat>() {
            @Override
            public void onResult(VideoHeartBeat videoHeartBeat, boolean isEmpty) {
                    if(videoHeartBeat.getIsSucceed().equals("1")){
                        String beanStatus = videoHeartBeat.getBeanStatus();
                        if(!TextUtils.isEmpty(beanStatus)&&beanStatus.equals("-1")){//钻石不足
//                            弹出拦截对话框
                            if(requestHeartBeat&&mVideoView!=null)
                              mVideoView.diamonInterrupt(true);
                            requestHeartBeat=false;
                        }

                    }

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
        if (null != mVideoView && requestHeartBeat) {
            mVideoView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doVideoHeartBeat();
                }
            }, 10000);
        }
    }
//    举报
    public void startReport(String userId,String resonCode) {
        ApiManager.report(userId, resonCode,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {

            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    public void systemVideoSuccess(long guid) {
        //        判断是否收费
        if(BeanPreference.getScreenGender()==0 || BeanPreference.getScreenGender()==1){
            connectType=2;
        }else {
            connectType=1;
        }
        ApiManager.systemVideoCreatSuccess(String.valueOf(guid),connectType,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接成功，开始记时");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接失败");
            }
        });
    }

    public void recordIntoPayActivity(String type,long guid) {

        ApiManager.intoPayActivityNum(String.valueOf(guid),UserPreference.getId(),"10001",type,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接成功，开始记时");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                LogUtil.v("AgoraHelper=VideoActivity==VideoPresenter","连接失败");
            }
        });
    }
}
