package com.wiscom.vchat.ui.homepage.presenter;

import android.text.TextUtils;

import com.wiscom.vchat.data.model.SearchCriteria;
import com.wiscom.vchat.data.preference.SearchPreference;
import com.wiscom.vchat.event.SearchEvent;
import com.wiscom.vchat.ui.dialog.CountrySelectDialog;
import com.wiscom.vchat.ui.dialog.LanguageSelectDialog;
import com.wiscom.vchat.ui.homepage.contract.SearchContract;
import com.wiscom.library.util.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public class SearchPresenter implements SearchContract.IPresenter {
    private SearchContract.IView mSearchView;

    public SearchPresenter(SearchContract.IView view) {
        this.mSearchView = view;
    }

    @Override
    public void start() {
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            mSearchView.setCountry(searchCriteria.getCountry());
            List<String> languageList = searchCriteria.getSpokenLanguage();
            if (!Utils.isListEmpty(languageList)) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String language : languageList) {
                    if (!TextUtils.isEmpty(language)) {
                        stringBuilder.append(language)
                                .append(",");
                    }
                }
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
                mSearchView.setLanguage(stringBuilder.toString());
            }
        }
    }

    @Override
    public void finish() {
        mSearchView = null;
    }

    @Override
    public void selectCountry() {
        CountrySelectDialog.show(mSearchView.obtainFragmentManager(), TextUtils.isEmpty(mSearchView.getCountry()) ? null : mSearchView.getCountry(),
                new CountrySelectDialog.OnCountrySelectListener() {
                    @Override
                    public void onSelected(String country) {
                        mSearchView.setCountry(country);
                        SearchPreference.saveSearchCountry(country);
                    }
                });
    }

    @Override
    public void selectLanguage() {
        List<String> selectedList = null;
        String selectedLanguages = mSearchView.getLanguage();
        if (!TextUtils.isEmpty(selectedLanguages)) {
            selectedList = Arrays.asList(selectedLanguages.split(","));
        }
        LanguageSelectDialog.show(mSearchView.obtainFragmentManager(), selectedList, new LanguageSelectDialog.OnLanguageSelectListener() {
            @Override
            public void onSelected(List<String> languages) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String language : languages) {
                    stringBuilder.append(language)
                            .append(",");
                }
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
                mSearchView.setLanguage(stringBuilder.toString());
                SearchPreference.saveSearchLanguage(stringBuilder.toString());
            }
        });
    }

    @Override
    public void startSearch() {
        // 发送事件，开始搜索
        EventBus.getDefault().post(new SearchEvent());
    }

}
