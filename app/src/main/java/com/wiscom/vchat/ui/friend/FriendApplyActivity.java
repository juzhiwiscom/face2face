package com.wiscom.vchat.ui.friend;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.event.RefreshFriendApplyListEven;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.widget.AutoSwipeRefreshLayout;
import com.wiscom.library.widget.XRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tianzhentao on 2018/1/3.
 */

public  class FriendApplyActivity extends BaseTopBarActivity implements FriendApplyContract.IView{
//    @BindView(R.id.withdraw_record)
//    FrameLayout mFlRecord;
    @BindView(R.id.activity_add_firend_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.activity_add_firend_xrecyerview)
    XRecyclerView mRecyclerView;
    private FriendAdapter mWithdrawRecordAdapter;
    private FriendApplyPresenter mWithdrawRecordPresenter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_friend_apply ;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.friend_apply);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mLlApplayFriend.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mWithdrawRecordAdapter = new FriendAdapter(mContext, R.layout.item_friend,true);
        mRecyclerView.setAdapter(mWithdrawRecordAdapter, R.layout.common_load_more);
        mWithdrawRecordPresenter = new FriendApplyPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mWithdrawRecordPresenter.refresh();
                }
            }
        });
        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mWithdrawRecordPresenter.loadMore();
            }
        });
        mIvLeft.setVisibility(View.VISIBLE);
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
//        mWithdrawRecordAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                VideoRecord videoRecord = mWithdrawRecordAdapter.getItemByPosition(position);
//                if (null != videoRecord) {
//                    UserBase userBase = videoRecord.getUserBase();
//                    if (null != userBase) {
//                        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount(),
//                                userBase.getNickName(), userBase.getIconUrlMininum()), getChildFragmentManager());
//                    }
//                }
//            }
//        });

    }

    @Override
    protected void loadData() {
        mWithdrawRecordPresenter.loadApplyList();

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mWithdrawRecordPresenter.finish();
//        刷新好友列表
        EventBus.getDefault().post(new UpdateFriendListEvent());
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
//        SnackBarUtil.showShort(mFlRecord, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mWithdrawRecordPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mWithdrawRecordPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }
    @Override
    public FriendAdapter getFriendApplyAdapter() {
        return mWithdrawRecordAdapter;
    }

    @Subscribe
    public void onEvent(RefreshFriendApplyListEven event) {
        mWithdrawRecordPresenter.loadApplyList();
    }
}