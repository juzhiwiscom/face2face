package com.wiscom.vchat.ui.pay;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.PayDict;
import com.wiscom.vchat.event.PaySuccessEvent;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.util.LogUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * 支付项适配器
 * Created by zhangdroid on 2017/6/10.
 */
public class PayAdapter extends CommonRecyclerViewAdapter<PayDict> {
    private PayPresenter mPayPresenter;
    private String serviceType;

    public void setPayPresenter(PayPresenter payPresenter) {
        this.mPayPresenter = payPresenter;
    }

    public PayAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public PayAdapter(Context context, int layoutResId, List<PayDict> dataList,String serviceType) {
        super(context, layoutResId, dataList);
        this.serviceType=serviceType;
    }

    @Override
    public void convert(final PayDict payDict, final int position, RecyclerViewHolder holder) {
        if (null != payDict) {
            if (!"1".equals(payDict.getIsvalid())) {// 商品不可用
                holder.getConvertView().setVisibility(View.GONE);
                removeItem(position);
            } else {
                if(serviceType.equals(C.pay.OPEN_VIP)){
                    holder.setBackgroudResource(R.id.pay_iv_icon,R.mipmap.open_vip_icon);
                }else{
                    holder.setBackgroudResource(R.id.pay_iv_icon,R.mipmap.buy_diamon);
                }
                holder.setText(R.id.item_pay_name, payDict.getServiceName());
                holder.setText(R.id.item_pay_purchase, TextUtils.concat(payDict.getCurrencyTag().toString(), payDict.getPrice()).toString());
                holder.setOnClickListener(R.id.item_pay_purchase, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 购买
                        if (null != mPayPresenter) {
                            switch (position) {
                                case 0:
                                    if(serviceType.equals(C.pay.OPEN_VIP)){
                                        mPayPresenter.purchase(C.pay.SKU_1_VIP);
//                                        payment(String.valueOf(payDict.getAddTime()),payDict.getServiceId(),"","");
                                    }else{
                                        mPayPresenter.purchase(C.pay.SKU_500_diamonds);
                                    }
                                    break;
                                case 1:
                                    if(serviceType.equals(C.pay.OPEN_VIP)){
                                        mPayPresenter.purchase(C.pay.SKU_3_VIP);
                                    }else{
                                        mPayPresenter.purchase(C.pay.SKU_1000_diamonds);
                                    }
                                    break;
                                case 2:
                                    if(serviceType.equals(C.pay.OPEN_VIP)){
                                        mPayPresenter.purchase(C.pay.SKU_12_VIP);
                                    }else{
                                        mPayPresenter.purchase(C.pay.SKU_3000_diamonds);
                                    }
                                    break;
                                case 3:
                                    mPayPresenter.purchase(C.pay.SKU_5000_diamonds);
                                    break;
                                case 4:
                                    mPayPresenter.purchase(C.pay.SKU_10000_diamonds);
                                    break;

                            }
                        }
                    }
                });
            }
        }

    }
//    测试接口使用
    private void payment(String payTime, String serviceId,String getPackageName,String getOrderId) {
        ApiManager.payment(payTime, serviceId, "1",getPackageName,getOrderId, new IGetDataListener<String>() {

            @Override
            public void onResult(String s, boolean isEmpty) {
                if (!TextUtils.isEmpty(s) && "success".equals(s)) {
                    // 后台支付成功，刷新用户商品信息
                    LogUtil.e("后台支付", "后台支付成功");
                    EventBus.getDefault().post(new PaySuccessEvent());
                } else {
                    // 后台支付结果请求失败
                    LogUtil.e("后台支付", "后台支付失败");
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                // 后台支付结果请求失败
                LogUtil.e("后台支付", "后台支付失败");
            }

        });
    }

}
