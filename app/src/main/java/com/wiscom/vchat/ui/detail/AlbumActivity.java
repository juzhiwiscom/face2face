package com.wiscom.vchat.ui.detail;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hyphenate.util.DensityUtil;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.ui.detail.contract.AlbumContract;
import com.wiscom.vchat.ui.detail.presenter.AlbumPresenter;
import com.wiscom.library.adapter.wrapper.HeaderAndFooterWrapper;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.SnackBarUtil;

import butterknife.BindView;

/**
 * 相册
 * Created by zhangdroid on 2017/5/27.
 */
public class AlbumActivity extends BaseTopBarActivity implements AlbumContract.IView {
    @BindView(R.id.activity_album_recyclerview)
    RecyclerView mRecyclerView;

    private AlbumPresenter albumPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_album;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_album);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        albumPresenter = new AlbumPresenter(this);
        albumPresenter.start();
    }

    @Override
    protected void setListeners() {
    }


    @Override
    protected void loadData() {
        albumPresenter.getPhotoInfo();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRecyclerView, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adater) {
        HeaderAndFooterWrapper headerAndFooterWrapper = new HeaderAndFooterWrapper(adater);
        headerAndFooterWrapper.setFullSpan(false);
        // 添加HeadView
        ImageView headView = new ImageView(mContext);
        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(DensityUtil.dip2px(mContext, 120),
                DensityUtil.dip2px(mContext, 120));
        int margin = DensityUtil.dip2px(mContext, 5);
        layoutParams.setMargins(margin, margin, margin, margin);
        headView.setLayoutParams(layoutParams);
        headView.setImageResource(R.mipmap.logo);
        headerAndFooterWrapper.addHeaderView(headView);
        headView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                albumPresenter.uploadImage();
            }
        });
        mRecyclerView.setAdapter(headerAndFooterWrapper);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        albumPresenter.finish();
    }

}
