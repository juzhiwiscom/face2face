package com.wiscom.vchat.ui.detail.presenter;

import android.os.Handler;
import android.text.TextUtils;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.UpLoadMyInfo;
import com.wiscom.vchat.data.model.UpLoadMyPhoto;
import com.wiscom.vchat.data.model.UploadInfoParams;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.UserInfoChangedEvent;
import com.wiscom.vchat.ui.detail.contract.EditInfoContract;
import com.wiscom.vchat.ui.dialog.AgeSelectDialog;
import com.wiscom.vchat.ui.dialog.CountrySelectDialog;
import com.wiscom.vchat.ui.dialog.LanguageSelectDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2017/6/9.
 */
public class EditInfoPresenter implements EditInfoContract.IPresenter {
    private EditInfoContract.IView mEditInfoView;

    public EditInfoPresenter(EditInfoContract.IView mEditInfoView) {
        this.mEditInfoView = mEditInfoView;
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mEditInfoView = null;
    }

    @Override
    public void setLocalInfo() {
        mEditInfoView.setUserAvator(UserPreference.getMiddleImage());
        mEditInfoView.setName(UserPreference.getNickname());
        mEditInfoView.setAge(UserPreference.getAge());
        mEditInfoView.setCountry(UserPreference.getCountry());
        mEditInfoView.setLanguage(UserPreference.getSpokenLanguage());
        mEditInfoView.setIntroducation(UserPreference.getIntroducation());
    }

    @Override
    public void upLoadMyInfo() {
        mEditInfoView.showLoading();
        UploadInfoParams uploadInfoParams = new UploadInfoParams();
        uploadInfoParams.setNickName(mEditInfoView.getName());
        uploadInfoParams.setAge(mEditInfoView.getAge());
        uploadInfoParams.setCountry(mEditInfoView.getCountry());
        uploadInfoParams.setSpokenLanguage(mEditInfoView.getLanguage());
        uploadInfoParams.setOwnWords(mEditInfoView.getIntroducation());
        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                mEditInfoView.dismissLoading();
                // 发送用户资料改变事件
                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
                // finish
                mEditInfoView.finishActivity();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mEditInfoView.dismissLoading();
                if (isNetworkError) {
                    mEditInfoView.showNetworkError();
                } else {
                    mEditInfoView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void upLoadAvator(File file, boolean photoType) {
        mEditInfoView.showLoading();
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mEditInfoView.dismissLoading();
                if (upLoadMyPhoto != null) {
                    UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
                            new Handler().postDelayed(new Runnable(){
                                public void run() {
                                    mEditInfoView.setUserAvator(stringFile);
                                }
                            }, 3000);
                        }
                    }
                }
                mEditInfoView.showTip("上传成功");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mEditInfoView.dismissLoading();
                if (isNetworkError) {
                    mEditInfoView.showNetworkError();
                } else {
                    mEditInfoView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void selectCountry() {
        CountrySelectDialog.show(mEditInfoView.obtainFragmentManager(), TextUtils.isEmpty(mEditInfoView.getCountry()) ? null : mEditInfoView.getCountry(),
                new CountrySelectDialog.OnCountrySelectListener() {
                    @Override
                    public void onSelected(String country) {
                        mEditInfoView.setCountry(country);
                    }
                });
    }

    @Override
    public void selectLanguage() {
        List<String> selectedList = null;
        String selectedLanguages = mEditInfoView.getLanguage();
        if (!TextUtils.isEmpty(selectedLanguages)) {
            selectedList = Arrays.asList(selectedLanguages.split(","));
        }
        LanguageSelectDialog.show(mEditInfoView.obtainFragmentManager(), selectedList, new LanguageSelectDialog.OnLanguageSelectListener() {
            @Override
            public void onSelected(List<String> languages) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String language : languages) {
                    stringBuilder.append(language)
                            .append(",");
                }
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
                mEditInfoView.setLanguage(stringBuilder.toString());
            }
        });
    }

    @Override
    public void selectAge() {
        String age = mEditInfoView.getAge();
        AgeSelectDialog.show(mEditInfoView.obtainFragmentManager(), TextUtils.isEmpty(age) ? null : age, new AgeSelectDialog.OnAgeSelectListener() {
            @Override
            public void onSelected(String age) {
                mEditInfoView.setAge(age);
            }
        });
    }

}
