package com.wiscom.vchat.ui.detail.presenter;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.ui.detail.contract.MyDetailContract;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public class MyDetailPresenter implements MyDetailContract.IPresenter {
    private MyDetailContract.IView mMyDetailView;

    public MyDetailPresenter(MyDetailContract.IView view) {
        this.mMyDetailView = view;
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mMyDetailView = null;
    }

    @Override
    public void getMyInfo() {
        mMyDetailView.showLoading();
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                // 更新用户信息
                if (null != myInfo) {
                    UserBase userBase = myInfo.getUserDetail().getUserBase();
                    if (null != userBase) {
                        UserPreference.saveUserInfo(userBase);
                        setRemoteInfo(userBase);
                    }
                    mMyDetailView.setUserPhotos(myInfo.getUserDetail().getUserPhotos());
                }
                mMyDetailView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                setLocalInfo();
                if (isNetworkError) {
                    mMyDetailView.showNetworkError();
                } else {
                    mMyDetailView.showTip(msg);
                }
                mMyDetailView.dismissLoading();
            }

        });
    }

    @Override
    public void updateMyInfo() {
        setLocalInfo();
    }

    /**
     * 设置从后台接口获取到的用户信息
     *
     * @param userBase
     */
    private void setRemoteInfo(UserBase userBase) {
        mMyDetailView.setNickname(userBase.getNickName());
        mMyDetailView.setGender(userBase.getGender() == 0);
        mMyDetailView.setUserInfo(String.valueOf(userBase.getAge()), userBase.getCountry(), userBase.getCity());
        mMyDetailView.setUserId(String.valueOf(userBase.getGuid()));
        mMyDetailView.setIntroducation(userBase.getOwnWords());
    }

    /**
     * 更新本地信息或从后台获取接口失败时
     */
    private void setLocalInfo() {
        mMyDetailView.setNickname(UserPreference.getNickname());
        mMyDetailView.setGender(UserPreference.isMale());
        mMyDetailView.setUserInfo(UserPreference.getAge(), UserPreference.getCountry(), UserPreference.getCity());
        mMyDetailView.setUserId(UserPreference.getId());
        mMyDetailView.setIntroducation(UserPreference.getIntroducation());
    }

}


