package com.wiscom.vchat.ui.videoshow;

import android.content.Context;
import android.widget.ImageView;

import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.recyclerview.CommonRecyclerViewAdapter;
import com.wiscom.vchat.common.recyclerview.RecyclerViewHolder;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.VideoSquare;

/**
 * Created by tianzhentao on 2018/5/4.
 */

public class VideoNewestAdapter extends CommonRecyclerViewAdapter<VideoSquare> {

    public VideoNewestAdapter(Context mContext, int item_video_newest) {
        super(mContext, item_video_newest);
    }

    @Override
    protected void convert(int position, RecyclerViewHolder viewHolder, VideoSquare bean) {
        if(bean!=null){
            ImageView ivPhoto = (ImageView) viewHolder.getView(R.id.newest_iv_photo);
            ImageView ivAvater = (ImageView) viewHolder.getView(R.id.newest_iv_avater);
            UserBase userBase = bean.getUserBase();
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrl())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivPhoto).build());
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).url(userBase.getIconUrlMininum())
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvater).build());
            viewHolder.setText(R.id.newest_iv_name,userBase.getNickName());
            viewHolder.setText(R.id.newest_tv_likes,bean.getUserVideoShow().getPraiseCount()+"");


        }


    }
}
