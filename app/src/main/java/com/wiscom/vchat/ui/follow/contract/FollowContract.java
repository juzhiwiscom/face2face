package com.wiscom.vchat.ui.follow.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;

/**
 * Created by Administrator on 2017/6/14.
 */

public interface FollowContract {
    interface IView extends BaseView {
        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();

        /**
         * 设置adapter
         *
         * @param adapter        adapter
         * @param loadMoreViewId 加载更多View
         */
        void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId);

        /**
         * 设置加载更多监听器
         *
         * @param onLoadMoreListener
         */
        void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();
        /**
         * 随着右边字母的滑动改变RecyclerView的位置
         *
         */
        void recyclerViewMoveTo(int i);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 加载关注用户列表
         *
         */
        void loadFollowUserList();

        /**
         * 下拉刷新
         */
        void refresh();
        /**
         * 获取滑动传递过来的字母
         */
        void getSlidingWord(String word);
        /**
         * 跳转搜索界面
         */
        void goToSearchPage();
    }
}
