package com.wiscom.vchat.ui.personalcenter.presenter;

import android.os.Parcelable;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.library.net.NetUtil;

/**
 * Created by tianzhentao on 2017/12/23.
 */

public class PersonActivity extends BaseTopBarActivity {
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_person;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
