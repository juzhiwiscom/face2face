package com.wiscom.vchat.ui.register;

import android.content.Context;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.base.CommonWebActivity;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.Register;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.parcelable.WebParcelable;
import com.wiscom.vchat.ui.login.LoginActivity;
import com.wiscom.vchat.ui.main.MainActivity;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class RegisterPresenter implements RegisterContract.IPresenter {
    private RegisterContract.IView mRegisterView;
    private Context mContext;
    private boolean isFirst=true;

    public RegisterPresenter(RegisterContract.IView view) {
        this.mRegisterView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mRegisterView = null;
        mContext = null;
    }

    @Override
    public void register() {
        mRegisterView.showLoading();
        ApiManager.register(mRegisterView.getNickname(), mRegisterView.getSex(), mRegisterView.getAge(),mRegisterView.getAvatarFile(),
                new IGetDataListener<Register>() {
                    @Override
                    public void onResult(Register register, boolean isEmpty) {
                        // 保存用户信息到本地
                        UserBase userBase = register.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
//                            huanXin(userBase);
                            handleHyphenateLoginResult();

                        } else {
//                            if(isFirst)
                            handleHyphenateLoginResult();
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        mRegisterView.dismissLoading();
                        if (isNetworkError) {
                            mRegisterView.showNetworkError();
                        } else {
                            mRegisterView.showTip(msg);
                        }
                    }
                });
    }

    private void huanXin(final UserBase userBase) {
        // 登录环信
        HyphenateHelper.getInstance().login(userBase.getAccount(), userBase.getPassword(), new HyphenateHelper.OnLoginCallback() {
            @Override
            public void onSuccess() {
                if(isFirst)
                    handleHyphenateLoginResult();
                LogUtil.v("HMP==","环信登陆成功");
            }

            @Override
            public void onFailed() {
                huanXin(userBase);
                if(isFirst)
                    handleHyphenateLoginResult();
                LogUtil.v("HMP==","环信登陆失败");

            }
        });
    }

    @Override
    public void goLoginPage() {
        LaunchHelper.getInstance().launch(mContext, LoginActivity.class);
    }

    @Override
    public void showAgreement(String title, String url) {
        LaunchHelper.getInstance().launch(mContext, CommonWebActivity.class,
                new WebParcelable(title, url, false));
    }

    @Override
    public void checkValid() {
        mRegisterView.setRegisterEnable();
    }

    private void handleHyphenateLoginResult() {
//        isFirst=false;
        if(mContext==null){
            mContext= BaseApplication.getGlobalContext();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.register_success));
            }
        });
        if(mRegisterView!=null){
            mRegisterView.dismissLoading();
        }
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        // 跳转主页面
        LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
    }


}
