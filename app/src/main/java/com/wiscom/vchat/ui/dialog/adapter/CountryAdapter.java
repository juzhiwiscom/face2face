package com.wiscom.vchat.ui.dialog.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseBooleanArray;

import com.wiscom.vchat.R;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;

import java.util.List;

/**
 * 国家选择（单选）适配器
 * Created by zhangdroid on 2017/6/9.
 */
public class CountryAdapter extends CommonRecyclerViewAdapter<String> {
    // 标记item是否选中的集合
    private SparseBooleanArray mSelectArray;
    // 当前选中的Item索引
    private int mSelectedPosition;

    /**
     * 初始化列表中Item的选中状态
     *
     * @param selectedText 已经选中的Item
     */
    public void initSelectState(String selectedText) {
        if (!TextUtils.isEmpty(selectedText)) {
            List<String> dataList = getAdapterDataList();
            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).equals(selectedText)) {
                    mSelectedPosition = i;
                    mSelectArray.put(i, true);
                } else {
                    mSelectArray.put(i, false);
                }
            }
        } else {
            // 默认所有item都不选中
            for (int i = 0; i < getSize(); i++) {
                mSelectArray.put(i, false);
            }
        }
    }

    public CountryAdapter(Context context, int layoutResId, List<String> dataList) {
        super(context, layoutResId, dataList);
        mSelectArray = new SparseBooleanArray(dataList.size());
    }

    @Override
    public void convert(final String s, final int position, RecyclerViewHolder holder) {
        if (!TextUtils.isEmpty(s)) {
            holder.setText(R.id.item_dialog_country, s);
        }
        holder.setVisibility(R.id.item_dialog_country_checked, isItemSelected(position));
    }

    private boolean isItemSelected(int position) {
        return mSelectArray.get(position);
    }

    public void setItemSelectState(int position, boolean selected) {
        mSelectArray.put(position, selected);
    }

    public int getSelectedPosition() {
        return mSelectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.mSelectedPosition = selectedPosition;
    }

    /**
     * @return 返回选中的item
     */
    public String getSelectedItem() {
        for (int i = 0; i < getSize(); i++) {
            if (mSelectArray.get(i)) {
                return getAdapterDataList().get(i);
            }
        }
        return null;
    }

}
