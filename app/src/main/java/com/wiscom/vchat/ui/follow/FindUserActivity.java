package com.wiscom.vchat.ui.follow;

import android.content.Context;
import android.os.Parcelable;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.ui.follow.contract.FindUserContract;
import com.wiscom.vchat.ui.follow.presenter.FindUserPresenter;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.SnackBarUtil;

import butterknife.BindView;

/**
 * 查找用户页面
 * Created by zhangdroid on 2017/5/27.
 */
public class FindUserActivity extends BaseTopBarActivity implements FindUserContract.IView,View.OnClickListener{
    @BindView(R.id.find_user_activity_tv_find)
    TextView tv_find;
    @BindView(R.id.find_user_activity_et_id)
    EditText et_id;
    @BindView(R.id.find_user_activity_rl_root)
    RelativeLayout mLlRoot;
    private FindUserPresenter mFindUserPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_find_user;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.follow_find_user);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mFindUserPresenter=new FindUserPresenter(this);
    }

    @Override
    protected void setListeners() {
        tv_find.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getRemoteId() {
        return et_id.getText().toString().trim();
    }


    @Override
    public void canGoToUserDetailActivity(String id) {
         mFindUserPresenter.goToUserDetailActivity(id);
    }

    @Override
    public void showIdOrMessageError(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_user_activity_tv_find:
                mFindUserPresenter.setOnclick();
                break;
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mFindUserPresenter.finish();
    }
}
