package com.wiscom.vchat.ui.detail.presenter;

import android.content.Context;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UpLoadMyPhoto;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.parcelable.BigPhotoParcelable;
import com.wiscom.vchat.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscom.vchat.ui.detail.contract.AlbumContract;
import com.wiscom.vchat.ui.photo.BigPhotoActivity;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.util.LaunchHelper;

import java.io.File;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */
public class AlbumPresenter implements AlbumContract.IPresenter {
    private AlbumContract.IView mAlbumIview;
    private Context mContext;
    private AlbumPhotoAdapter mAlbumPhotoAdapter;

    public AlbumPresenter(AlbumContract.IView mAlbumIview) {
        this.mAlbumIview = mAlbumIview;
        this.mContext = mAlbumIview.obtainContext();
    }

    @Override
    public void start() {
        mAlbumPhotoAdapter = new AlbumPhotoAdapter(mContext, R.layout.album_recyclerview_item);
        mAlbumIview.setAdapter(mAlbumPhotoAdapter);
    }

    @Override
    public void finish() {
        mAlbumIview = null;
        mContext = null;
        mAlbumPhotoAdapter = null;
    }

    @Override
    public void getPhotoInfo() {
        mAlbumIview.showLoading();
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                mAlbumIview.dismissLoading();
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        mAlbumPhotoAdapter.bind(userPhotos);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAlbumIview.dismissLoading();
                if (isNetworkError) {
                    mAlbumIview.showNetworkError();
                } else {
                    mAlbumIview.showTip(msg);
                }
            }
        });
        mAlbumPhotoAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                List<UserPhoto> photoList = mAlbumPhotoAdapter.getAdapterDataList();
                LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                        Util.convertPhotoUrl(photoList)));
            }
        });
        mAlbumPhotoAdapter.setOnItemLongClickListener(new MultiTypeRecyclerViewAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, int position, RecyclerViewHolder viewHolder) {
                //删除图片
            }
        });
    }

    @Override
    public void uploadImage() {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(mAlbumIview.obtainFragmentManager());
                    ApiManager.upLoadMyPhotoOrAvator(file, false, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                if (userPhoto != null) {
                                    List<UserPhoto> adapterDataList = mAlbumPhotoAdapter.getAdapterDataList();
                                    if (adapterDataList != null && adapterDataList.size() > 0) {
                                        adapterDataList.add(userPhoto);
                                        mAlbumPhotoAdapter.bind(adapterDataList);
                                    }
                                }
                            }
                            mAlbumIview.showTip("上传成功");
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                                mAlbumIview.showNetworkError();
                            } else {
                                mAlbumIview.showTip(msg);
                            }
                        }
                    });
                }
            }
        });
    }

}
