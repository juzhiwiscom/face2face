package com.wiscom.vchat.ui.follow.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.UserDetailforOther;
import com.wiscom.vchat.parcelable.UserDetailParcelable;
import com.wiscom.vchat.ui.detail.UserDetailActivity;
import com.wiscom.vchat.ui.follow.contract.FindUserContract;
import com.wiscom.library.util.LaunchHelper;

/**
 * Created by Administrator on 2017/6/15.
 */

public class FindUserPresenter implements FindUserContract.IPresenter{
    private FindUserContract.IView mFildUserView;
    private Context mContext;

    public FindUserPresenter(FindUserContract.IView mFildUserView) {
        this.mFildUserView = mFildUserView;
        this.mContext=mFildUserView.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mFildUserView=null;
        mContext=null;
    }

    @Override
    public void goToUserDetailActivity(String id) {
        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class, new UserDetailParcelable(id));
    }

    @Override
    public void setOnclick() {
        final String remoteId = mFildUserView.getRemoteId();
        if(!TextUtils.isEmpty(remoteId)){
            ApiManager.getUserInfo(remoteId, new IGetDataListener<UserDetailforOther>() {
                @Override
                public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                    if(userDetailforOther!=null){
                        String isSucced = userDetailforOther.getIsSucced();
                        if (isSucced.equals("1")) {
                            mFildUserView.canGoToUserDetailActivity(remoteId);
                        }else{
                            mFildUserView.showIdOrMessageError("抱歉，您所寻找的聊友不存在或已经注销");
                        }
                    }
                }
                @Override
                public void onError(String msg, boolean isNetworkError) {
                    mFildUserView.showIdOrMessageError("抱歉，您所寻找的聊友不存在或已经注销");
                }
            });
        }else{
          mFildUserView.showTip("请输入对方ID");
        }
    }
}
