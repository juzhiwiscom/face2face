package com.wiscom.vchat.ui.personalcenter.presenter;

import android.content.ClipboardManager;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.FaceBookAccount;
import com.wiscom.vchat.data.model.UpLoadMyPhoto;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.ui.personalcenter.contract.AuthenticationContract;

import java.io.File;

/**
 * Created by Administrator on 2017/6/16.
 */

public class AuthenticationPresenter implements AuthenticationContract.IPresenter{
    private AuthenticationContract.IView mAuthenticationIview;
    private Context mContext;
    private File videoFile;
    public AuthenticationPresenter(AuthenticationContract.IView mAuthenticationIview) {
        this.mAuthenticationIview = mAuthenticationIview;
        this.mContext=mAuthenticationIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mAuthenticationIview=null;
        mContext=null;
    }

    @Override
    public void goVideoRecordPage() {

    }

    @Override
    public void getUserInfo() {
    }


    @Override
    public void upLoadAvator(File file, boolean photoType) {
        mAuthenticationIview.showLoading();
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mAuthenticationIview.dismissLoading();
                if (upLoadMyPhoto != null) {
                    UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        if (!TextUtils.isEmpty(stringFile)) {
                            new Handler().postDelayed(new Runnable(){
                                public void run() {
                                    mAuthenticationIview.getAvatarUrl(stringFile);
                                }
                            }, 3000);

                        }
                    }
                }
                mAuthenticationIview.showTip("上传成功");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAuthenticationIview.dismissLoading();
                if (isNetworkError) {
                    mAuthenticationIview.showNetworkError();
                } else {
                    mAuthenticationIview.showTip(msg);
                }
            }
        });   
    }
 
    @Override
    public void startRecord() {


    }

    @Override
    public void startPlay() {

    }

    @Override
    public void reRecord() {

    }

    @Override
    public void getFaceBookAccount() {
        String middleImage = UserPreference.getMiddleImage();
            mAuthenticationIview.getAvatarUrl(middleImage);
        ApiManager.getFaceBookAccount(new IGetDataListener<FaceBookAccount>() {
            @Override
            public void onResult(FaceBookAccount faceBookAccount, boolean isEmpty) {
                if(faceBookAccount!=null){
                    mAuthenticationIview.setFaceBookAccount(faceBookAccount.getGoldHostFacebook());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void submitCheck(String videoNumber, File file) {
        if(!TextUtils.isEmpty(videoNumber)&&file!=null){
            ApiManager.submitCheck(videoNumber,file, new IGetDataListener<String>() {
                @Override
                public void onResult(String s, boolean isEmpty) {
                    mAuthenticationIview.finishActivity();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        }else{
            mAuthenticationIview.showTip("请先上传视频");
        }
    }

    @Override
    public void copyFacebookAccount() {
        ClipboardManager clip = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        if(!TextUtils.isEmpty(mAuthenticationIview.copyFacebookAccount())){
            clip.setText(mAuthenticationIview.copyFacebookAccount()); // 复制
            mAuthenticationIview.showTip("已复制到剪贴板");
        }
    }


}
