package com.wiscom.vchat.ui.personalcenter;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.FileUtil;
import com.wiscom.library.util.ToastUtil;
import com.wiscom.library.widget.PlayView;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.event.UpdateMyInfo;
import com.wiscom.vchat.parcelable.PlayViewParcelable;
import com.wiscom.vchat.view.beauty.core.MediaType;
import com.wiscom.vchat.view.beauty.filter.GPUImageFilter;
import com.wiscom.vchat.view.beauty.filter.MagicCameraDisplay;
import com.wiscom.vchat.view.beauty.utils.MagicFilterFactory;
import com.wiscom.vchat.view.beauty.utils.MagicFilterType;
import com.wiscom.vchat.view.camera.CameraInterface;
import com.wiscom.vchat.view.utils.DisplayUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.wiscom.library.util.DeviceUtil.getScreenHeight;
import static com.wiscom.library.util.DeviceUtil.getScreenWidth;
import static com.wiscom.vchat.C.Video.SIZE_1;
import static com.wiscom.vchat.C.Video.SIZE_2;

/**
 * Created by Administrator on 2017/6/20.
 * 录音的Activity
 */

public class RecordActivity extends BaseFragmentActivity implements SurfaceHolder.Callback, View.OnClickListener {
    @BindView(R.id.activity_record_camera_show_view)
    GLSurfaceView cameraShowView;
//    @BindView(R.id.activity_record_camera_glsurfaceview)
//    GLSurfaceView glSurfaceView;
    @BindView(R.id.activity_record_video_flash_light)
    ImageView videoFlashLight;
    @BindView(R.id.activity_record_video_time)
    Chronometer videoTime;
    @BindView(R.id.activity_record_swicth_camera)
    ImageView swicthCamera;
    @BindView(R.id.activity_record_iv_start)
    ImageView recordButton;
    @BindView(R.id.activity_record_iv_finish)
    ImageView iv_finish;
    @BindView(R.id.activity_record_tv_second_button)
    TextView tv_second_button;
    @BindView(R.id.activity_record_tv_random)
    TextView tv_random;
    @BindView(R.id.record_warn)
    RelativeLayout recordWarn;
    @BindView(R.id.record_playview)
    PlayView playView;
    @BindView(R.id.record_iv_play)
    ImageView mIvPlay;
    @BindView(R.id.record_iv_re_record)
    ImageView mIvReRecord;
    @BindView(R.id.record_ll_finish)
    LinearLayout mLlFinish;
    @BindView(R.id.record_bootom)
    RelativeLayout recordBootom;
    @BindView(R.id.vedio_iv_paly_baground)
    ImageView ivPlayBground;


    MediaRecorder recorder;
    SurfaceHolder surfaceHolder;

    Camera camera;

    OrientationEventListener orientationEventListener;

    File videoFile;

    int rotationRecord = 90;

    int rotationFlag = 90;

    int flashType;

    int frontRotate;

    int frontOri;

    int cameraType = 1;

    int cameraFlag = 0; //1为后置

    boolean flagRecord = false;//是否正在录像
    public final static String DATA = "URL";
    public final static String VIDEO_URL = "VIDEO_URL";
    public final static String VIDEO_IMAGE_URL = "VIDEO_IMAGE_URL";
    String uri;
    CountDownTimer countDownTimer;
    //结果码
    public static final int RESULT_CODE = 2001;

    private long playPostion = -1;
    private long duration = -1;

    private PlayViewParcelable mPlayViewParcelable;
    private String random;
    private String palyUrl;
    private boolean recordFinish=false;
    private String videoUrl;
    private String videoImageUrl;
    private boolean isFirstClick=true;
    private File imageFile;
    private MagicCameraDisplay mMagicCameraDisplay;
    private boolean preparedFinish=false;
    private boolean clickRecord=false;
//    private boolean mIsRecording = true;



    @Override
    protected int getLayoutResId() {
        return R.layout.activity_record;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
//        mPlayViewParcelable= (PlayViewParcelable) parcelable;
//        if(!TextUtils.isEmpty(mPlayViewParcelable.urlData)){
//            videoFile=new File(mPlayViewParcelable.urlData);
//        }
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        videoUrl = getIntent().getStringExtra(VIDEO_URL);
        videoImageUrl = getIntent().getStringExtra(VIDEO_IMAGE_URL);
        uri = getIntent().getStringExtra(DATA);
        if (!TextUtils.isEmpty(uri)) {
            videoFile = new File(uri);
            try {
                if (!videoFile.exists() && !videoFile.isDirectory()) {
                    videoFile.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(!TextUtils.isEmpty(videoUrl)){
//            打开播放功能
            if(!TextUtils.isEmpty(videoImageUrl)){
                ivPlayBground.setVisibility(View.VISIBLE);
                ImageLoaderUtil.getInstance().loadImage(RecordActivity.this, new ImageLoader.Builder().url(videoImageUrl)
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivPlayBground).build());
            }
            rePlayUI();
            swicthCamera.setVisibility(View.GONE);
            palyUrl = videoUrl;
            initPlayView();

            recordFinish=true;
        }else{
            initView();
        }
    }


    @Override
    protected void setListeners() {
        videoFlashLight.setOnClickListener(this);
        swicthCamera.setOnClickListener(this);
        recordButton.setOnClickListener(this);
        iv_finish.setOnClickListener(this);
        mIvPlay.setOnClickListener(this);
        mIvReRecord.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (playView.getVisibility() == View.VISIBLE) {
            playView.pause();
            playPostion = playView.getCurrentPosition();
        }
        try {
            if (flagRecord) {
                endRecord();
                if (camera != null && cameraType == 0) {
                    //关闭后置摄像头闪光灯
                    camera.lock();
                    flashLogic(camera.getParameters(), 0, true);
                    camera.unlock();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        CameraInterface.getInstance().onPause();
        if (flagRecord &&mMagicCameraDisplay!=null) {
            mMagicCameraDisplay.stopRecording();
        }

    }

    @Override
    public void onBackPressed() {
        if (flagRecord) {
            //如果是录制中的就完成录制
            onPause();
            return;
        }
        super.onBackPressed();
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_record_video_flash_light:
                clickFlash();
                break;
            case R.id.activity_record_swicth_camera:
                if(recordFinish){
//                    录制完成，点击保存视频按钮
                    saveVedio();
                }else{
//                    切换传摄像头
//                    switchCamera();
                    mMagicCameraDisplay.switchCamera();
                }
                break;
            case R.id.activity_record_iv_start:
                recordWarn.setVisibility(View.GONE);
                tv_second_button.setVisibility(View.VISIBLE);
//                clickRecord();

                flagRecord = true;
                mMagicCameraDisplay.startRecording();
                startRecordUI();
                countDownTimer.start();


                break;
            case R.id.activity_record_iv_finish:
                finish();
                break;
            case R.id.record_iv_play:

                if(!preparedFinish&&!clickRecord){
                    LoadingDialog.showNoCanceled(getSupportFragmentManager());
                }else{
                    LoadingDialog.hide();
                    ivPlayBground.setVisibility(View.GONE);
                }
                play();
                playingUI();
                break;
            case R.id.record_iv_re_record:
                preparedFinish=true;
                clickRecord=true;
                recordFinish=false;
                swicthCamera.setImageResource(R.mipmap.record_switch);
                reRecoedUI();
                if(videoUrl!=null && isFirstClick){
                    isFirstClick=false;
                    initView();
                }
                break;
        }
    }

    private void saveVedio() {
//        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//        mmr.setDataSource(mMagicCameraDisplay.getMediaOutPath());
//        Bitmap bitmap = mmr.getFrameAtTime();//获取第一帧图片
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(mMagicCameraDisplay.getMediaOutPath(), MediaStore.Images.Thumbnails.MINI_KIND);
        try {
            imageFile = saveFile(bitmap, C.Video.SD_IMAGE_PATH, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
//            ivPlayBground.setImageBitmap(bitmap);
//        mmr.release();//释放资源
        if (!TextUtils.isEmpty(mMagicCameraDisplay.getMediaOutPath())) {
            videoFile = new File(mMagicCameraDisplay.getMediaOutPath());
            try {
                if (!videoFile.exists() && !videoFile.isDirectory()) {
                    videoFile.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        上传视频
        if(videoFile!=null&&imageFile!=null){

            LoadingDialog.show(getSupportFragmentManager());

            ApiManager.uploadVedio(videoFile,imageFile, new IGetDataListener<String>() {
                @Override
                public void onResult(String s, boolean isEmpty) {
                    LoadingDialog.hide();
//                    更新个人信息接口
                    EventBus.getDefault().post(new UpdateMyInfo());
                    ToastUtil.showShortToast(RecordActivity.this,getString(R.string.uploaded_successfully));

                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    LoadingDialog.hide();
                }
            });
        }
    }

    /**
     * 将Bitmap转换成文件
     * 保存文件
     * @param bm
     * @param fileName
     * @throws IOException
     */
    public static File saveFile(Bitmap bm,String path, String fileName) throws IOException {
        File dirFile = new File(path);
        if(!dirFile.exists()){
            dirFile.mkdir();
        }
        File myCaptureFile = new File(path,".jpg");
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
        bos.flush();
        bos.close();
        return myCaptureFile;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        initCamera(cameraType, false);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        surfaceHolder = holder;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        endRecord();
        releaseCamera();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        endRecordUI();
    }

    private void initView() {
//        random = getRandom();
        tv_random.setText(random);
        countDownTimer = new CountDownTimer(11000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tv_second_button.setText(millisUntilFinished / 1000 + "");
            }

            @Override
            public void onFinish() {
                if (flagRecord) {
                    endRecord();
                }
            }
        };
//        doStartSize();
//        SurfaceHolder holder = cameraShowView.getHolder();
//        holder.addCallback(this);
//        // setType必须设置，要不出错.
//        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

//        设置美颜

        mMagicCameraDisplay = new MagicCameraDisplay(this, cameraShowView, MediaType.MEDIA_RECORD);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                openBeauty();
            }
        }, 1000);
        float previewRate = DisplayUtil.getScreenRate(this); // 默认全屏的比例预览
        CameraInterface.getInstance().setPreviewRate(previewRate);
//        mMagicCameraDisplay.startRecording();

        mMagicCameraDisplay.onResume();

        rotationUIListener();


    }
    protected void openBeauty() {
        GPUImageFilter mFilter = MagicFilterFactory.getFilters(
                MagicFilterType.BEAUTY, this);
        mMagicCameraDisplay.setFilter(mFilter);
    }
    @Override
    protected void onResume() {
        super.onResume();



    }


    @Override
    protected void onStop() {
        super.onStop();
        CameraInterface.getInstance().onPause();
        if (flagRecord &&mMagicCameraDisplay!=null) {
            mMagicCameraDisplay.stopRecording();
        }
    }



    //获取4为的0到9的随机数
    public String getRandom() {
        int v1 = (int) (Math.random() * 10);
        int v2 = (int) (Math.random() * 10);
        int v3 = (int) (Math.random() * 10);
        int v4 = (int) (Math.random() * 10);
        String s1 = String.valueOf(v1);
        String s2 = String.valueOf(v2);
        String s3 = String.valueOf(v3);
        String s4 = String.valueOf(v4);
        String s = s1 + " " + s2 + " " + s3 + " " + s4;
        return s;
    }
    /**
     * 播放前和播放后的状态
     */
    private void rePlayUI() {
        recordWarn.setVisibility(View.GONE);
        mLlFinish.setVisibility(View.VISIBLE);
        cameraShowView.setVisibility(View.GONE);
        recordBootom.setVisibility(View.GONE);
        iv_finish.setVisibility(View.VISIBLE);
    }
    /**
     * 播放中的状态
     */
    private void playingUI() {
        mLlFinish.setVisibility(View.GONE);
    }
    /**
     * 点击重新录制后的状态／录制前的状态
     */
    private void reRecoedUI() {
        ivPlayBground.setVisibility(View.GONE);
        cameraShowView.setVisibility(View.VISIBLE);
        playView.setVisibility(View.GONE);
        mLlFinish.setVisibility(View.GONE);
        recordWarn.setVisibility(View.VISIBLE);
        swicthCamera.setVisibility(View.VISIBLE); // 旋转摄像头打开
        recordBootom.setVisibility(View.VISIBLE);
        iv_finish.setVisibility(View.VISIBLE);
        recordButton.setVisibility(View.VISIBLE);
        tv_second_button.setVisibility(View.GONE);
    }

    /**
     * 开始录制时候的状态
     */
    private void startRecordUI() {
        swicthCamera.setVisibility(View.GONE); // 旋转摄像头关闭
//        videoFlashLight.setVisibility(View.GONE); //闪光灯关闭
        iv_finish.setVisibility(View.GONE);
        recordButton.setVisibility(View.GONE);
        tv_second_button.setVisibility(View.VISIBLE);
    }

    /**
     * 停止录制时候的状态
     */
    private void endRecordUI() {
        swicthCamera.setVisibility(View.VISIBLE); // 旋转摄像头关闭
//        videoFlashLight.setVisibility(View.VISIBLE); //闪光灯关闭
        iv_finish.setVisibility(View.VISIBLE);
        recordButton.setVisibility(View.VISIBLE);
        tv_second_button.setVisibility(View.GONE);
        recordButton.setImageResource(R.mipmap.record_btn);   //录制按钮变成待停止
    }

    /**
     * 录制按键
     */
    private void clickRecord() {
        if (!flagRecord) {
            if (startRecord()) {
                startRecordUI();
                countDownTimer.start();
            }
        } else {
            endRecord();
        }
    }

    /**
     * 旋转界面UI
     */
    private void rotationUIListener() {
        orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int rotation) {
                if (!flagRecord) {
                    if (((rotation >= 0) && (rotation <= 30)) || (rotation >= 330)) {
                        // 竖屏拍摄
                        if (rotationFlag != 0) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 0);
                            //这是竖屏视频需要的角度
                            rotationRecord = 90;
                            //这是记录当前角度的flag
                            rotationFlag = 0;
                        }
                    } else if (((rotation >= 230) && (rotation <= 310))) {
                        // 横屏拍摄
                        if (rotationFlag != 90) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 90);
                            //这是正横屏视频需要的角度
                            rotationRecord = 0;
                            //这是记录当前角度的flag
                            rotationFlag = 90;
                        }
                    } else if (rotation > 30 && rotation < 95) {
                        // 反横屏拍摄
                        if (rotationFlag != 270) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 270);
                            //这是反横屏视频需要的角度
                            rotationRecord = 180;
                            //这是记录当前角度的flag
                            rotationFlag = 270;
                        }
                    }
                }
            }
        };
        orientationEventListener.enable();
    }


    private void rotationAnimation(int from, int to) {
        ValueAnimator progressAnimator = ValueAnimator.ofInt(from, to);
        progressAnimator.setDuration(300);
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int currentAngle = (int) animation.getAnimatedValue();
                videoFlashLight.setRotation(currentAngle);
                // videoTime.setRotation(currentAngle);
                swicthCamera.setRotation(currentAngle);
            }
        });
        progressAnimator.start();
    }

    /**
     * 因为录制改分辨率的比例可能和屏幕比例一直，所以需要调整比例显示
     */
    private void doStartSize() {
        int screenWidth = getScreenWidth(this);
        int screenHeight = getScreenHeight(this);
        setViewSize(cameraShowView, screenWidth * SIZE_1 / SIZE_2, screenHeight);
//        setViewSize(cameraShowView, screenWidth, screenHeight);
    }

    public static void setViewSize(View view, int width, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (null == layoutParams)
            return;
        layoutParams.width = width;
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    /**
     * 初始化相机
     *
     * @param type    前后的类型
     * @param flashDo 赏光灯是否工作
     */
    private void initCamera(int type, boolean flashDo) {

        if (camera != null) {
            //如果已经初始化过，就先释放
            releaseCamera();
        }

        try {
            camera = Camera.open(type);
            if (camera == null) {
                return;
            }
            camera.lock();

            //Point screen = new Point(getScreenWidth(this), getScreenHeight(this));
            //现在不用获取最高的显示效果
            //Point show = getBestCameraShow(camera.getParameters(), screen);

            Camera.Parameters parameters = camera.getParameters();
            if (type == 0) {
                //基本是都支持这个比例
                parameters.setPreviewSize(SIZE_1, SIZE_2);
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);//1连续对焦
                camera.cancelAutoFocus();// 2如果要实现连续的自动对焦，这一句必须加上
            }
            camera.setParameters(parameters);
            flashLogic(camera.getParameters(), flashType, flashDo);
            if (cameraType == 1) {
                frontCameraRotate();
                camera.setDisplayOrientation(frontRotate);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    camera.setDisplayOrientation(270);//90
                } else {
                    camera.setDisplayOrientation(90);
                }
            }
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
            camera.unlock();
        } catch (Exception e) {
            e.printStackTrace();
            releaseCamera();
        }
    }

    private boolean startRecord() {

        //懒人模式，根据闪光灯和摄像头前后重新初始化一遍，开期闪光灯工作模式
        initCamera(cameraType, true);

        if (recorder == null) {
            recorder = new MediaRecorder();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || camera == null || recorder == null) {
            camera = null;
            recorder = null;
            return false;
        }
        try {
            recorder.setCamera(camera);
            // 这两项需要放在setOutputFormat之前
            recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            // Set output file format，输出格式
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

            //必须在setEncoder之前
            recorder.setVideoFrameRate(15);  //帧数  一分钟帧，15帧就够了
            recorder.setVideoSize(SIZE_1, SIZE_2);//这个大小就够了

            // 这两项需要放在setOutputFormat之后
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

            recorder.setVideoEncodingBitRate(3 * SIZE_1 * SIZE_2);//第一个数字越大，清晰度就越高，考虑文件大小的缘故，就调整为1
            int frontRotation;
            if (rotationRecord == 180) {
                //反向的前置
                frontRotation = 180;
            } else {
                //正向的前置
                frontRotation = (rotationRecord == 0) ? 270 - frontOri : frontOri; //录制下来的视屏选择角度，此处为前置
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                recorder.setOrientationHint((cameraType == 1) ? 270 : 270);
            } else {
                recorder.setOrientationHint((cameraType == 1) ? frontRotation : rotationRecord);
            }
            //把摄像头的画面给它
            recorder.setPreviewDisplay(surfaceHolder.getSurface());
            //创建好视频文件用来保存
            if (videoFile != null) {
                //设置创建好的输入路径
                recorder.setOutputFile(videoFile.getPath());
                recorder.prepare();
                recorder.start();
                //不能旋转啦
                orientationEventListener.disable();
                flagRecord = true;
            }
        } catch (Exception e) {
            //一般没有录制权限或者录制参数出现问题都走这里
            e.printStackTrace();
            //还是没权限啊
            recorder.reset();
            recorder.release();
            recorder = null;
            FileUtil.deleteFile(videoFile.getPath());
            return false;
        }
        return true;
    }

    private void endRecord() {
        //反正多次进入，比如surface的destroy和界面onPause
        if (!flagRecord) {
            return;
        }
//        flagRecord = false;
//        try {
//            if (recorder != null) {
//                recorder.stop();
//                recorder.reset();
//                recorder.release();
//                orientationEventListener.enable();
//                recorder = null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        开始准备播放
//        palyUrl = videoFile.getAbsolutePath();
        flagRecord = false;
        mMagicCameraDisplay.stopRecording();
        final String mediaOutPath = mMagicCameraDisplay.getMediaOutPath();
        palyUrl = mediaOutPath;
        LoadingDialog.show(getSupportFragmentManager());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                 MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(mediaOutPath);
                Bitmap bitmap = mmr.getFrameAtTime();//获取第一帧图片
                if(bitmap==null){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(mediaOutPath, MediaStore.Images.Thumbnails.MINI_KIND);


//                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//                            mmr.setDataSource(mediaOutPath);
//                            Bitmap bitmap = mmr.getFrameAtTime();//获取第一帧图片
                            if(bitmap==null){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(mediaOutPath, MediaStore.Images.Thumbnails.MINI_KIND);
//                                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//                                        mmr.setDataSource(mediaOutPath);
//                                        Bitmap bitmap = mmr.getFrameAtTime();//获取第一帧图片
                                        if(bitmap!=null){
                                            recordFinish(bitmap);
                                        }else{
                                            rePlayUI();
                                            initPlayView();
                                            if(mMagicCameraDisplay!=null){
                                                swicthCamera.setImageResource(R.mipmap.record_save_btn);
                                                swicthCamera.setVisibility(View.VISIBLE);
                                            }
                                            recordFinish=true;
                                            LoadingDialog.hide();
                                        }
                                    }
                                },2000);
                            }
                        }
                    },500);
                }else{
                    recordFinish(bitmap);
                }
            }
        },500);




//        Intent intent = new Intent(this, AuthenticationActivity.class);
//        //将结果返回上个界面
//        intent.putExtra(C.Video.DATA, videoFile.getAbsolutePath());
//        intent.putExtra("videoNumber", random);
//        setResult(RESULT_CODE, intent);
//        finish();
    }

    private void recordFinish(Bitmap bitmap) {
//        int i = new Random().nextInt();
//        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +"videorecordtre" + File.separator + i+ "recordtreimage.jpg";
//        try {
//            imageFile = saveFile(bitmap, path, null);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mmr.release();//释放资源
        ivPlayBground.setVisibility(View.VISIBLE);
        ivPlayBground.setImageBitmap(bitmap);
//        String absolutePath = imageFile.getAbsolutePath();
//        ImageLoaderUtil.getInstance().loadImage(RecordActivity.this, new ImageLoader.Builder().url(absolutePath)
//                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivPlayBground).build());

        rePlayUI();
//        initPlayView();
        if(mMagicCameraDisplay!=null){
            swicthCamera.setImageResource(R.mipmap.record_save_btn);
            swicthCamera.setVisibility(View.VISIBLE);
        }
        recordFinish=true;
        LoadingDialog.hide();
        initPlayView();
    }

    //    初始化播放视频
    private void initPlayView() {
        if (!TextUtils.isEmpty(palyUrl)) {
            preparedFinish=false;
            playView.setVisibility(View.VISIBLE);
            playView.setVideoURI(Uri.parse(palyUrl));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    playView.seekTo(1);
                    playView.pause();
                    if(mMagicCameraDisplay!=null){
                        swicthCamera.setImageResource(R.mipmap.record_save_btn);
                        swicthCamera.setVisibility(View.VISIBLE);
                    }
                    mLlFinish.setVisibility(View.VISIBLE);
                }
            });
            playView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    return false;
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
                                preparedFinish=true;
                                LoadingDialog.hide();
                                ivPlayBground.setVisibility(View.GONE);

                            }
                            return true;
                        }
                    });
//                    //获取视频资源的宽度
//                    int videoWidth = mp.getVideoWidth();
//                    //获取视频资源的高度
//                    int videoHeight = mp.getVideoHeight();
//                    playView.setSizeH(videoHeight);
//                    playView.setSizeW(videoWidth);
//                    playView.requestLayout();
                    duration = mp.getDuration();
                }
            });
        }
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
        if (!isScreenOn) {
            playView.pause();
        }
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));
    }

    /**
     * 播放
     */
    private void play() {
        if (playView.isPlaying()) {
            playView.pause();
        } else {
            if (playView.getCurrentPosition() == playView.getDuration()) {
                playView.seekTo(0);
            }
            playView.start();
        }
    }


    public void clickFlash() {
        if (camera == null) {
            return;
        }
        camera.lock();
        Camera.Parameters p = camera.getParameters();
        if (flashType == 0) {
            flashLogic(p, 1, false);
        } else {
            flashLogic(p, 0, false);

        }
        camera.unlock();
    }


    /**
     * 释放摄像头资源
     */
    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.lock();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 闪光灯逻辑
     *
     * @param p    相机参数
     * @param type 打开还是关闭
     * @param isOn 是否启动
     */
    private void flashLogic(Camera.Parameters p, int type, boolean isOn) {
        flashType = type;
        if (type == 0) {
            if (isOn) {
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(p);
            }
            videoFlashLight.setImageResource(R.drawable.flash_off);
        } else {
            if (isOn) {
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(p);
            }
            videoFlashLight.setImageResource(R.drawable.flash_on);
        }
        if (cameraFlag == 0) {
            videoFlashLight.setVisibility(View.GONE);
        } else {
//            videoFlashLight.setVisibility(View.VISIBLE);
            videoFlashLight.setVisibility(View.GONE);

        }
    }

    /**
     * 切换摄像头
     */
    public void switchCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数0或者1;

        try {
            for (int i = 0; i < cameraCount; i++) {
                Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
                if (cameraFlag == 1) {
                    //后置到前置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        frontCameraRotate();//前置旋转摄像头度数
                        switchCameraLogic(i, 0, frontRotate);
                        break;
                    }
                } else {
                    //前置到后置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        switchCameraLogic(i, 1, 90);
                        break;
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /***
     * 处理摄像头切换逻辑
     *
     * @param i           哪一个，前置还是后置
     * @param flag        切换后的标志
     * @param orientation 旋转的角度
     */
    private void switchCameraLogic(int i, int flag, int orientation) {
        if (camera != null) {
            camera.lock();
        }
        endRecordUI();
        releaseCamera();
        camera = Camera.open(i);//打开当前选中的摄像头
        try {
            camera.setDisplayOrientation(orientation);
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        cameraFlag = flag;
        flashLogic(camera.getParameters(), 0, false);
        camera.startPreview();
        cameraType = i;
        camera.unlock();
    }

    /**
     * 旋转前置摄像头为正的
     */
    private void frontCameraRotate() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(1, info);
        int degrees = getDisplayRotation(this);
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        frontOri = info.orientation;
        frontRotate = result;
    }

    /**
     * 获取旋转角度
     */
    private int getDisplayRotation(Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }

    /**
     * 获取和屏幕比例最相近的，质量最高的显示
     */
    private Point getBestCameraShow(Camera.Parameters parameters, Point screenResolution) {
        float tmpSize;
        float minDiffSize = 100f;
        float scale = (float) screenResolution.x / (float) screenResolution.y;
        Camera.Size best = null;
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        for (Camera.Size s : supportedPreviewSizes) {
            tmpSize = Math.abs(((float) s.height / (float) s.width) - scale);
            if (tmpSize < minDiffSize) {
                minDiffSize = tmpSize;
                best = s;
            }
        }
        return new Point(best.width, best.height);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        finish();
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onCloseCamera();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//             EventBus.getDefault().post(new CloseLocaleVedioEvent(false));
//            }
//        },3000);

    }
    /**
     * 关闭摄像头，同时释放美颜
     */
    private void onCloseCamera() {
        CameraInterface.getInstance().onDestroy();
        if (mMagicCameraDisplay != null)
            mMagicCameraDisplay.onDestroy();
    }
}
