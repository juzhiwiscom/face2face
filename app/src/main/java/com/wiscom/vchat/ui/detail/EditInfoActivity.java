package com.wiscom.vchat.ui.detail;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.ui.detail.contract.EditInfoContract;
import com.wiscom.vchat.ui.detail.presenter.EditInfoPresenter;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;

import java.io.File;

import butterknife.BindView;

/**
 * 编辑个人信息页面
 * Created by zhangdroid on 2017/5/27.
 */
public class EditInfoActivity extends BaseTopBarActivity implements View.OnClickListener, EditInfoContract.IView {
    @BindView(R.id.edit_activity_rl_root_layout)
    CardView mLlRoot;//根布局
    @BindView(R.id.edit_activity_fl_upload_photo)
    FrameLayout fl_upload_photo;//上传头像
    @BindView(R.id.edit_activity_iv_avatar)
    ImageView iv_avatar;//头像
    @BindView(R.id.edit_activity_rl_album)
    RelativeLayout rl_album;//相册
    @BindView(R.id.edit_activity_tv_nickname)
    TextView tv_nickname;//昵称
    @BindView(R.id.edit_activity_rl_age)
    RelativeLayout rl_age;//年龄布局
    @BindView(R.id.edit_activity_tv_age)
    TextView tv_age;//年龄
    @BindView(R.id.edit_activity_rl_area)
    RelativeLayout rl_area;//地区布局
    @BindView(R.id.edit_activity_tv_area)
    TextView tv_area;//地区
    @BindView(R.id.edit_activity_rl_language)
    RelativeLayout rl_language;//语言布局
    @BindView(R.id.edit_activity_tv_language)
    TextView tv_language;//语言
    @BindView(R.id.edit_activity_et_introduce)
    EditText et_introduce;
    private EditInfoPresenter mEditInfoPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_edit);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mEditInfoPresenter = new EditInfoPresenter(this);
        setRightText(getString(R.string.person_save), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditInfoPresenter.upLoadMyInfo();
            }
        });
    }

    @Override
    protected void setListeners() {
        fl_upload_photo.setOnClickListener(this);
        rl_album.setOnClickListener(this);
        rl_age.setOnClickListener(this);
        rl_area.setOnClickListener(this);
        rl_language.setOnClickListener(this);
        et_introduce.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mEditInfoPresenter.setLocalInfo();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_activity_fl_upload_photo:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        mEditInfoPresenter.upLoadAvator(file, true);
                    }
                });
                break;
            case R.id.edit_activity_rl_album:// 跳转到相册页面
                LaunchHelper.getInstance().launch(mContext, AlbumActivity.class);
                break;
            case R.id.edit_activity_rl_age:
                mEditInfoPresenter.selectAge();
                break;
            case R.id.edit_activity_rl_area:
                //选择国家和地区
                mEditInfoPresenter.selectCountry();
                break;
            case R.id.edit_activity_rl_language:
                //选择语言
                mEditInfoPresenter.selectLanguage();
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public String getName() {
        return tv_nickname.getText().toString();
    }

    @Override
    public String getAge() {
        return tv_age.getText().toString();
    }

    @Override
    public String getCountry() {
        return tv_area.getText().toString();
    }

    @Override
    public String getLanguage() {
        return tv_language.getText().toString();
    }

    @Override
    public String getIntroducation() {
        return et_introduce.getText().toString();
    }

    @Override
    public void setUserAvator(String photoUrl) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(photoUrl)
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
    }

    @Override
    public void setName(String name) {
        if (!TextUtils.isEmpty(name)) {
            tv_nickname.setText(name);
        }
    }

    @Override
    public void setAge(String age) {
        if (!TextUtils.isEmpty(age)) {
            tv_age.setText(age);
        }
    }

    @Override
    public void setCountry(String country) {
        if (!TextUtils.isEmpty(country)) {
            tv_area.setText(country);
        }
    }

    @Override
    public void setLanguage(String language) {
        if (!TextUtils.isEmpty(language)) {
            tv_language.setText(language);
        }
    }

    @Override
    public void setIntroducation(String introducation) {
        if (!TextUtils.isEmpty(introducation)) {
            et_introduce.setText(introducation);
        }
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void finishActivity() {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEditInfoPresenter.finish();
    }

}
