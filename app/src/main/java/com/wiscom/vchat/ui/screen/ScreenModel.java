package com.wiscom.vchat.ui.screen;

/**
 * Created by tianzhentao on 2018/1/3.
 */

class ScreenModel {
    private String name;
    private String time;
    private String avatar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
