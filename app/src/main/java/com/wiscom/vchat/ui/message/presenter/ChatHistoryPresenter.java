package com.wiscom.vchat.ui.message.presenter;

import android.content.Context;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.model.HuanXinUser;
import com.wiscom.vchat.db.DbModle;
import com.wiscom.vchat.ui.message.adapter.ChatHistoryAdapter;
import com.wiscom.vchat.ui.message.contract.ChatHistoryContract;
import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryPresenter implements ChatHistoryContract.IPresenter {
    private ChatHistoryContract.IView mChatHistoryView;
    private Context mContext;
    private ChatHistoryAdapter mChatHistoryAdapter;
    List<HuanXinUser> allAcount=new ArrayList<>();
    public ChatHistoryPresenter(ChatHistoryContract.IView view) {
        this.mChatHistoryView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mChatHistoryView = null;
        mContext = null;
        mChatHistoryAdapter=null;
    }

    @Override
    public void loadConversationList() {
        // 查询未读消息数
        allAcount=DbModle.getInstance().getUserAccountDao().getAllAcount();
        mChatHistoryAdapter = new ChatHistoryAdapter(mContext, R.layout.item_chat_history);
        mChatHistoryView.setAdapter(mChatHistoryAdapter, R.layout.common_load_more);
        if (allAcount!=null&&allAcount.size()>0) {
            mChatHistoryAdapter.bind(allAcount);
            mChatHistoryView.hideRefresh(1);
        }
        mChatHistoryAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
//                EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
//                    if(allAcount!=null&&allAcount.size()>0){
//                        HuanXinUser huanXinUser = allAcount.get(position);
//                        if (huanXinUser!=null) {
//                            DbModle.getInstance().getUserAccountDao().setState(huanXinUser,true);//把标记设置为已经读取
//                            String hxId = huanXinUser.getHxId();
//                            if(!TextUtils.isEmpty(hxId)){
//                                long guid = Long.parseLong(hxId);
//                                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
//                                        huanXinUser.getAccount(), huanXinUser.getHxName(), huanXinUser.getHxIcon()));
//                            }
//                        }
//                    }
            }
        });
    }

    @Override
    public void refresh() {

    }

    @Override
    public void loadMore() {
        mChatHistoryView.hideRefresh(1);
    }

}
