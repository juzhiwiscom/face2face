package com.wiscom.vchat.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.ui.message.adapter.ChatHistoryAdapter;
import com.wiscom.vchat.ui.message.contract.ChatHistoryContract;
import com.wiscom.vchat.ui.message.presenter.ChatHistoryPresenter;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.widget.AutoSwipeRefreshLayout;
import com.wiscom.library.widget.XRecyclerView;

import butterknife.BindView;

/**
 * 聊天消息记录列表
 * Created by zhangdroid on 2017/6/10.
 */
public class ChatHistoryFragment extends BaseFragment implements ChatHistoryContract.IView {
    @BindView(R.id.chat_history_root)
    FrameLayout mFlRoot;
    @BindView(R.id.chat_history_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.chat_history_list)
    XRecyclerView mRecyclerView;

    private ChatHistoryPresenter mChatHistoryPresenter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_chat_history;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        // 添加小秘书
//        mRecyclerView.addHeaderView(R.layout.chat_admin);
        mChatHistoryPresenter = new ChatHistoryPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新和上拉加载更多
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mChatHistoryPresenter.refresh();
                }
            }
        });
        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mChatHistoryPresenter.loadMore();
            }
        });
    }

    @Override
    protected void loadData() {
//        mChatHistoryPresenter.loadConversationList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChatHistoryPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mFlRoot, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mChatHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Override
    public ChatHistoryAdapter getChatHistoryAdapter() {
        return null;
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRecyclerView.setAdapter(adapter,loadMoreViewId);
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatHistoryPresenter.loadConversationList();
    }
}
