package com.wiscom.vchat.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

import java.io.File;

/**
 * Created by Administrator on 2017/6/9.
 */
public interface EditInfoContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        String getName();

        String getAge();

        String getCountry();

        String getLanguage();

        String getIntroducation();

        void setUserAvator(String photoUrl);

        void setName(String name);

        void setAge(String age);

        void setCountry(String country);

        void setLanguage(String language);

        void setIntroducation(String introducation);

        FragmentManager obtainFragmentManager();

        void finishActivity();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 设置用户信息
         */
        void setLocalInfo();

        /**
         * 上传用户信息
         */
        void upLoadMyInfo();

        /**
         * 上传用户的头像
         */
        void upLoadAvator(File file, boolean photoType);

        void selectCountry();

        void selectLanguage();

        void selectAge();

    }

}
