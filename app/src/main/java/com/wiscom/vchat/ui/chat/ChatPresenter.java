package com.wiscom.vchat.ui.chat;

import android.content.Context;
import android.os.Environment;
import android.os.Message;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.MotionEvent;

import com.hyphenate.chat.EMMessage;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.RecordUtil;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.library.util.FileUtil;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.util.Utils;

import java.io.File;
import java.util.List;

/**
 * Created by zhangdroid on 2017/6/27.
 */
public class ChatPresenter implements ChatContract.IPresenter {
    private static final String TAG = ChatPresenter.class.getSimpleName();
    private ChatContract.IView mChatView;
    private Context mContext;
    // 接收方用户account
    private String mAccount;
    // 接收方用户guid
    private long mGuid;
    // 录音文件存放目录
    private String mRecordDirectory;
    // 录音文件路径
    private String mRecordOutputPath;
    // 是否正在录音
    private boolean mIsRecording;
    // 录音是否被取消
    private boolean mIsRecordCanceled;
    // 录音时长（毫秒）
    private long mRecordDuration;
    // 录音计时刷新间隔（毫秒）
    private final long mRefreshInterval = 250;
    // 每页消息数
    private final int mPageSize = 20;

    public ChatPresenter(ChatContract.IView view, String account, long guid) {
        this.mChatView = view;
        this.mContext = view.obtainContext();
        this.mAccount = account;
        this.mGuid = guid;
    }

    @Override
    public void start() {
        // 录音文件临时存放目录，内部存储，不需要权限
        mRecordDirectory = FileUtil.getExternalFilesDir(mContext, Environment.DIRECTORY_MUSIC) + File.separator;
    }

    @Override
    public void finish() {
        mChatView = null;
        mContext = null;
        // 释放录音和播放器资源
        RecordUtil.getInstance().releaseRecord();
        if (RecordUtil.getInstance().isPlaying()) {// 如果正在播放语音，需要停止
            RecordUtil.getInstance().stop();
        }
        RecordUtil.getInstance().release();
    }

    @Override
    public void handleAsyncTask(Message message) {
        switch (message.what) {
            case C.message.MSG_TYPE_TIMER:// 计时
                if (mIsRecording) {
                    mRecordDuration += mRefreshInterval;
                    // 录音超过1分钟自动发送
                    if (mRecordDuration > 60_000) {
                        stopRecordAndSend();
                    } else {
                        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
                    }
                }
                break;
            case C.message.MSG_TYPE_DELAYED:// 延时结束录音
                RecordUtil.getInstance().stopRecord();
                break;

            case C.message.MSG_TYPE_INIT:// 初始化聊天记录
                List<EMMessage> list = (List<EMMessage>) message.obj;
                if (Utils.isListEmpty(list)) {
                    mChatView.toggleShowEmpty(true, null);
                } else {
                    LogUtil.i(TAG, "load()# message list个数="+list.size()+"===" + list.toString());
//                    tian
//                        Long lastMsgTime = list.get(list.size()-1).getMsgTime();//获取最后一条消息时间
//                        String msgId = list.get(list.size() - 1).getMsgId();//获取最后一条消息的id
////                        从数据库中取出最后一条数据
//                    HuanXinUser accountByHyID = DbModle.getInstance().getUserAccountDao().getAccountByHyID(msgId);
//                    if(accountByHyID.getLastMsgTime()!=null){
//                        Long LastTimeByDB = Long.valueOf(accountByHyID.getLastMsgTime());
//                        if(lastMsgTime<LastTimeByDB){
//                            ...
//                        }
//
//                    }

                    mChatView.getChatAdapter().bind(list);
                }
                mChatView.hideRefresh();
                mChatView.scroollToBottom();
                break;
            case C.message.MSG_TYPE_LOAD_MORE:// 加载更多聊天记录
                List<EMMessage> emMessagesMore = (List<EMMessage>) message.obj;
                if (!Utils.isListEmpty(emMessagesMore)) {
                    LogUtil.i(TAG, "loadMore()# message list = " + emMessagesMore.toString());
                    for (int i = 0; i < emMessagesMore.size(); i++) {
                        Long lastMsgTime = emMessagesMore.get(i).getMsgTime();//获取消息时间
                        Long showLastMsgTime = Long.valueOf(SharedPreferenceUtil.getStringValue(mContext, "MESSAGE_ID",emMessagesMore.get(i).getMsgId(), "0"));//展示的最后一条消息的时间
                        if(lastMsgTime>showLastMsgTime){//如果得到的时间小于或等于最后展示的时间，说明数据重复加载了，
                            mChatView.getChatAdapter().appendToList(emMessagesMore);
                        }
                    }
                }
                mChatView.hideRefresh();
                break;
            case C.message.MSG_TYPE_SEND_TXT:// 发送文字消息
                if (!TextUtils.isEmpty(mAccount)) {
                    HyphenateHelper.getInstance().sendTextMessage(mAccount, mChatView.getInputText(),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                    Message msg = Message.obtain();
                                    msg.what = C.message.MSG_TYPE_UPDATE;
                                    msg.obj = emMessage;
                                    mChatView.sendMessage(msg);
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                mChatView.clearInput();
                break;
            case C.message.MSG_TYPE_SEND_VOICE:
                if (!TextUtils.isEmpty(mAccount)) {
                    HyphenateHelper.getInstance().sendVoiceMessage(mAccount, mRecordOutputPath, (int) (mRecordDuration / 1000),
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                    Message msg = Message.obtain();
                                    msg.what = C.message.MSG_TYPE_UPDATE;
                                    msg.obj = emMessage;
                                    mChatView.sendMessage(msg);
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_SEND_IMAGE:
                if (!TextUtils.isEmpty(mAccount)) {
                    HyphenateHelper.getInstance().sendImageMessage(mAccount, (String) message.obj,
                            new HyphenateHelper.OnMessageSendListener() {
                                @Override
                                public void onSuccess(EMMessage emMessage) {
                                    Message msg = Message.obtain();
                                    msg.what = C.message.MSG_TYPE_UPDATE;
                                    msg.obj = emMessage;
                                    mChatView.sendMessage(msg);
                                }

                                @Override
                                public void onError() {
                                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                                }
                            });
                } else {
                    mChatView.showTip(mContext.getString(R.string.chat_send_failed));
                }
                break;
            case C.message.MSG_TYPE_UPDATE:// 更新消息
                ChatAdapter chatAdapter = mChatView.getChatAdapter();
                if (chatAdapter.getItemCount() == 0) {
                    mChatView.toggleShowEmpty(false, null);
                }
                EMMessage emMessage = (EMMessage) message.obj;
                if (null != chatAdapter && null != emMessage) {
                    chatAdapter.insertItem(chatAdapter.getItemCount(), emMessage);
                }
                mChatView.scroollToBottom();
                break;
        }
    }

    @Override
    public void initChatConversation() {
        LogUtil.i(TAG, "initChatConversation()");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_INIT;
                message.obj = HyphenateHelper.getInstance().initConversation(mAccount, mPageSize);
                mChatView.sendMessage(message);
            }
        }).start();
    }

    @Override
    public void refresh() {
        // 加载当前消息列表之前的消息
        LogUtil.i(TAG, "refresh()");
        // 当前显示聊天记录列表条数
        final int listCount = mChatView.getChatAdapter().getItemCount();
        // 全部聊天记录条数
        final int allCount = HyphenateHelper.getInstance().getAllMsgCount(mAccount);
        LogUtil.i(TAG, "refresh()-当前显示的条数="+listCount+"--全部聊天记录条数="+allCount);
        if (listCount < allCount) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int pageSize = mPageSize;
                    if (allCount - listCount < 20) {
                        pageSize = allCount - listCount;
                    }
                    Message message = Message.obtain();
                    message.what = C.message.MSG_TYPE_LOAD_MORE;
                    message.obj = HyphenateHelper.getInstance(). loadMoreMessages(mAccount, pageSize);
                    mChatView.sendMessage(message);
                }
            }).start();
        } else {
            mChatView.hideRefresh();
        }
    }

    @Override
    public void sendTextMessage() {
//        ApiManager.interruptText(mGuid, mChatView.getInputText(), new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_TXT);
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//               // mChatView.showInterruptDialog(mContext.getString(R.string.message_interrupt));
//                mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_TXT);
//            }
//        });
    }

    @Override
    public void sendImageMessage(final boolean isTakePhoto) {

        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                String path = file.toString();
                Message message = Message.obtain();
                message.what = C.message.MSG_TYPE_SEND_IMAGE;
                message.obj = path;
                mChatView.sendMessage(message);
            }
        });
//        GetPhotoActivity.toGetPhotoActivity(mContext, isTakePhoto, new GetPhotoActivity.OnGetPhotoPathListener() {
//            @Override
//            public void getSelectedPhotoPath(final String path) {
//                if (!TextUtils.isEmpty(path)) {
//                    ApiManager.interruptImage(mGuid, new File(path), new IGetDataListener<BaseModel>() {
//                        @Override
//                        public void onResult(BaseModel baseModel, boolean isEmpty) {
//                            Message message = Message.obtain();
//                            message.what = C.message.MSG_TYPE_SEND_IMAGE;
//                            message.obj = path;
//                            mChatView.sendMessage(message);
//                        }
//
//                        @Override
//                        public void onError(String msg, boolean isNetworkError) {
//                            mChatView.showInterruptDialog(mContext.getString(R.string.message_interrupt));
//                        }
//                    });
//                } else {
//                    mChatView.showTip(mContext.getString(R.string.chat_image_empty));
//                }
//            }
//        });
    }

    @Override
    public void handleTouchEventDown() {
        mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        startRecord();
        mIsRecordCanceled = false;
    }

    @Override
    public void handleTouchEventMove(MotionEvent event) {
        if (event.getY() < -100) { // 上滑取消发送
            mIsRecordCanceled = true;
            mChatView.showRecordPopupWindow(C.message.STATE_CANCELED);
        } else {
            mIsRecordCanceled = false;
            mChatView.showRecordPopupWindow(C.message.STATE_RECORDING);
        }
    }

    @Override
    public void handleTouchEventUp() {
        mChatView.showRecordPopupWindow(C.message.STATE_IDLE);
        mChatView.dismissPopupWindow();
        if (mIsRecordCanceled) {
            cancelRecord();
        } else {
            stopRecordAndSend();
        }
    }

    private void startRecord() {
        // 录音开始前震动一下
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(88);
        // 根据系统时间生成文件名
        String recordFileName = FileUtil.createFileNameByTime() + ".mp3";
        // 录音文件临时保存路径：data下包名music目录
        mRecordOutputPath = mRecordDirectory + recordFileName;
        // 开始录音
        RecordUtil.getInstance().startRecord(mRecordOutputPath);
        mIsRecording = true;
        mRecordDuration = 0;
        // 计时，间隔250毫秒刷新
        mChatView.sendMessage(mRefreshInterval, C.message.MSG_TYPE_TIMER);
    }

    private void stopRecordAndSend() {
        if (mIsRecording) {
            mIsRecording = false;
            if (mRecordDuration < 1000) {// 录音时长小于1秒的不发送
                // 删除小于1秒的文件
                FileUtil.deleteFile(mRecordOutputPath);
                // 延时500毫秒调用MediaRecorder#stop()，防止出现start()之后立马调用stop()的异常
                mChatView.sendMessage(500, C.message.MSG_TYPE_DELAYED);
            } else {// 发送语音
                RecordUtil.getInstance().stopRecord();
                sendVoice();
            }
        }
    }

    private void cancelRecord() {
        if (mIsRecordCanceled) {
            RecordUtil.getInstance().stopRecord();
            mIsRecordCanceled = false;
            FileUtil.deleteFile(mRecordOutputPath);
        }
    }

    private void sendVoice() {
//        ApiManager.interruptVoice(mGuid, new File(mRecordOutputPath), new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mChatView.sendMessage(0, C.message.MSG_TYPE_SEND_VOICE);
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//                // 删除已经录制的音频文件
//                FileUtil.deleteFile(mRecordOutputPath);
//                mChatView.showInterruptDialog(mContext.getString(R.string.message_interrupt));
//            }
//        });
    }

}
