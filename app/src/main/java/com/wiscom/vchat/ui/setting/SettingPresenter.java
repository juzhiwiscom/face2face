package com.wiscom.vchat.ui.setting;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.ui.register.RegisterActivity;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.FileUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public class SettingPresenter implements SettingContract.IPresenter {
    private SettingContract.IView mSettingView;
    private Context mContext;

    public SettingPresenter(SettingContract.IView view) {
        this.mSettingView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mSettingView = null;
        mContext = null;
    }

    @Override
    public void getNoDistrubState() {
        mSettingView.toggleNoDistrub("3".equals(UserPreference.getStatus()));
    }

    @Override
    public void toggleNoDistrub(final boolean toggle) {
        if ((toggle && "3".equals(UserPreference.getStatus())) || (!toggle && !"3".equals(UserPreference.getStatus()))) {
            return;
        }
        ApiManager.modifyUserStatus(toggle ? "3" : "1", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                UserPreference.setStatus(toggle ? "3" : "1");
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mSettingView.toggleNoDistrub(!toggle);
            }
        });
    }

    @Override
    public void getCacheSize() {
        mSettingView.setCacheSize(FileUtil.getTotalCacheSize(mContext));
    }

    @Override
    public void clearCache() {
        mSettingView.showAlertDialog(mContext.getString(R.string.setting_clear_cache_tip), new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {
            }

            @Override
            public void onPositiveClick(View view) {
                // 清除系统缓存
                FileUtil.clearAllCache(mContext);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // 清除Glide图片磁盘缓存
                        ImageLoaderUtil.getInstance().clearDiskCache(mContext);
                    }
                }).start();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        // 清除Glide图片内存缓存
                        ImageLoaderUtil.getInstance().clearMemoryCache(mContext);
                        mSettingView.setCacheSize("0.0B");
                        mSettingView.showTip(mContext.getString(R.string.setting_clear_cache_finish));
                    }
                });
            }
        });
    }

    @Override
    public void getVersionName() {
        mSettingView.setVersion(DeviceUtil.getVersionName(mContext));
    }

    @Override
    public void logout() {
        mSettingView.showAlertDialog(mContext.getString(R.string.setting_logout_tip), new OnDialogClickListener() {
            @Override
            public void onNegativeClick(View view) {
            }

            @Override
            public void onPositiveClick(View view) {
                AgoraHelper.getInstance().logout();
                HyphenateHelper.getInstance().logout();
                EventBus.getDefault().post(new FinishEvent());
                UserPreference.setRegisterCountry("111");
                UserPreference.setRegisterName("");
                UserPreference.setState(null);
                SharedPreferenceUtil.setIntValue(mContext,"Read_Msg_Num","All_Read_Msg_Num",0);//                    将未读消息清空
                LaunchHelper.getInstance().launchFinish(mContext, RegisterActivity.class);
            }
        });
    }

    public void changePassword() {
        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_change_password, null);
        bottomDialog.setContentView(contentView);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = Util.dp2px(mContext,270);
        params.height =Util.dp2px(mContext,185);
        params.topMargin=Util.dp2px(mContext,200);
        contentView.setLayoutParams(params);
//        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
        final String password = UserPreference.getPassword();
        final TextView oldPassword = (TextView) contentView.findViewById(R.id.et_old_password);
        final EditText newPassword = (EditText) contentView.findViewById(R.id.et_new_password);
        TextView cancel = (TextView) contentView.findViewById(R.id.password_cancel);
        TextView sure = (TextView) contentView.findViewById(R.id.password_sure);
        oldPassword.setText(password);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
            }
        });
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldWord = oldPassword.getText().toString();
                String newWord = newPassword.getText().toString();
                if(newWord.length()<6 || newWord.length()>12){
                    ToastUtil.showShortToast(mContext,mContext.getString(R.string.new_password_error));
                    return;
                }
                setPassword(password,newWord);

                bottomDialog.dismiss();
            }
        });


    }

    private void setPassword(String oldPassWord, final String newWord) {
        ApiManager.changePassword(oldPassWord, newWord,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {
                UserPreference.setPassword(newWord);
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });    }
}
