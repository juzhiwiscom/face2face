package com.wiscom.vchat.ui.message;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarFragment;
import com.wiscom.vchat.common.RefreshRecyclerView;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.VideoHelper;
import com.wiscom.vchat.common.recyclerview.CommonRecyclerViewAdapter;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.VideoRecord;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.event.UpdateVedioListEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.message.adapter.VideoHistoryAdapter;
import com.wiscom.vchat.ui.message.contract.VideoHistoryContract;
import com.wiscom.vchat.ui.message.presenter.VideoHistoryPresenter;
import com.wiscom.vchat.ui.pay.PayActivity;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.util.LaunchHelper;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 视频聊天记录列表
 * Created by zhangdroid on 2017/6/10.
 */
public class VideoHistoryFragment extends BaseTopBarFragment implements VideoHistoryContract.IView {
    @BindView(R.id.message_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
//    @BindView(R.id.video_history_root)
//    FrameLayout mFlRoot;
//    @BindView(R.id.video_history_refresh)
//    AutoSwipeRefreshLayout mSwipeRefreshLayout;
//    @BindView(R.id.video_history_list)
//    XRecyclerView mRecyclerView;

    private VideoHistoryPresenter mVideoHistoryPresenter;
    private VideoHistoryAdapter mVideoHistoryAdapter;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_history;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.record);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }
    @Override
    protected void initViews() {
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
//        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//        mRecyclerView.setLayoutManager(linearLayoutManager);
//        mRecyclerView.setHasFixedSize(true);
//        mVideoHistoryAdapter = new VideoHistoryAdapter(mContext, R.layout.item_video_history);
//        mRecyclerView.setAdapter(mVideoHistoryAdapter, R.layout.common_load_more);

        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Util.getLoadingView(getActivity()));
       mVideoHistoryAdapter = new VideoHistoryAdapter(mContext, R.layout.item_video_history);
        mRefreshRecyclerView.setAdapter(mVideoHistoryAdapter);

        mVideoHistoryPresenter = new VideoHistoryPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mVideoHistoryPresenter.refresh();
                }            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                mVideoHistoryPresenter.loadMore();


            }
        });
//        // 下拉刷新和上拉加载更多
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (mIsFirstLoad) {
//                    mIsFirstLoad = false;
//                } else {
//                    mVideoHistoryPresenter.refresh();
//                }
//            }
//        });
//        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                mVideoHistoryPresenter.loadMore();
//            }
//        });
        mVideoHistoryAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position, com.wiscom.vchat.common.recyclerview.RecyclerViewHolder viewHolder) {
                final VideoRecord videoRecord = mVideoHistoryAdapter.getItemByPosition(position);
//                判断是否是好友
                isFirend(videoRecord);


            }
        });
    }

    private void isFirend(final VideoRecord videoRecord) {
        ApiManager.getFriendFromId(videoRecord.getUserBase().getAccount()+"", new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            int isFriend = userDetail.getIsFriend();
                            if (isFriend == 1) {
//                               说明是朋友。不拦截
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount(),
                                        userBase.getNickName(), userBase.getIconUrlMininum()), getChildFragmentManager());
                            } else {
//                                说明不是朋友，拦截
                                setInter(videoRecord);

                            }
                        }
                    }
                } else {
//                    toggleShowEmpty(false,getString(R.string.friend_noting),null);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });    }

    private void setInter(final VideoRecord videoRecord) {
        AlertDialog.show(getFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
                mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                    }

                    @Override
                    public void onPositiveClick(View view) {
//                                判断钻石是否足够，不够的话跳转支付界面
                        int beanCount = BeanPreference.getBeanCount();
                        if(beanCount!=0 && beanCount>50){
//                                    钻石足够
                            if (null != videoRecord) {
                                UserBase userBase = videoRecord.getUserBase();
                                if (null != userBase) {
                                    beanCount=beanCount-50;
                                    BeanPreference.setBeanCount(beanCount);
                                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount(),
                                            userBase.getNickName(), userBase.getIconUrlMininum()), getChildFragmentManager());
                                }
                            }
                        }else{
//                                    弹出钻石不足需要充值的按钮
                            AlertDialog.show(getFragmentManager(), mContext.getString(R.string.lack_diamon), mContext.getString(R.string.interupt_null),
                                    mContext.getString(R.string.go_buy), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                        @Override
                                        public void onNegativeClick(View view) {
                                        }

                                        @Override
                                        public void onPositiveClick(View view) {
//                                                    跳转钻石购买界面
                                            LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                        }
                                    });
                        }

                    }
                });
    }

    @Override
    protected void loadData() {
        mVideoHistoryPresenter.loadHistoryList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mVideoHistoryPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
//        SnackBarUtil.showShort(mFlRoot, msg);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mVideoHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mVideoHistoryPresenter.refresh();
            }
        });
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(delaySeconds);
            mRefreshRecyclerView.loadMoreCompleted(delaySeconds);
        }
//        mSwipeRefreshLayout.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
//                    mSwipeRefreshLayout.setRefreshing(false);
//                }
//            }
//        }, delaySeconds * 1000);
    }

    @Override
    public void showLoadMore() {
//        mRecyclerView.setVisibility(R.id.load_more_progress, true);
//        mRecyclerView.setVisibility(R.id.load_more_msg, true);
//        mRecyclerView.setVisibility(R.id.load_more_empty, false);
//        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
//        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
//        mRecyclerView.setVisibility(R.id.load_more_empty, true);
//        mRecyclerView.setVisibility(R.id.load_more_progress, false);
//        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }
    @Override
    public void setOnLoadMoreListener(RefreshRecyclerView.OnLoadingMoreListener onLoadMoreListener) {
        mRefreshRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public VideoHistoryAdapter getVideoHistoryAdapter() {
        return mVideoHistoryAdapter;
    }
    @Subscribe
    public void onEvent(UpdateVedioListEvent event) {
        mVideoHistoryPresenter.refresh();
    }

}
