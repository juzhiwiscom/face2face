package com.wiscom.vchat.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.UserDetailforOther;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public interface UserDetailContract {
    interface IView extends BaseView {
        String getUserId();

        void getData(UserDetailforOther data);

        void showLoading();

        void dismissLoading();

        void showNetworkError();
        void followSucceed();
        //设置ViewPager
       void setViewPagerData(List<UserPhoto> userPhotos);
        //设置文字显示的数据
        void setInforData(UserBase userBase);
        //获取是否关注的判断字段
        void getFollowOrUnFollow(UserDetail userDetail);
        //获取现在的状态的回调
        void getStatus(int status);

        /**
         * @return 获得FragmentManager，用于显示对话框
         */
        FragmentManager obtainFragmentManager();
        /**
         * 判断是否是主播的方法
         */
        void isAnchor();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取用户信息
         */
        void getUserInfoData();
        /**
         * 关注
         */
        void follow(String remoteUid);
        /**
         * 取消关注
         */
        void unFollow(String remoteUid);
        /**
         * 跳转视频聊天界面
         */
        void goVideoCallPage();

        /**
         * 跳转写信界面
         */
        void goWriteMessagePage();
    }
}
