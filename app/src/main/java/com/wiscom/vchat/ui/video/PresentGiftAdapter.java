package com.wiscom.vchat.ui.video;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiscom.vchat.R;

/**
 * Created by tianzhentao on 2018/2/26.
 */

class PresentGiftAdapter extends RecyclerView.Adapter<PresentGiftAdapter.ViewHolder> {
    public PresentGiftAdapter(Context mContext) {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // 实例化展示的view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_present_gift, parent, false);
        // 实例化viewholder
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTv;

        public ViewHolder(View itemView) {
            super(itemView);
//            mTv = (TextView) itemView.findViewById(R.id.item_tv);
        }
    }
}
