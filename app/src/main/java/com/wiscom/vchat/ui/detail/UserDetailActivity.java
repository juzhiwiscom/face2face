package com.wiscom.vchat.ui.detail;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.UserDetailforOther;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.parcelable.BigPhotoParcelable;
import com.wiscom.vchat.parcelable.UserDetailParcelable;
import com.wiscom.vchat.ui.detail.adapter.CommonPhotoAdapter;
import com.wiscom.vchat.ui.detail.contract.UserDetailContract;
import com.wiscom.vchat.ui.detail.presenter.UserDetailPresenter;
import com.wiscom.vchat.ui.photo.BigPhotoActivity;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.util.Utils;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 对面空间页面
 * Created by zhangdroid on 2017/5/27.
 */
public class UserDetailActivity extends BaseFragmentActivity implements View.OnClickListener, UserDetailContract.IView {
    @BindView(R.id.user_detail_root_layout)
    RelativeLayout mLlRoot;
    @BindView(R.id.user_detail_iv_back)
    ImageView iv_back;
    @BindView(R.id.user_detail_iv_set)
    ImageView iv_set;
    @BindView(R.id.user_detail_tv_name)
    TextView tv_name;
    @BindView(R.id.user_detail_tv_price)
    TextView tv_price;
    @BindView(R.id.user_detail_tv_language)
    TextView tv_language;
    @BindView(R.id.user_detail_tv_fllow)
    TextView tv_fllow;
    @BindView(R.id.user_detail_iv_fllow)
    ImageView iv_follow;
    @BindView(R.id.user_detail_rl_fllow)
    RelativeLayout rl_fllow;
    @BindView(R.id.user_detail_iv_sex)
    ImageView iv_sex;
    @BindView(R.id.user_detail_tv_age)
    TextView tv_age;
    @BindView(R.id.user_detail_tv_area)
    TextView tv_area;
    @BindView(R.id.user_detail_tv_id)
    TextView tv_id;
    @BindView(R.id.user_detail_vp)
    UltraViewPager mViewPager;
    @BindView(R.id.user_detail_iv_occupied)
    ImageView iv_occupied;
    @BindView(R.id.user_detail_tv_soliloquy)
    TextView tv_soliloquy;
    @BindView(R.id.user_detail_ll_video_talk)
    LinearLayout ll_video_talk;//发起来视频聊天的按钮
    @BindView(R.id.user_detail_ll_send_message)
    LinearLayout ll_send_message;//发信息的按钮
    @BindView(R.id.user_detail_iv_video_status)
    ImageView iv_status;
    @BindView(R.id.user_detail_video_and_message)
    LinearLayout linear_video_and_message;
    private UserDetailPresenter userDetailPresenter;
    private UserDetailParcelable remoteUidParcelable;
    private boolean isFollow = true;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_user_detail;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        remoteUidParcelable = (UserDetailParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        userDetailPresenter = new UserDetailPresenter(this);
    }

    @Override
    protected void setListeners() {
        iv_back.setOnClickListener(this);
        iv_set.setOnClickListener(this);
        rl_fllow.setOnClickListener(this);
        ll_video_talk.setOnClickListener(this);
        ll_send_message.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        userDetailPresenter.getUserInfoData();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_detail_iv_back:
                finish();
                break;
            case R.id.user_detail_iv_set:
                //拉黑 举报
                break;
            case R.id.user_detail_rl_fllow:
                if (isFollow) {
                    //关注
                    userDetailPresenter.follow(remoteUidParcelable.remoteUid);
                    setUnFollow();
                } else if (!isFollow) {
                    AlertDialog.show(getSupportFragmentManager(), getString(R.string.alert), "确定不再关注此人？",
                            getString(R.string.positive), getString(R.string.negative), new OnDialogClickListener() {
                                @Override
                                public void onNegativeClick(View view) {
                                }

                                @Override
                                public void onPositiveClick(View view) {
                                    userDetailPresenter.unFollow(remoteUidParcelable.remoteUid);
                                    setFollow();
                                }
                            });
                }
                break;
            case R.id.user_detail_ll_video_talk:
                //发起视频聊天
                userDetailPresenter.goVideoCallPage();
                break;
            case R.id.user_detail_ll_send_message:
                //发信息
                userDetailPresenter.goWriteMessagePage();
                break;
        }
    }

    //取消关注
    private void setUnFollow() {
        isFollow = false;
        tv_fllow.setText("已关注");
        iv_follow.setImageResource(R.drawable.unfollow);
    }

    //关注
    private void setFollow() {
        isFollow = true;
        tv_fllow.setText("关注");
        iv_follow.setImageResource(R.drawable.follow);
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getUserId() {
        String remoteUid = null;
        if (remoteUidParcelable != null) {
            remoteUid = remoteUidParcelable.remoteUid;
        }
        if (!TextUtils.isEmpty(remoteUid)) {
            return remoteUid;
        } else {
            return null;
        }
    }

    @Override
    public void getData(UserDetailforOther data) {
        ;
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void followSucceed() {
        //关注成功，或者取消关注
        if (!isFollow) {
            SnackBarUtil.showShort(mLlRoot, "关注成功");
        } else if (isFollow) {
            SnackBarUtil.showShort(mLlRoot, "取消关注成功");
        }
    }

    //设置ViewPager
    @Override
    public void setViewPagerData(final List<UserPhoto> userPhotos) {
        if (!Utils.isListEmpty(userPhotos)) {
            // 添加图片ImageView
            List<ImageView> imageViews = new ArrayList<>();
            for (final UserPhoto item : userPhotos) {
                if (null != item) {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(item.getFileUrlMiddle())
                            .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
                    // 点击图片查看大图
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(userPhotos.indexOf(item),
                                    Util.convertPhotoUrl(userPhotos)));
                        }
                    });
                    imageViews.add(imageView);
                }
            }
            // 设置滑动方向
            mViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
            // 设置切换动画
            mViewPager.setPageTransformer(false, new UltraDepthScaleTransformer());
            mViewPager.setAdapter(new CommonPhotoAdapter(imageViews));
            if (userPhotos.size() > 1) {
                mViewPager.initIndicator();
                mViewPager.getIndicator()
                        .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                        .setGravity(Gravity.CENTER | Gravity.BOTTOM)
                        .setNormalColor(Color.GRAY)
                        .setFocusColor(getResources().getColor(R.color.main_color))
                        .build();
                // 设置自动循环
                mViewPager.setInfiniteLoop(true);
                // 设置自动循环时间间隔
                mViewPager.setAutoScroll(2_000);
            }
            iv_occupied.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
        } else {
            iv_occupied.setImageResource(Util.getDefaultImage());
            iv_occupied.setVisibility(View.VISIBLE);
            mViewPager.setVisibility(View.GONE);
        }
    }

    //设置文字显示的数据
    @Override
    public void setInforData(UserBase userBase) {
        if (userBase != null) {
            tv_name.setText(userBase.getNickName());
            tv_age.setText(userBase.getAge() + "");
            tv_area.setText(userBase.getCountry() + " " + userBase.getCity());
            tv_language.setText(userBase.getLanguage());
            tv_id.setText("ID " + userBase.getGuid());
            tv_soliloquy.setText(userBase.getOwnWords());
        }
    }

    // 获取是否关注的信息
    @Override
    public void getFollowOrUnFollow(UserDetail userDetail) {
        if (userDetail != null) {
            if (userDetail.getIsFollow().equals("1")) {
                //已经关注
                setUnFollow();
            } else if (userDetail.getIsFollow().equals("0")) {
                //没有关注
                setFollow();
            }
        }
    }

    @Override
    public void getStatus(int status) {
        switch (status) {
            case C.homepage.STATE_FREE:// 空闲
                iv_status.setImageResource(R.drawable.ic_video_free);
                break;
            case C.homepage.STATE_BUSY:// 忙线中
                iv_status.setImageResource(R.drawable.ic_video_busy);
                break;
            case C.homepage.STATE_NO_DISTRUB:// 勿扰
                iv_status.setImageResource(R.drawable.ic_video_offline);
                break;
        }
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void isAnchor() {
        linear_video_and_message.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userDetailPresenter.finish();
    }

}
