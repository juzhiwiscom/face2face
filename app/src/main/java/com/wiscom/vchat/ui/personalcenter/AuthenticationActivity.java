package com.wiscom.vchat.ui.personalcenter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.parcelable.PlayViewParcelable;
import com.wiscom.vchat.ui.personalcenter.contract.AuthenticationContract;
import com.wiscom.vchat.ui.personalcenter.presenter.AuthenticationPresenter;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.image.RoundedCornersTransformation;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.widget.PlayView;

import java.io.File;

import butterknife.BindView;

import static com.wiscom.vchat.C.Video.REQUEST_CODE;

/**
 * 认证主播页面
 * Created by zhangdroid on 2017/5/27.
 */
public class AuthenticationActivity extends BaseTopBarActivity implements View.OnClickListener, AuthenticationContract.IView {
    @BindView(R.id.activity_authentication_rl_root)
    RelativeLayout mLlRoot;
    @BindView(R.id.activity_authentication_btn_commit)
    Button btn_submit;//提交按钮
    @BindView(R.id.activity_authentication_iv_avatar)
    ImageView iv_avatar;//头像
    @BindView(R.id.activity_authentication_playview)
    PlayView playView;//播放的view
    @BindView(R.id.activity_authentication_rl_record)
    RelativeLayout rl_record;//开始录制视频按钮
    @BindView(R.id.activity_authentication_rl_play)
    RelativeLayout rl_play;//播放按钮
    @BindView(R.id.activity_authentication_rl_re_rcord)
    RelativeLayout rl_re_record;//重新录制按钮
    @BindView(R.id.activity_authentication_tv_facebook)
    TextView tv_facebook;//facebook的账号
    @BindView(R.id.activity_authentication_tv_copy)
    TextView tv_copy;//复制按钮
    private long playPostion = -1;
    private long duration = -1;
    private File videoFile;
    String stringExtra=null;
    public final static String DATA = "URL";
    private String   videoNumber=null;
    private AuthenticationPresenter mAuthticationPresenter;
    private String text;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_authentication;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_anchor);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mAuthticationPresenter = new AuthenticationPresenter(this);
    }

    @Override
    protected void setListeners() {
        btn_submit.setOnClickListener(this);
        iv_avatar.setOnClickListener(this);
        rl_record.setOnClickListener(this);
        rl_play.setOnClickListener(this);
        rl_re_record.setOnClickListener(this);
        tv_copy.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
      mAuthticationPresenter.getFaceBookAccount();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_authentication_btn_commit:
                //提交审核
                mAuthticationPresenter.submitCheck(videoNumber,videoFile);
                break;
            case R.id.activity_authentication_iv_avatar:
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        mAuthticationPresenter.upLoadAvator(file, true);
                    }
                });
                //上传头像
                break;
            case R.id.activity_authentication_rl_record:
                //开始录制
                 //LaunchHelper.getInstance().launchResult(AuthenticationActivity.this,RecordActivity.class,new PlayViewParcelable(videoFile.getAbsolutePath()), REQUEST_CODE);
                Intent intent = new Intent(this, RecordActivity.class);
                videoDir();
                intent.putExtra(DATA, videoFile.getAbsolutePath());
                startActivityForResult(intent,REQUEST_CODE);
                break;
            case R.id.activity_authentication_rl_play:
                //开始播放
                LaunchHelper.getInstance().launch(mContext, PlayActivity.class,new PlayViewParcelable(videoFile.getAbsolutePath()));
                break;
            case R.id.activity_authentication_rl_re_rcord:
                //重新录制
                Intent intent3 = new Intent(this, RecordActivity.class);
                videoDir();
                intent3.putExtra(DATA, videoFile.getAbsolutePath());
                startActivityForResult(intent3,REQUEST_CODE);
                break;
            case R.id.activity_authentication_tv_copy:
               mAuthticationPresenter.copyFacebookAccount();
                //复制按钮
                break;
        }
    }
    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAuthticationPresenter.finish();
        playView.stopPlayback();
    }
    private void initPlayView() {
        if(!TextUtils.isEmpty(stringExtra)){
            playView.setVideoURI(Uri.parse(stringExtra));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playView.seekTo(1);
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //获取视频资源的宽度
                    int videoWidth = mp.getVideoWidth();
                    //获取视频资源的高度
                    int videoHeight = mp.getVideoHeight();
                    playView.setSizeH(videoHeight);
                    playView.setSizeW(videoWidth);
                    playView.requestLayout();
                    duration = mp.getDuration();
                }
            });
        }
    }

    public void videoDir() {
          videoFile = new File(C.Video.SD_PATH);
        // 创建文件
        try {
            if (!videoFile.exists()) {
                videoFile.createNewFile();
                videoFile.setWritable(Boolean.TRUE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));
    }


    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        playPostion = playView.getCurrentPosition();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //判断请求码
        if(requestCode == REQUEST_CODE){
            //判断结果码
            if(resultCode == RecordActivity.RESULT_CODE){
                stringExtra = data.getStringExtra(C.Video.DATA);
                String s = data.getStringExtra("videoNumber");
                videoNumber = s.replaceAll(" ", "");
                rl_record.setVisibility(View.GONE);
                rl_play.setVisibility(View.VISIBLE);
                rl_re_record.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initPlayView();
    }

    @Override
    public void getAvatarUrl(String url) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url)
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).transform(new RoundedCornersTransformation(this,20,20)).build());
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setFaceBookAccount(String account) {
        text=account;
      tv_facebook.setText(account);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String copyFacebookAccount() {
        return tv_facebook.getText().toString();
    }
}
