package com.wiscom.vchat.ui.personalcenter.adapter;

import android.content.Context;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.model.WithdrawRecord;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class WithdrawRecordAdapter extends CommonRecyclerViewAdapter<WithdrawRecord> {


    public WithdrawRecordAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public WithdrawRecordAdapter(Context context, int layoutResId, List<WithdrawRecord> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(WithdrawRecord withdrawRecord, int position, RecyclerViewHolder holder) {
        if(withdrawRecord!=null){
//            holder.setText(R.id.tv_pay_way,withdrawRecord.getWithdrawWay());
            holder.setText(R.id.tv_pay_way,"paypal提现");
            holder.setText(R.id.tv_money,withdrawRecord.getWithdrawMoney());
//            holder.setText(R.id.tv_withdraw_time, DateTimeUtil.convertTimeMillis2String(withdrawRecord.getRecordTime()));

            Date nowTime = new Date(withdrawRecord.getRecordTime());
            SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd");
            String retStrFormatNowDate = sdFormatter.format(nowTime);
            holder.setText(R.id.tv_withdraw_time,retStrFormatNowDate);
        }

    }
}
