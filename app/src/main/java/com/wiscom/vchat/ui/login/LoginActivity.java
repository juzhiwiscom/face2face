package com.wiscom.vchat.ui.login;

import android.content.Context;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.ui.dialog.FindPasswordDialog;
import com.wiscom.vchat.ui.register.RegisterActivity;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 登录页面
 * Created by zhangdroid on 2017/5/12.
 */
public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener, LoginContract.IView {
    @BindView(R.id.login_root)
    LinearLayout mLlRoot;
    @BindView(R.id.login_account)
    TextInputEditText mEtAccount;
    @BindView(R.id.login_password)
    TextInputEditText mEtPassword;
    @BindView(R.id.login)
    Button mBtnLogin;
    @BindView(R.id.login_register)
    TextView mTvRegister;
    @BindView(R.id.find_password)
    TextView mTvFindPassword;

    private LoginPresenter mLoginPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mLoginPresenter = new LoginPresenter(this);
        mLoginPresenter.start();
        checkValid();
    }

    @Override
    protected void setListeners() {
        mBtnLogin.setOnClickListener(this);
        mTvRegister.setOnClickListener(this);
        mTvFindPassword.setOnClickListener(this);
        mEtAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkValid();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkValid();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:// 登录
                mLoginPresenter.login();
                break;

            case R.id.login_register:// 跳转注册页面
                LaunchHelper.getInstance().launch(mContext, RegisterActivity.class);
                break;

            case R.id.find_password:// 忘记密码
                mLoginPresenter.findPassword();
                break;
        }
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void setAccount(String account) {
        mEtAccount.setText(account);
    }

    @Override
    public void setPassword(String password) {
        mEtPassword.setText(password);
    }

    @Override
    public String getAccount() {
        return mEtAccount.getText().toString();
    }

    @Override
    public String getPassword() {
        return mEtPassword.getText().toString();
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void showFindPwdDialog(String account, String password) {
        FindPasswordDialog.show((String) TextUtils.concat(getString(R.string.find_pwd_account, account), "\n",
                getString(R.string.find_pwd_password, password)), getSupportFragmentManager());
    }

    /**
     * 检测是否可以登录
     */
    private void checkValid() {
        mBtnLogin.setEnabled(!TextUtils.isEmpty(mEtAccount.getText().toString())
                && !TextUtils.isEmpty(mEtPassword.getText().toString()));
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

}
