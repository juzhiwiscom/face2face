package com.wiscom.vchat.ui.pay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.Utils;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.PayDict;
import com.wiscom.vchat.data.model.PayWay;
import com.wiscom.vchat.event.PaySuccessEvent;
import com.wiscom.vchat.ui.pay.util.IabHelper;
import com.wiscom.vchat.ui.pay.util.IabResult;
import com.wiscom.vchat.ui.pay.util.Purchase;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

//import com.facebook.appevents.AppEventsConstants;
//import com.facebook.appevents.AppEventsLogger;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class PayPresenter implements PayContract.IPresenter {
    private static final String TAG = PayPresenter.class.getSimpleName();
    private PayContract.IView mPayView;
    private Context mContext;
    private IabHelper mHelper;
    private boolean mIsInitialized;
    private static final int REQUEST_CODE = 0;
    private PayAdapter mPayAdapter;

    public PayPresenter(PayContract.IView view) {
        this.mPayView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        mHelper = new IabHelper(mContext, C.KEY_GOOGLE_APP);
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                // 初始化成功
                mIsInitialized = true;
            }
        });
    }

    @Override
    public void finish() {
        mPayView = null;
        mContext = null;
        mIsInitialized = false;
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
//        mPayAdapter = null;
    }

    @Override
    public void getPayWay(final String serviceType) {
        mPayView.showLoading();
        ApiManager.getPayWay(serviceType, new IGetDataListener<PayWay>() {
            @Override
            public void onResult(PayWay payWay, boolean isEmpty) {
                if (null != payWay) {
                    mPayAdapter = new PayAdapter(mPayView.obtainContext(), R.layout.item_pay_list,
                            checkProductValid(payWay.getDictPayList()),serviceType);
                    mPayView.setAdapter(mPayAdapter);
                }
                mPayView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mPayView.showNetworkError();
                }
                if(mPayView!=null)
                mPayView.dismissLoading();
            }
        });
    }

    /**
     * 检测后台商品是否可用
     *
     * @param list 后台返回的商品信息列表
     * @return 可用的商品列表
     */
    private List<PayDict> checkProductValid(List<PayDict> list) {
        List<PayDict> validList = new ArrayList<>();
        if (!Utils.isListEmpty(list)) {
            for (PayDict item : list) {
                if ("1".equals(item.getIsvalid())) {
                    validList.add(item);
                }
            }
        }
        return validList;
    }

    @Override
    public void purchase(String sku) {
        if (checkPayEnabled()) {
            if (mHelper != null) {
                try {
                    mHelper.launchPurchaseFlow((Activity) mContext, sku, REQUEST_CODE, mPurchaseFinishedListener);
                }
                catch(IllegalStateException ex){
                    Toast.makeText((Activity) mContext, "Please retry in a few seconds.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            mPayView.showTip(mContext.getString(R.string.pay_disabled));
        }
    }

    @Override
    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    /**
     * @return 检查Google支付是否可用
     */
    private boolean checkPayEnabled() {
        boolean b1 = DeviceUtil.checkAPKExist(mPayView.obtainContext(), "com.android.vending");
        boolean b2 = mHelper != null;
        boolean mIsInitialized = this.mIsInitialized;
        boolean b = DeviceUtil.checkAPKExist(mPayView.obtainContext(), "com.android.vending") && mHelper != null && this.mIsInitialized;

        return b;
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                LogUtil.v("TianTian","支付失败");
                return;
            }
            switch (purchase.getSku()) {
                case C.pay.SKU_1_VIP:
                    payment(purchase, 0,"1_month_VIP",14.99);
                    LogUtil.v("TianTian","支付成功，开始调后台接口");
                    break;
                case C.pay.SKU_3_VIP:
                    payment(purchase, 1,"1_month_VIP",29.99);
                    break;
                case C.pay.SKU_12_VIP:
                    payment(purchase, 2,"1_year_VIP",74.99);
                    break;
                case C.pay.SKU_500_diamonds:
                    payment(purchase, 0,"500_diamonda",7.99);
                    break;
                case C.pay.SKU_1000_diamonds:
                    payment(purchase, 1,"1000_diamonda",15.99);
                    break;
                case C.pay.SKU_3000_diamonds:
                    payment(purchase, 2,"3000_diamonda",47.99);
                    break;
                case C.pay.SKU_5000_diamonds:
                    payment(purchase,3,"5000_diamonda",79.99);
                    break;
                case C.pay.SKU_10000_diamonds:
                    payment(purchase, 4,"10000_diamonda",159.99);
                    break;
            }
            // 消耗掉已购买的商品
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                // 支付成功
                switch (purchase.getSku()) {
                    case C.pay.SKU_1_VIP:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_3_VIP:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_12_VIP:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_500_diamonds:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_1000_diamonds:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_3000_diamonds:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_5000_diamonds:
                        showSuccessTip();
                        break;
                    case C.pay.SKU_10000_diamonds:
                        showSuccessTip();
                        break;
                }
            }
        }
    };

    /**
     * 向后台发送支付结果
     */
    private void payment(Purchase purchase, final int position, final String product, final double price) {
        String payType="1";
        if(product.contains("VIP")){
             payType="1";
        }else {
             payType="2";
        }
        ApiManager.payment(purchase, mPayAdapter.getAdapterDataList().get(position).getServiceId(), payType, new IGetDataListener<String>() {

            @Override
            public void onResult(String s, boolean isEmpty) {
                if (!TextUtils.isEmpty(s) && "success".equals(s)) {
                    // 后台支付成功，刷新用户商品信息
                    LogUtil.e(TAG, "后台支付成功");
                    EventBus.getDefault().post(new PaySuccessEvent());

//                    facebook手动支付统计
//                    AppEventsLogger logger = AppEventsLogger.newLogger(mContext);
//                    Bundle parameters = new Bundle();
//                    parameters.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "USD");
//                    parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, product);
////                    parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT, "[{\"id\": \"1234\", \"quantity\": 2, \"item_price\": 5.99}, {\"id\": \"5678\", \"quantity\": 1, \"item_price\": 9.99}]");
//
//                    logger.logEvent(AppEventsConstants.EVENT_NAME_PURCHASED, price,parameters);
//                    com.facebook

                } else {
                    // 后台支付结果请求失败
                    LogUtil.e(TAG, "后台支付失败");
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                // 后台支付结果请求失败
                LogUtil.e(TAG, "后台支付失败");
            }

        });
    }

    private void showSuccessTip() {
        mPayView.showTip(mContext.getString(R.string.pay_success));
    }

}
