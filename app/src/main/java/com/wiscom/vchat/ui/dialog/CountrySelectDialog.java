package com.wiscom.vchat.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.constant.DataDictManager;
import com.wiscom.vchat.ui.dialog.adapter.CountryAdapter;
import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.dialog.BaseDialogFragment;

/**
 * 国家选择对话框（单选）
 * Created by zhangdroid on 2017/6/9.
 */
public class CountrySelectDialog extends BaseDialogFragment {
    private String mSelectedCountry;
    private OnCountrySelectListener mOnCountrySelectListener;

    public static CountrySelectDialog newInstance(String selectedCountry, OnCountrySelectListener listener) {
        CountrySelectDialog languageSelectDialog = new CountrySelectDialog();
        languageSelectDialog.mSelectedCountry = selectedCountry;
        languageSelectDialog.mOnCountrySelectListener = listener;
        return languageSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_country;
    }

    @Override
    protected void setDialogContentView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.dialog_country_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        final CountryAdapter countryAdapter = new CountryAdapter(getContext(), R.layout.item_dialog_select_country,
                DataDictManager.getCountryList());
        countryAdapter.initSelectState(mSelectedCountry);
        recyclerView.setAdapter(countryAdapter);
        countryAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                int selectedPosition = countryAdapter.getSelectedPosition();
                if (selectedPosition != position) {
                    countryAdapter.setItemSelectState(selectedPosition, false);
                    countryAdapter.notifyItemChanged(selectedPosition);
                    countryAdapter.setSelectedPosition(position);
                    countryAdapter.setItemSelectState(position, true);
                    countryAdapter.notifyItemChanged(position);
                }
            }
        });
        Button btnCancel = (Button) view.findViewById(R.id.dialog_country_cancel);
        Button btnSave = (Button) view.findViewById(R.id.dialog_country_save);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != mOnCountrySelectListener) {
                    mOnCountrySelectListener.onSelected(countryAdapter.getSelectedItem());
                }
            }
        });
    }

    public static void show(FragmentManager fragmentManager, String selectedCountry, OnCountrySelectListener onLanguageSelectListener) {
        newInstance(selectedCountry, onLanguageSelectListener).show(fragmentManager, "country_select");
    }

    public interface OnCountrySelectListener {
        void onSelected(String country);
    }

}
