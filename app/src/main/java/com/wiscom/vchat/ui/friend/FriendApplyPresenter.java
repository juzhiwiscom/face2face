package com.wiscom.vchat.ui.friend;

import android.content.Context;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.FriendListModel;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.library.util.Utils;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class FriendApplyPresenter implements FriendApplyContract.IPresenter{
   private FriendApplyContract.IView mFriendApplyView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "20";
    public FriendApplyPresenter(FriendApplyContract.IView view){
        this.mFriendApplyView=view;
        this.mContext=view.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mContext=null;
        mFriendApplyView=null;

    }
    @Override
    public void loadApplyList() {
        load();
    }

    @Override
    public void refresh() {
        pageNum = 1;
        load();
    }

    @Override
    public void loadMore() {
        mFriendApplyView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {

        ApiManager.getFriendApply(pageNum, new IGetDataListener<FriendListModel>() {
            @Override
            public void onResult(FriendListModel friendListModel, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mFriendApplyView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mFriendApplyView.showNoMore();
                        mFriendApplyView.hideLoadMore();
                    }

                } else {
                    if (null != friendListModel) {
                        List<UserBase> list = friendListModel.getListUser();
                        if (!Utils.isListEmpty(list)) {
//                            aboutNameList = getAboutNameList(list);
                            if(pageNum==1){
                                if(mFriendApplyView.getFriendApplyAdapter()!=null){
                                    mFriendApplyView.getFriendApplyAdapter().replaceAll(list);
                                }
                            }else if(pageNum>1){
                                mFriendApplyView.getFriendApplyAdapter().appendToList(list);
                            }
                            mFriendApplyView.showNoMore();
                            mFriendApplyView.hideLoadMore();
                        }
                    }
                }
                mFriendApplyView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mFriendApplyView.hideRefresh(1);
                if (!isNetworkError) {
                    mFriendApplyView.toggleShowError(true, msg);
                }

            }
        });
    }

}
