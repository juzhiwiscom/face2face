package com.wiscom.vchat.ui.homepage.presenter;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.CloseLocaleVedioEvent;
import com.wiscom.vchat.event.IntoPersonFragmentEvent;
import com.wiscom.vchat.event.UpdateAvatorEvent;
import com.wiscom.vchat.ui.screen.Screen1Activity;
import com.wiscom.vchat.ui.video.VideoActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by tianzhentao on 2017/12/23.
 */

public class HomeFragment extends BaseFragment {
//    @BindView(R.id.fl_set_local_viedao)
    FrameLayout flSetLocalViedao;
    Unbinder unbinder;
    @BindView(R.id.iv_loading_heart)
    ImageView ivLoadingHeart;
    @BindView(R.id.fl_loading)
    FrameLayout flLoading;
    @BindView(R.id.home_personal)
    ImageView homePersonal;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    private boolean close=false;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

//    @Override
//    protected View getNoticeView() {
//        return null;
//    }
//
//    @Override
//    protected void getArgumentParcelable(Parcelable parcelable) {
//
//    }

    @Override
    protected void initViews() {
        updateAvator();

//        心的动画效果
//        Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_show_heart);
//        ivLoadingHeart.startAnimation(hyperspaceJumpAnimation);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!close){
            if (flSetLocalViedao != null)
                flSetLocalViedao.removeAllViews();
            flSetLocalViedao = (FrameLayout) getActivity().findViewById(R.id.fl_set_local_viedao);
//        预览本地视频
            if (AgoraHelper.getInstance() != null) {
                AgoraHelper.getInstance().setupLocalVideo(false,mContext, flSetLocalViedao);
            }
        }

    }

    @Override
    protected void setListeners() {
        flLoading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //申请录音权限
                getRecordPermission();

            }
        });
        homePersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                跳转到个人中心页面
                EventBus.getDefault().post(new IntoPersonFragmentEvent());
            }
        });
        llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(getActivity(), Screen1Activity.class);
            }
        });

    }


    @Override
    protected void loadData() {

    }

    private void getRecordPermission() {
        int checkCallPhonePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
            HomeFragment.this.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},222);
            return ;
        } else {
            //有权限，
            LaunchHelper.getInstance().launch(mContext, VideoActivity.class);

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 222) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //申请成功，
                LaunchHelper.getInstance().launch(mContext, VideoActivity.class);

            } else {
                Toast.makeText(getActivity(), "RECORD AUDIO PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setBackgroundColor(int backgroundColor) {
        flSetLocalViedao.setBackgroundColor(backgroundColor);
    }

    public void setAlpha1(int alpha) {
        flSetLocalViedao.getBackground().setAlpha(alpha);
    }
    @Subscribe
    public void onEvent(UpdateAvatorEvent event) {
        updateAvator();
    }
    @Subscribe
    public void onEvent(CloseLocaleVedioEvent event) {
         close = event.isClose();
        if(close){
            if (flSetLocalViedao != null)
                flSetLocalViedao.removeAllViews();
        }else{
            if (flSetLocalViedao != null)
//                flSetLocalViedao.removeAllViews();
//            flSetLocalViedao = (FrameLayout) getActivity().findViewById(R.id.fl_set_local_viedao);

//        预览本地视频
          AgoraHelper.getInstance().setupLocalVideo(false,mContext, flSetLocalViedao);
        }
    }

    private void updateAvator() {
        //        加载头像
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(UserPreference.getSmallImage()).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(homePersonal).build());

    }
}
