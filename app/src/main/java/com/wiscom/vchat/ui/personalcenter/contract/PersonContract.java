package com.wiscom.vchat.ui.personalcenter.contract;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/27.
 */
public interface PersonContract {



    interface IView extends BaseView {

        /**
         * 设置用户头像
         *
         * @param url
         */
        void setUserAvatar(String url);

        /**
         * 设置用户昵称
         *
         * @param name
         */
        void setUserName(String name);

        /**
         * 设置用户ID
         *
         * @param id
         */
        void setUserId(String id);

        /**
         * 设置用户接听率/投诉率
         *
         * @param receivePrecent  接听率
         * @param complainPrecent 投诉率
         */
        void setUserPrecent(String receivePrecent, String complainPrecent);

        /**
         * 设置普通用户账户余额
         *
         * @param balance 余额
         */
        void setBalance(String balance);

        /**
         * 设置是否为主播
         */
        void setIsAnchor(boolean isAnchor);

        /**
         * 设置累计收入
         *
         * @param income 收入
         */
        void setAccumulatedIncome(String income);

        /**
         * 设置主播当前账户金币数
         *
         * @param
         */
        void setAnchorCurrentIncome(String currentIncome);

        /**
         * 设置主播价格
         *
         * @param price 价格
         */
        void setPrice(String price);


    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取当前用户信息并设置
         */
        void getUserInfo();

        /**
         * 更新普通用户余额
         */
        void updateBalance();

        /**
         * 更新播主价格
         */
        void updatePrice(String newPrice);

    }

}
