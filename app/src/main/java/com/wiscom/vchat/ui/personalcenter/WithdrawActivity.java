package com.wiscom.vchat.ui.personalcenter;

import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.parcelable.TvBalanceParcelable;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 提现页面
 * Created by zhangdroid on 2017/5/27.
 */
public class WithdrawActivity extends BaseTopBarActivity {



    @BindView(R.id.tv_withdraw)
    TextView tvWithdraw;
    @BindView(R.id.iv_paypal)
    ImageView ivPaypal;
    @BindView(R.id.iv_alipay)
    ImageView ivAlipay;
    private TvBalanceParcelable balanceParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdraw;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_withdraw);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
         balanceParcelable = (TvBalanceParcelable) parcelable;

    }

    @Override
    protected void initViews() {
        tvWithdraw.setText(balanceParcelable.balance);
        /**进入提现记录页面**/
        setRightText(getString(R.string.person_withdraw_record), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchHelper.getInstance().launch(mContext, WithdrawRecordActivity.class);

            }
        });

        ivPaypal.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
//                LaunchHelper.getInstance().launch(mContext, WithdrawToPaypalActivity.class);
                LaunchHelper.getInstance().launch(mContext, WithdrawToPaypalActivity.class,
                        new TvBalanceParcelable((String) tvWithdraw.getText()));
            }
        });
        ivAlipay.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                LaunchHelper.getInstance().launch(mContext, WithdrawToAlipayActivity.class);
                ToastUtil.showShortToast(mContext,"宝宝正在努力开通中，请小主稍等时日。");

            }
        });
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Message message) {
        if(message.what==1111){
            String newMoney =(String) message.obj;
            double withdrawMoney=Double.parseDouble(newMoney)*20;
            double curentMoney= Double.parseDouble((String) tvWithdraw.getText())-withdrawMoney;
            tvWithdraw.setText((int)curentMoney+"");
//            tvWithdraw.setText("5000");
        }
    }

}
