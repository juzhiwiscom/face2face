package com.wiscom.vchat.ui.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.recyclerview.CommonRecyclerViewAdapter;
import com.wiscom.vchat.common.recyclerview.RecyclerViewHolder;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.VideoRecord;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DateTimeUtil;

import java.util.List;

/**
 * 视频记录适配器
 * Created by zhangdroid on 2017/7/6.
 */
public class VideoHistoryAdapter extends CommonRecyclerViewAdapter<VideoRecord> {

    public VideoHistoryAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public VideoHistoryAdapter(Context context, int layoutResId, List<VideoRecord> dataList) {
        super(context, layoutResId, dataList);
    }
    @Override
    public void convert(int position,RecyclerViewHolder holder,VideoRecord videoRecord) {
        if (null != videoRecord) {
//            switch (videoRecord.getCallStatus()) {
//                case "1":// 拨出
//                    holder.setImageResource(R.id.item_video_history_orientation, R.drawable.video_green);
//                    break;
//                case "2":// 收到
//                    holder.setImageResource(R.id.item_video_history_orientation, R.drawable.video_gray);
//                    break;
//                case "3":// 拒绝
//                    holder.setImageResource(R.id.item_video_history_orientation, R.drawable.video_red);
//                    break;
//            }
            UserBase userBase = videoRecord.getUserBase();
            if (null != userBase) {
//                switch (Integer.parseInt(userBase.getStatus())) {
//                    case C.homepage.STATE_FREE:// 空闲
//                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_free));
//                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_green);
//                        break;
//                    case C.homepage.STATE_BUSY:// 忙线中
//                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_busy));
//                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_red);
//                        break;
//                    case C.homepage.STATE_NO_DISTRUB:// 勿扰
//                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_nodistrub));
//                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_gray);
//                        break;
//                }
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                        .url(userBase.getIconUrlMininum()).imageView((ImageView) holder.getView(R.id.item_video_history_avatar)).build());
                holder.setText(R.id.item_video_history_nickname, userBase.getNickName());
                if (userBase.getGender()==1) {
                    holder.setImageResource(R.id.item_video_history_gender, R.drawable.ic_female);
                } else {
                    holder.setImageResource(R.id.item_video_history_gender, R.drawable.ic_male);
                }
//                holder.setText(R.id.item_video_history_age, String.valueOf(userBase.getAge()));
                String country = ParamsUtils.getCountryValues(userBase.getCountry());
                String state = ParamsUtils.getStateValue(userBase.getState());
                holder.setText(R.id.item_video_history_area, TextUtils.concat(country, " ", state).toString());
//                holder.setText(R.id.item_video_history_banner, String.valueOf(userBase.getAge()));
                ImageView banner = (ImageView) holder.getView(R.id.item_video_history_banner);
                banner.setImageResource(Util.getBanner(country));
            }
            // 通话时间
            holder.setText(R.id.item_video_history_time, DateTimeUtil.convertTimeMillis2String(videoRecord.getRecordTime()));
        }
    }

}
