package com.wiscom.vchat.ui.pay;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.facebook.CallbackManager;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBean;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.event.PaySuccessEvent;
import com.wiscom.vchat.parcelable.PayParcelable;

import org.greenrobot.eventbus.Subscribe;


/**
 * 支付页面
 * Created by zhangdroid on 2017/5/25.
 */
public class PayActivity extends BaseTopBarActivity implements PayContract.IView {
    RecyclerView mRecyclerView;
    LinearLayout mLlRoot;
    LinearLayout payVipFunction;
//    private CallbackManager callbackManager = CallbackManager.Factory.create();
//    com.facebook

    private PayPresenter mPayPresenter;
    private PayParcelable mPayParcelable;
    private String mTitle;
    private String fromTag;
    private TextView mDiamondsNum;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_pay;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return mTitle;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayParcelable = (PayParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mLlRoot = (LinearLayout) findViewById(R.id.pay_root);
        mRecyclerView = (RecyclerView) findViewById(R.id.pay_product_list);
        payVipFunction=findViewById(R.id.pay_vip_function);
        fromTag = mPayParcelable.fromTag;
        if (fromTag.equals(C.pay.OPEN_VIP)) {
//            来自开通vip
            mTitle = getString(R.string.person_open_vip);
            payVipFunction.setVisibility(View.VISIBLE);
        } else if (fromTag.equals(C.pay.BUY_DIAMON)) {
//            来自购买钻石
            LinearLayout mDiamondsDetail = (LinearLayout) findViewById(R.id.pay_diamonds_detail);
            mDiamondsNum = (TextView) findViewById(R.id.buy_diamon_have_diamon);
            mDiamondsDetail.setVisibility(View.VISIBLE);
//            从后台获取钻石数量
            getMyInfo();
            mTitle = getString(R.string.person_buy_diamon);
            payVipFunction.setVisibility(View.GONE);

        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mPayPresenter = new PayPresenter(this);
        mPayPresenter.start();
    }

    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                // 更新用户信息
                if (null != myInfo) {
                    UserBean userBase = myInfo.getUserDetail().getUserBean();
                    if (null != userBase) {
                        BeanPreference.setBeanCount(userBase.getCounts());
                        mDiamondsNum.setText(BeanPreference.getBeanCount() + "");
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mDiamondsNum.setText(BeanPreference.getBeanCount() + "");
            }

        });
    }

    @Override
    protected void setListeners() {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean b = mPayPresenter.handleActivityResult(requestCode, resultCode, data);
        if (!b) {
            super.onActivityResult(requestCode, resultCode, data);
//            facebook统计购买情况
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//            com.facebook
        }
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//            com.facebook

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPayPresenter.finish();
    }

    @Override
    protected void loadData() {
        mPayPresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
        mPayPresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {
        if (null != payAdapter) {
            payAdapter.setPayPresenter(mPayPresenter);
            mRecyclerView.setAdapter(payAdapter);
        }
    }
    @Subscribe
    public void onEvent(PaySuccessEvent event) {
        getMyInfo();
    }


}
