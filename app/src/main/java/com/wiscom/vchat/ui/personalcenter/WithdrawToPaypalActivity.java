package com.wiscom.vchat.ui.personalcenter;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.data.api.ApiConstant;
import com.wiscom.vchat.parcelable.TvBalanceParcelable;
import com.wiscom.vchat.ui.personalcenter.contract.WithdrawToPaypalContract;
import com.wiscom.vchat.ui.personalcenter.presenter.WithdrawToPaypalPresenter;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.SnackBarUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WithdrawToPaypalActivity extends BaseTopBarActivity implements WithdrawToPaypalContract.IView, View.OnClickListener {

    @BindView(R.id.et_money)
    EditText etMoney;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_sur)
    EditText etSur;
    @BindView(R.id.et_nation)
    TextView etNation;
    @BindView(R.id.btn_withdraw_explain)
    Button btnWithdrawExplain;
    @BindView(R.id.btn_withdraw)
    Button btnWithdraw;
    @BindView(R.id.rl_paypal)
    RelativeLayout rlPaypal;
    @BindView(R.id.tv_money_null)
    TextView tvMoneyNull;
    @BindView(R.id.tv_email_null)
    TextView tvEmailNull;
    @BindView(R.id.tv_name_null)
    TextView tvNameNull;
    @BindView(R.id.tv_sur_null)
    TextView tvSurNull;
    @BindView(R.id.tv_nation_null)
    TextView tvNationNull;
    @BindView(R.id.rl_nation)
    RelativeLayout rlNation;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    private WithdrawToPaypalPresenter mWithdrawToPaypalPresenter;
    private boolean num;
    private TvBalanceParcelable balanceParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdraw_to_paypal;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.commodity_detail);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        balanceParcelable = (TvBalanceParcelable) parcelable;

    }

    @Override
    protected void initViews() {
        /**将金币数转化为美元**/
     int balance=Integer.parseInt(balanceParcelable.balance)/20;
        tvBalance.setText(balance+"");
//        tvBalance.setText("130000");
        mWithdrawToPaypalPresenter = new WithdrawToPaypalPresenter(this);
    }

    @Override
    protected void setListeners() {
        rlNation.setOnClickListener(this);
        btnWithdrawExplain.setOnClickListener(this);
        btnWithdraw.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /**国家选择**/
            case R.id.rl_nation:
                mWithdrawToPaypalPresenter.selectCountry();
                break;
            /**提现说明**/
            case R.id.btn_withdraw_explain:
                mWithdrawToPaypalPresenter.showWithdrawExplain(getString(R.string.withdraw_agreement), ApiConstant.URL_AGREEMENT_WITHDRAW);
                break;
            /**提现**/
            case R.id.btn_withdraw:
                String getMoney = etMoney.getText().toString().trim();
                String getEmail = etEmail.getText().toString().trim();
                String getName = etName.getText().toString().trim();
                String getNation = etNation.getText().toString().trim();
                String getSur = etSur.getText().toString().trim();
                if (!TextUtils.isEmpty(getMoney)) {
                    CharSequence tvbalance = tvBalance.getText();
                    Double balance = Double.parseDouble(String.valueOf(tvbalance));
                    tvMoneyNull.setVisibility(View.VISIBLE);
                    try {
                        Double money = Double.parseDouble(getMoney);
                        if (money > balance) {
                            tvMoneyNull.setText(getString(R.string.not_ufficient_funds));
                        } else if (money < 50) {
                            tvMoneyNull.setText(getString(R.string.min_amount));
                        } else if (money > 600) {
                            tvMoneyNull.setText(getString(R.string.max_amount));
                        } else if (TextUtils.isEmpty(getEmail)) {
                            tvEmailNull.setVisibility(View.VISIBLE);
                            tvMoneyNull.setVisibility(View.GONE);
                        } else if (TextUtils.isEmpty(getName)) {
                            tvNameNull.setVisibility(View.VISIBLE);
                            tvMoneyNull.setVisibility(View.GONE);
                            tvEmailNull.setVisibility(View.GONE);
                        } else if (TextUtils.isEmpty(getSur)) {
                            tvSurNull.setVisibility(View.VISIBLE);
                            tvMoneyNull.setVisibility(View.GONE);
                            tvEmailNull.setVisibility(View.GONE);
                            tvNameNull.setVisibility(View.GONE);
                        } else if (getNation.contains(getString(R.string.action))) {
                            tvNationNull.setVisibility(View.VISIBLE);
                            tvMoneyNull.setVisibility(View.GONE);
                            tvEmailNull.setVisibility(View.GONE);
                            tvNameNull.setVisibility(View.GONE);
                            tvSurNull.setVisibility(View.GONE);
                        } else {
                            tvMoneyNull.setVisibility(View.GONE);
                            tvEmailNull.setVisibility(View.GONE);
                            tvNameNull.setVisibility(View.GONE);
                            tvNationNull.setVisibility(View.GONE);
                            tvSurNull.setVisibility(View.GONE);
                            mWithdrawToPaypalPresenter.withdrawSure(getMoney,getEmail,getName,getSur,getNation);
                        }

                    } catch (Exception e) {
                        tvMoneyNull.setText(getString(R.string.currect_amount));
                    }
                } else if (TextUtils.isEmpty(getMoney)) {





                    tvMoneyNull.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(rlPaypal, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public String getCountry() {
        return etNation.getText().toString();
    }

    @Override
    public void setCountry(String country) {
        if (!TextUtils.isEmpty(country)) {
            etNation.setText(country);
        }
    }

    @Override
    public void finishActivity() {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWithdrawToPaypalPresenter.finish();
    }


}
