package com.wiscom.vchat.ui.video;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by tianzhentao on 2018/1/22.
 */

public class VideoRemoteView extends FrameLayout {
    private int mL,mT,mR,mB;
    private boolean isFist = true;

    public VideoRemoteView(@NonNull Context context) {
        super(context);
    }

    public VideoRemoteView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoRemoteView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (isFist){
            mL = l;
            mT=t;
            mR=r;
            mB=b;
            isFist =false;
        }
        getChildAt(0).layout(mL, mT, mR, mB);
    }

}
