package com.wiscom.vchat.ui.videoshow;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by tianzhentao on 2018/5/4.
 */

public class VideoShowFragment extends BaseFragment {
    @BindView(R.id.video_show_uploading)
    TextView videoShowUploading;
    @BindView(R.id.video_show_record)
    TextView videoShowRecord;
    @BindView(R.id.video_show_tab)
    TabLayout videoShowTab;
    @BindView(R.id.video_show_viewpager)
    ViewPager videoShowViewpager;
    Unbinder unbinder;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_show;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        VideoShowAdapter videoShowAdapter = new VideoShowAdapter(getChildFragmentManager());
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new NewestVideoFragemnt());
        fragments.add(new HotVideoFragment());
        videoShowAdapter.setData(fragments, Arrays.asList(getString(R.string.newest),getString(R.string.hot)));
        videoShowViewpager.setAdapter(videoShowAdapter);
        videoShowTab.setupWithViewPager(videoShowViewpager);
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.video_show_uploading, R.id.video_show_record})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.video_show_uploading:
                break;
            case R.id.video_show_record:
                break;
        }
    }
}
