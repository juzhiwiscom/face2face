package com.wiscom.vchat.ui.chat;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hyphenate.chat.EMImageMessageBody;
import com.hyphenate.chat.EMMessage;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.parcelable.BigPhotoParcelable;
import com.wiscom.vchat.ui.photo.BigPhotoActivity;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.provider.ItemViewProvider;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DateTimeUtil;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.widget.ChatImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天图片消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class ImageMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;

    public ImageMessageRightProvider(Context context) {
        this.mContext = context;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_image_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.IMAGE && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_image_avatar_right)).build());
        if (null != emMessage) {
            holder.setText(R.id.item_chat_image_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            final EMImageMessageBody emImageMessageBody = (EMImageMessageBody) emMessage.getBody();
            if (null != emImageMessageBody) {
                ChatImageView chatImageView = (ChatImageView) holder.getView(R.id.item_chat_image_right);
                // 设置图片规格：1/3屏幕宽度，宽高比3：4
                int width = DeviceUtil.getScreenWidth(mContext) / 3;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, (int) ((4 / 3.0f) * width));
                chatImageView.setLayoutParams(layoutParams);
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                        .url(emImageMessageBody.getThumbnailUrl()).imageView(chatImageView).build());
                chatImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 点击查看大图
                        List<String> list = new ArrayList<String>();
                        list.add(emImageMessageBody.getRemoteUrl());
                        LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0, list));
                    }
                });
            }
        }
    }

}
