package com.wiscom.vchat.ui.detail.contract;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/14.
 */
public interface AlbumContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        FragmentManager obtainFragmentManager();

        void setAdapter(RecyclerView.Adapter adater);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取图片列表
         */
        void getPhotoInfo();

        /**
         * 上传图片
         */
        void uploadImage();

    }

}
