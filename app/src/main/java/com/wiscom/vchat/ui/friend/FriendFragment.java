package com.wiscom.vchat.ui.friend;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarFragment;
import com.wiscom.vchat.common.RefreshRecyclerView;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.ApplyAddFriendMode;
import com.wiscom.vchat.data.model.ApplyPayModel;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.ui.pay.PayActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.Unbinder;

/**
 * Created by tianzhentao on 2018/1/3.
 */

public class FriendFragment extends BaseTopBarFragment implements FriendContract.IView{
//    @BindView(R.id.friend_rl_applay)
//    RelativeLayout friendRlApplay;
//    @BindView(R.id.chat_history_refresh)
//    AutoSwipeRefreshLayout mSwipeRefresh;
//    @BindView(R.id.chat_history_list)
//    XRecyclerView mRecyclerView;
    @BindView(R.id.message_recyclerview)
    RefreshRecyclerView mRefreshRecyclerView;
    Unbinder unbinder;
    private LinearLayoutManager linearLayoutManager;
    private FriendPresenter mFollowPresenter;
    private FriendAdapter mFirendAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_friend;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.tab_follow);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void initViews() {
        mIvRight.setVisibility(View.VISIBLE);
        mIvRight.setImageResource(R.mipmap.add_btn);
        mFollowPresenter = new FriendPresenter(this);

        // 设置下拉刷新样式
        mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
        mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRefreshRecyclerView.setLayoutManager(linearLayoutManager);
        // 设置加载动画
        mRefreshRecyclerView.setLoadingView(Util.getLoadingView(getActivity()));
    }
    @Override
    protected void setListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFollowPresenter.refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                mFollowPresenter.loadFollowUserList();
            }
        });
//        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                mFollowPresenter.refresh();
//            }
//        });
        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                判断是否拦截
                ApiManager.applyAddFriend("", new IGetDataListener<ApplyAddFriendMode>() {
                    @Override
                    public void onResult(ApplyAddFriendMode baseModel, boolean isEmpty) {
                        String isSucceed = baseModel.getIsSucceed();
                        if(!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")){
                            int payIntercept = baseModel.getPayIntercept();
                            if(payIntercept==0){
                                //不拦截
                                LaunchHelper.getInstance().launch(getActivity(), AddFriendActivity.class);
                            }else {
                                //弹出拦截对话框
                                AlertDialog.show(getFragmentManager(), mContext.getString(R.string.alert), mContext.getString(R.string.intercept),
                                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                            @Override
                                            public void onNegativeClick(View view) {
                                            }

                                            @Override
                                            public void onPositiveClick(View view) {
                                                if(BeanPreference.getBeanCount()>10){
//                                                    扣费接口
                                                    payRequest();
                                                }
//                                                跳转开通vip的界面
                                                LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                            }
                                        });
                            }
                        }

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }
                });
            }
        });

    }

    private void payRequest() {
        ApiManager.applypay(new IGetDataListener<ApplyPayModel>() {
            @Override
            public void onResult(ApplyPayModel baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                LaunchHelper.getInstance().launch(getActivity(), AddFriendActivity.class);

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }


    @Override
    protected void loadData() {
        mFollowPresenter.loadFollowUserList();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }
    @Override
    public void hideRefresh(int delaySeconds) {
        if (mRefreshRecyclerView != null) {
            mRefreshRecyclerView.refreshCompleted(delaySeconds);
            mRefreshRecyclerView.loadMoreCompleted(delaySeconds);
        }
//        mSwipeRefresh.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mSwipeRefresh.isRefreshing()) {
//                    mSwipeRefresh.setRefreshing(false);
//                }
//            }
//        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
//        super.toggleShowError(toggle, msg, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mFollowPresenter.refresh();
//            }
//        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRefreshRecyclerView.setAdapter(adapter);
//        mRecyclerView.setHasFixedSize(true);
//        LayoutInflater li = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
//        View headView = li.inflate(R.layout.item_friend_header, null);
//        RelativeLayout friendRlApplay = (RelativeLayout) headView.findViewById(R.id.friend_rl_applay);
//        LinearLayout friendLlSecretary = (LinearLayout) headView.findViewById(R.id.friend_ll_secretary);
//
//        mRecyclerView.addHeaderView(headView);
//        friendRlApplay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LaunchHelper.getInstance().launch(getActivity(), FriendApplyActivity.class);
//            }
//        });
//        friendLlSecretary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                小秘书点击事件
////                    条目点击，跳转到聊天界面
//                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
//                        "100010000", getString(R.string.friend_secretary), "KeFuIconUrl","country","1"));
//            }
//        });
    }

    @Override
    public void setOnLoadMoreListener(RefreshRecyclerView.OnLoadingMoreListener onLoadMoreListener) {
        mRefreshRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
//        mRecyclerView.setVisibility(R.id.load_more_progress, true);
//        mRecyclerView.setVisibility(R.id.load_more_msg, true);
//        mRecyclerView.setVisibility(R.id.load_more_empty, false);
//        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
//        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
//        mRecyclerView.setVisibility(R.id.load_more_empty, true);
//        mRecyclerView.setVisibility(R.id.load_more_progress, false);
//        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFollowPresenter.finish();
    }
    @Subscribe
    public void onEvent(UpdateFriendListEvent event) {
        mFollowPresenter.refresh();
    }
}
