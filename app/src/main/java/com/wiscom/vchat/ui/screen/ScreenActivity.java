//package com.wiscom.vchat.ui.screen;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.Parcelable;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.LinearLayoutManager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
//import com.wiscom.library.dialog.AlertDialog;
//import com.wiscom.library.dialog.OnDialogClickListener;
//import com.wiscom.library.net.NetUtil;
//import com.wiscom.library.util.LaunchHelper;
//import com.wiscom.library.widget.AutoSwipeRefreshLayout;
//import com.wiscom.library.widget.XRecyclerView;
//import com.wiscom.vchat.C;
//import com.wiscom.vchat.R;
//import com.wiscom.vchat.base.BaseFragmentActivity;
//import com.wiscom.vchat.data.api.ApiManager;
//import com.wiscom.vchat.data.api.IGetDataListener;
//import com.wiscom.vchat.data.model.BaseModel;
//import com.wiscom.vchat.data.preference.BeanPreference;
//import com.wiscom.vchat.data.preference.UserPreference;
//import com.wiscom.vchat.parcelable.PayParcelable;
//import com.wiscom.vchat.ui.pay.PayActivity;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//
///**
// * Created by tianzhentao on 2018/1/2.
// */
//
//public class ScreenActivity extends BaseFragmentActivity implements ScreenContract.IView {
//    @BindView(R.id.screen_list)
//    XRecyclerView mRecyclerView;
//    @BindView(R.id.screen_refresh)
//    AutoSwipeRefreshLayout mSwipeRefreshLayout;
//    @BindView(R.id.screen_exit)
//    ImageView screenExit;
//    private ScreenAdapter mScreenAdapter;
//    private ScreenPresenter mWithdrawRecordPresenter;
//    // 初次进入时自动显示刷新，此时不调用刷新
//    private boolean mIsFirstLoad = true;
//    private LinearLayout allPeople;
//    private LinearLayout womanPeople;
//    private LinearLayout manPeople;
//
//    @Override
//    protected int getLayoutResId() {
//        return R.layout.activity_screen;
//    }
//
//    @Override
//    protected boolean isApplyTranslucentStatusBar() {
//        return false;
//    }
//
//    @Override
//    protected boolean isRegistEventBus() {
//        return false;
//    }
//
//    @Override
//    protected void getBundleExtras(Parcelable parcelable) {
//
//    }
//
//    @Override
//    protected View getNoticeView() {
//        return null;
//    }
//
//    @Override
//    protected void initViews() {
//        mSwipeRefreshLayout.setFirstHideRefresh();
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
//        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
//        GridLayoutManager linearLayoutManager = new GridLayoutManager(mContext,1, LinearLayoutManager.HORIZONTAL, false);
//        mRecyclerView.setLayoutManager(linearLayoutManager);
//        mRecyclerView.setHasFixedSize(false);
////
//        mScreenAdapter = new ScreenAdapter(mContext, R.layout.item_screen);
//        mRecyclerView.setAdapter(mScreenAdapter, R.layout.common_load_more);
////        mRecyclerView.setAdapter(mWithdrawRecordAdapter);
//
//        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
//        View headView = li.inflate(R.layout.item_screen_header, null);
//        mRecyclerView.addHeaderView(headView);
//        mWithdrawRecordPresenter = new ScreenPresenter(this);
//        mIsFirstLoad = false;
//
//        allPeople = (LinearLayout) headView.findViewById(R.id.screen_all_people);
//        womanPeople = (LinearLayout) headView.findViewById(R.id.screen_woman_people);
//        manPeople = (LinearLayout) headView.findViewById(R.id.screen_man_people);
//        int beanCount = BeanPreference.getScreenGender();
//        if(beanCount==0){
//            manPeople.setBackgroundResource(R.mipmap.screen_select);
//            womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
//            allPeople.setBackgroundResource(R.mipmap.screen_no_select);
//        }else if(beanCount==1){
//            womanPeople.setBackgroundResource(R.mipmap.screen_select);
//            allPeople.setBackgroundResource(R.mipmap.screen_no_select);
//            manPeople.setBackgroundResource(R.mipmap.screen_no_select);
//        }else{
//            allPeople.setBackgroundResource(R.mipmap.screen_select);
//            womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
//            manPeople.setBackgroundResource(R.mipmap.screen_no_select);
//        }
//    }
//
//    @Override
//    protected void setListeners() {
//        screenExit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//        // 下拉刷新和上拉加载更多
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (mIsFirstLoad) {
//                    mIsFirstLoad = false;
//                } else {
//                    mWithdrawRecordPresenter.refresh();
//                }
//            }
//        });
//        mRecyclerView.setOnLoadingMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                mWithdrawRecordPresenter.loadMore();
//            }
//        });
//        allPeople.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                allPeople.setBackgroundResource(R.mipmap.screen_select);
//                womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                manPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                BeanPreference.setScreenGender(-1);
//            }
//        });
//        womanPeople.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
//                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
//                            @Override
//                            public void onNegativeClick(View view) {
//                            }
//
//                            @Override
//                            public void onPositiveClick(View view) {
////                                判断钻石是否足够，不够的话跳转支付界面
//                                int beanCount = BeanPreference.getBeanCount();
//                                if (beanCount != 0 && beanCount > 50) {
////                                    钻石足够
//                                    int currentBean = BeanPreference.getBeanCount() - 50;
//                                    BeanPreference.setBeanCount(currentBean);
//                                    womanPeople.setBackgroundResource(R.mipmap.screen_select);
//                                    allPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                                    manPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                                    BeanPreference.setScreenGender(1);
//                                } else {
////                                    充值
//                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
//                                    intoPayActivityNum("1");
//                                }
//
//                            }
//                        });
//
//            }
//        });
//        manPeople.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
//                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
//                            @Override
//                            public void onNegativeClick(View view) {
//                            }
//
//                            @Override
//                            public void onPositiveClick(View view) {
////                                判断钻石是否足够，不够的话跳转支付界面
//                                int beanCount = BeanPreference.getBeanCount();
//                                if (beanCount != 0 && beanCount > 2) {
////                                    钻石足够
//                                    int currentBean = BeanPreference.getBeanCount() - 2;
//                                    BeanPreference.setBeanCount(currentBean);
//                                    manPeople.setBackgroundResource(R.mipmap.screen_select);
//                                    womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                                    allPeople.setBackgroundResource(R.mipmap.screen_no_select);
//                                    BeanPreference.setScreenGender(0);
//                                } else {
////                                    充值
//                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
//                                    intoPayActivityNum("2");
//                                }
//
//                            }
//                        });
//            }
//        });
//
//
//    }
//
//    private void intoPayActivityNum(String type) {
//        ApiManager.intoPayActivityNum("", UserPreference.getId(),"10001",type,new IGetDataListener<BaseModel>() {
//            @Override
//            public void onResult(BaseModel baseModel, boolean isEmpty) {
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//            }
//        });
//    }
//
//    @Override
//    protected void loadData() {
//        mWithdrawRecordPresenter.loadHistoryList();
//    }
//
//    @Override
//    protected void networkConnected(NetUtil.NetType type) {
//
//    }
//
//    @Override
//    protected void networkDisconnected() {
//
//    }
//
//    @Override
//    public ScreenAdapter getVideoHistoryAdapter() {
//        return mScreenAdapter;
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        // TODO: add setContentView(...) invocation
//        ButterKnife.bind(this);
//    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mWithdrawRecordPresenter.finish();
//    }
//
//    @Override
//    public Context obtainContext() {
//        return mContext;
//    }
//
//    @Override
//    public void showTip(String msg) {
////        SnackBarUtil.showShort(mFlRecord, msg);
//    }
//
//    @Override
//    public void toggleShowError(boolean toggle, String msg) {
//        super.toggleShowError(toggle, msg, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toggleShowError(false, null, null);
//                mWithdrawRecordPresenter.refresh();
//            }
//        });
//    }
//
//    @Override
//    public void toggleShowEmpty(boolean toggle, String msg) {
//        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toggleShowEmpty(false, null, null);
//                mWithdrawRecordPresenter.refresh();
//            }
//        });
//    }
//
//    @Override
//    public void hideRefresh(int delaySeconds) {
//        mSwipeRefreshLayout.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (null != mSwipeRefreshLayout && mSwipeRefreshLayout.isRefreshing()) {
//                    mSwipeRefreshLayout.setRefreshing(false);
//                }
//            }
//        }, delaySeconds * 1000);
//    }
//
//    @Override
//    public void showLoadMore() {
////        mRecyclerView.setVisibility(R.id.load_more_progress, true);
////        mRecyclerView.setVisibility(R.id.load_more_msg, true);
////        mRecyclerView.setVisibility(R.id.load_more_empty, false);
////        mRecyclerView.startLoadMore();
//    }
//
//    @Override
//    public void hideLoadMore() {
//        mRecyclerView.finishLoadMore();
//    }
//
//    @Override
//    public void showNoMore() {
////        mRecyclerView.setVisibility(R.id.load_more_empty, true);
////        mRecyclerView.setVisibility(R.id.load_more_progress, false);
////        mRecyclerView.setVisibility(R.id.load_more_msg, false);
//    }
////    @Override
////    public WithdrawRecordAdapter getWithdrawRecordAdapter() {
////        return mWithdrawRecordAdapter;
////    }
//}
