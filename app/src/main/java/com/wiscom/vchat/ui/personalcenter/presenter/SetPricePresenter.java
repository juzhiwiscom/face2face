package com.wiscom.vchat.ui.personalcenter.presenter;

import android.os.Message;

import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.ui.personalcenter.contract.SetPriceConteact;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2017/7/5.
 */

public class SetPricePresenter implements SetPriceConteact.IPresenter{


    private SetPriceConteact.IView mSetPriceView;

    public SetPricePresenter(SetPriceConteact.IView mSetPriceView) {
        this.mSetPriceView = mSetPriceView;
    }






           @Override
           public void savePrice() {
            mSetPriceView.showLoading();
            final String newPrice = mSetPriceView.getTestring();
            ApiManager.savePrice(newPrice,new IGetDataListener<String>(){

            @Override
            public void onResult(String s, boolean isEmpty) {
                mSetPriceView.dismissLoading();
                //发送消息更新价格
                Message message = new Message();
                message.what=1010;
                message.obj=newPrice;
                EventBus.getDefault().post(message);
                mSetPriceView.finishActivity();
            }


            @Override
            public void onError(String msg, boolean isNetworkError) {
                mSetPriceView.dismissLoading();
                if (isNetworkError) {
                    mSetPriceView.showNetworkError();
                } else {
                    mSetPriceView.showTip(msg);
                }
            }


        });
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mSetPriceView=null;

    }
}
