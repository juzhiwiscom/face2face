package com.wiscom.vchat.ui.video;

import android.content.Context;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.data.model.AgoraParams;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.AgoraEvent;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.video.contract.VideoInviteContract;
import com.wiscom.vchat.ui.video.presenter.VideoInvitePresenter;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * 视频邀请页面（邀请/被邀请）
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoInviteActivity extends BaseFragmentActivity implements View.OnClickListener,
        SurfaceHolder.Callback, VideoInviteContract.IView {
    @BindView(R.id.video_invite_avatar)
    ImageView mIvAvatar;
    @BindView(R.id.video_invite_nickname)
    TextView mTvNickname;
    @BindView(R.id.video_invite_tip)
    TextView mTvVideoTip;
    @BindView(R.id.video_cancel)
    TextView mTvVideoCancel;
    @BindView(R.id.video_reject)
    TextView mTvVideoReject;
    @BindView(R.id.video_accept)
    TextView mTvVideoAccept;
    // 遮罩层
    @BindView(R.id.video_invite_avatar_big)
    ImageView mIvAvatarBig;
    @BindView(R.id.video_invite_mask)
    View mMaskView;
    // 相机预览
    @BindView(R.id.video_invite_preview)
    SurfaceView mSurfaceView;

    private VideoInviteParcelable mVideoInviteParcelable;
    private VideoInvitePresenter mVideoInvitePresenter;
    private SurfaceHolder mSurfaceHolder;
    // 发起视频时的频道id，生成规则：邀请人guid+被邀请人guid
    private String mChannelId;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_video_invite;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoInviteParcelable = (VideoInviteParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (null != mVideoInviteParcelable) {
            ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl).transform(new CropCircleTransformation(mContext))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvAvatar).build());
            mTvNickname.setText(mVideoInviteParcelable.nickname);
            mTvVideoTip.setText(mVideoInviteParcelable.isInvited ? getString(R.string.video_invited) : getString(R.string.video_invite));
            mTvVideoCancel.setVisibility(mVideoInviteParcelable.isInvited ? View.GONE : View.VISIBLE);
            mTvVideoReject.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            mTvVideoAccept.setVisibility(mVideoInviteParcelable.isInvited ? View.VISIBLE : View.GONE);
            if (mVideoInviteParcelable.isInvited) {
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(mVideoInviteParcelable.imgageUrl)
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(mIvAvatarBig).build());
                mIvAvatarBig.setVisibility(View.VISIBLE);
                mMaskView.setVisibility(View.VISIBLE);
                mSurfaceView.setVisibility(View.GONE);
                // 被邀请加入的频道id
                mChannelId = mVideoInviteParcelable.channelId;
                mVideoInvitePresenter = new VideoInvitePresenter(this, mVideoInviteParcelable.uId, mVideoInviteParcelable.account,
                        mVideoInviteParcelable.nickname, mVideoInviteParcelable.hostPrice,mVideoInviteParcelable.country,mVideoInviteParcelable.genter,mVideoInviteParcelable.imgageUrl,C.Video.ACCEPT_INVITE);

            } else {
                mIvAvatarBig.setVisibility(View.GONE);
                mMaskView.setVisibility(View.GONE);
                mSurfaceView.setVisibility(View.VISIBLE);
                // 生成channelId
                mChannelId = UserPreference.getId() + mVideoInviteParcelable.uId;
                mVideoInvitePresenter = new VideoInvitePresenter(this, mVideoInviteParcelable.uId, mVideoInviteParcelable.account,
                        mVideoInviteParcelable.nickname, mVideoInviteParcelable.hostPrice,mVideoInviteParcelable.country,mVideoInviteParcelable.genter,mVideoInviteParcelable.imgageUrl,C.Video.GO_CALL);

            }
        }

    }

    @Override
    protected void setListeners() {
        mTvVideoCancel.setOnClickListener(this);
        mTvVideoReject.setOnClickListener(this);
        mTvVideoAccept.setOnClickListener(this);
        if (!mVideoInviteParcelable.isInvited) {
            // 主动邀请视频时开启相机预览
            mSurfaceHolder = mSurfaceView.getHolder();
            mSurfaceHolder.addCallback(this);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mVideoInvitePresenter.startPreView();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mVideoInvitePresenter.stopPreview();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_cancel:// 取消
                mVideoInvitePresenter.cancelInVite(mChannelId, mVideoInviteParcelable.account);
                break;

            case R.id.video_reject:// 拒绝
                mVideoInvitePresenter.refuseInvite(mChannelId, mVideoInviteParcelable.account);
                break;

            case R.id.video_accept:// 接受
                mVideoInvitePresenter.acceptInvite(mChannelId, mVideoInviteParcelable.account);
                break;
        }
    }

    @Override
    protected void loadData() {
        if (!mVideoInviteParcelable.isInvited) {
            // 发起视频邀请
            mVideoInvitePresenter.startInvite(mChannelId, mVideoInviteParcelable.account, new Gson().toJson(
//                    new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage())));
            new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(),UserPreference.getCountry(),UserPreference.getGender(), C.Video.GO_CALL)));

        }
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoInvitePresenter.finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing,屏蔽返回键
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        ToastUtil.showShortToast(mContext, msg);
    }

    @Override
    public void hideSurfaceView() {
        mSurfaceView.setVisibility(View.GONE);
    }

    @Override
    public SurfaceHolder getHolder() {
        return mSurfaceHolder;
    }

    @Override
    public void changeInviteState(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            mTvVideoTip.setText(msg);
        }
    }

    @Override
    public void finishActivity(int seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, seconds * 1000);
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraEvent event) {
        mVideoInvitePresenter.handleAgoraEvent(event);
    }

}
