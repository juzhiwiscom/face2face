package com.wiscom.vchat.ui.homepage.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.parcelable.ListParcelable;
import com.wiscom.vchat.ui.homepage.HomepageAdapter;
import com.wiscom.vchat.ui.homepage.ListFragment;
import com.wiscom.vchat.ui.homepage.ListFragment2;
import com.wiscom.vchat.ui.homepage.ListFragment3;
import com.wiscom.vchat.ui.homepage.contract.HomepageContract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/31.
 */
public class HomepagePresenter implements HomepageContract.IPresenter {
    private HomepageContract.IView mHomepageView;
    private Context mContext;

    public HomepagePresenter(HomepageContract.IView view) {
        this.mHomepageView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mHomepageView = null;
        mContext = null;
    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mHomepageView.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabList;
        if (UserPreference.isAnchor()) {// 播主
            tabList = Arrays.asList(mContext.getString(R.string.homepage_goddess),
                    mContext.getString(R.string.homepage_active_friend), mContext.getString(R.string.homepage_new_friend));
            fragmentList.add(ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_GODDESS)));
            fragmentList.add(ListFragment2.newInstance(new ListParcelable(C.homepage.TYPE_ACTIVE_FRIEND)));
            fragmentList.add(ListFragment3.newInstance(new ListParcelable(C.homepage.TYPE_NEW_FRIEND)));
        } else {// 普通用户
            tabList = Arrays.asList(mContext.getString(R.string.homepage_goddess),
                    mContext.getString(R.string.homepage_active_anchor), mContext.getString(R.string.homepage_new));
            fragmentList.add(ListFragment.newInstance(new ListParcelable(C.homepage.TYPE_GODDESS)));
            fragmentList.add(ListFragment2.newInstance(new ListParcelable(C.homepage.TYPE_ACTIVE_ANCHOR)));
            fragmentList.add(ListFragment3.newInstance(new ListParcelable(C.homepage.TYPE_NEW)));
        }
        homepageAdapter.setData(fragmentList, tabList);
        mHomepageView.setAdapter(homepageAdapter);
    }

}
