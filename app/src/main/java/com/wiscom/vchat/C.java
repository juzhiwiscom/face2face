package com.wiscom.vchat;

import android.os.Environment;

import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.library.util.FileUtil;

import java.io.File;

/**
 * <p>该类保存项目中用到的所有常量，用法参考系统常量类R.java。</p>
 * 包括：
 * <li>1、全局常量</li>
 * <li>2、模块常量：各个功能模块独有，按模块分别配置</li>
 * <li>3、Service和Receiver action常量</li>
 * <li>4、Permission常量：高危权限</li>
 * Created by zhangdroid on 2017/5/17.
 */
public final class C {

    // *************************************************** 全局常量 ***************************************************

    /**
     * Google app key
     */
//    public static final String KEY_GOOGLE_APP = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnUtDRtQtNuJsPj2i2LOuXser5U170XO0kuH5TDW/1KIb6sZykMWcAyot+HU7ptWvcOkBfn5qNOr7YZotLPhurFaVZexAj+waST3FWEJDfDC/aumT2HBSdsNXs98BtvM8fXsUB9Dauy45fhGKlL9U9RlRXSZDNU77a8Hqb0u/zwm9AnWz2p1d9tzTIbBm1yUKbmyXm6hZU0e7Vxm4a8LAk/jXQAdPN3HOAP0iq5SCN7bM1eOkOoHplJmItGO/PKNjq8C7jhJ9MEycf80j4jg/5b1wnsOlenu+j/+6nFGtFn04aL4kXFIe8WedT8YbUOXrYfgHbY/aS0ruktYgIPi7GQIDAQAB";
    public static final String KEY_GOOGLE_APP = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt9lE68fq8J3kKL2pr6rTRs38ae99zShbPTTbcb7FU+dl19dAcI7BUMi87voUqm2LXMPEZPKF8pn8TzSd7lYdpku/TEYRxR/5I4Gt63riUnryjwIQPDTbKeV428CZAOF2c1NVgB81iUeztQTgimN0zeFPyM4JhpvMtmI0p4Pv8v78z572O+y3CkD93gRkg5Rmd1EANkl2NEZuMyM59oAd5wAsgtWaRqlDgKLmTwpQ6cQyilA8TZGfCm+edYPwCZ2YZ6+V7ZN2GBMFvaPIphQaJx/uG6nnIe5qEeoZYy/pGC5ChOLovmWkQljeIPxRZo5rGibDDkHRevnTcUkUdcOZkQIDAQAB";
    /**
     * 声网 app id
     */
    public static final String KEY_AGORA = "9ebeab23fa13431089bb8760b6f3a47a";
    /**
     * 头像的保存路径
     */
    public static final String AVATAR_PATH = FileUtil.getExternalFilesDir(BaseApplication.getGlobalContext(), Environment.DIRECTORY_PICTURES)
            + File.separator + "photo.jpg";
    /**
     * 产品号
     */
    public static final String PRODUCT_ID = "2";


    /**
     * 渠道号
     */
    public static final String FID = "11154";
    /**
     * 渠道号美国
     */
     String F_IDAM = "30201";
    /**
     * 渠道号 印度
     */
     String F_IDIN = "30212";
    /*
    * 澳大利亚
    * */
    static String F_IDNSW="30211";
    /*
    * 印尼
    * */
    static String F_IDJT="30213";
    /*
    * 英国
    * */
    static String F_IDGL="30214";
    /*
    * 加拿大
    * */
    static String F_IDTO="30215";
    /*
    * 新西兰
    * */
    static String F_IDAL="30216";
    /*
    * 爱尔兰
    * */
    static String F_IDDL="30217";
    /*
    * 南非
    * */
    static String F_IDJG="30218";
    /*
    * 新加坡
    * */
    static String F_IDOD="30220";
    /*
    * 巴基斯坦
    * */
    static String F_IDKC="30224";
    /*
    * 菲律宾
    * */
    static String F_IDMA="30225";
    /*
    * 香港
    * */
    static String F_IDWC="30233";


    // *************************************************** Service action常量 ***************************************************

    public static final class service {
        public static final String ACTION_APP_CREATE = "com.online.face2face.service.APP_CREATE";
    }

    // *************************************************** Receiver action常量 ***************************************************

    public static final class receiver {
    }

    // *************************************************** 高危权限常量 ***************************************************

    public static final class permission {
        public static final String PERMISSION_PHONE = "android.permission.READ_PHONE_STATE";
        public static final String PERMISSION_WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
        public static final String PERMISSION_READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
        public static final String PERMISSION_CAMERA = "android.permission.CAMERA";
        public static final String PERMISSION_RECORD_AUDIO = "android.permission.RECORD_AUDIO";
        public static final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";

    }

    // *************************************************** 首页tab常量 ***************************************************

    public static final class homepage {
        /**
         * 女神
         */
        public static final int TYPE_GODDESS = 1;
        /**
         * 活跃播主
         */
        public static final int TYPE_ACTIVE_ANCHOR = 2;
        /**
         * 新人
         */
        public static final int TYPE_NEW = 3;
        /**
         * 活跃聊友
         */
        public static final int TYPE_ACTIVE_FRIEND = 4;
        /**
         * 新晋聊友
         */
        public static final int TYPE_NEW_FRIEND = 5;

        /**
         * 空闲
         */
        public static final int STATE_FREE = 1;
        /**
         * 忙线中
         */
        public static final int STATE_BUSY = 2;
        /**
         * 勿扰
         */
        public static final int STATE_NO_DISTRUB = 3;
    }

    // *************************************************** 消息tab常量 ***************************************************

    public static final class message {
        // 录音状态常量
        public static final int STATE_IDLE = 0;// 默认
        public static final int STATE_RECORDING = 1;// 录音中
        public static final int STATE_CANCELED = 2;// 取消录音
        // 聊天页Handler常量
        public static final int MSG_TYPE_VOICE_UI_TIME = 666;// 录音框UI计时
        public static final int MSG_TYPE_UPDATE = 888;// 发送消息后更新
        public static final int MSG_TYPE_TIMER = 0;// 计时
        public static final int MSG_TYPE_DELAYED = 1;// 延时
        public static final int MSG_TYPE_INIT = 2;// 初始化聊天记录
        public static final int MSG_TYPE_LOAD_MORE = 3;// 加载更多聊天记录
        public static final int MSG_TYPE_SEND_TXT = 4;// 发送文字消息
        public static final int MSG_TYPE_SEND_VOICE = 5;// 发送语音消息
        public static final int MSG_TYPE_SEND_IMAGE = 6;// 发送图片消息
    }

    // *************************************************** 支付页常量 ***************************************************

    public static final class pay {
        // google商品SKU
        public static final String SKU1 = "1pay";
        public static final String SKU2 = "2pay";
        public static final String SKU3 = "3pay";
        public static final String SKU4 = "4pay";
        public static final String SKU5 = "5pay";
        public static final String SKU6 = "6pay";
        public static final String SKU7 = "7pay";
        // google商品SKU
        public static final String SKU_1_VIP = "1monthvip1";
        public static final String SKU_3_VIP = "3monthvip";
        public static final String SKU_12_VIP = "12monthvip";
        public static final String SKU_500_diamonds = "500diamondspay";
        public static final String SKU_1000_diamonds = "1000diamondspay";
        public static final String SKU_3000_diamonds = "3000diamondspay";
        public static final String SKU_5000_diamonds = "5000diamondspay";
        public static final String SKU_10000_diamonds = "10000diamondspay";
        //
        // 支付拦截来源
        public static final String FROM_TAG_PERSON = "1";// 个人中心页面
        public static final String FROM_TAG_HOMEPAGE = "2";// 首页列表项
        public static final String FROM_TAG_CHAT = "3";// 聊天页面
        public static final String OPEN_VIP = "2";// 开通VIP
        public static final String BUY_DIAMON = "1";// 购买钻石
    }

    // *************************************************** 视频录制和上传 ***************************************************

    public static final class Video {
        public static final String DATA = "URL";
        public static final int REQUEST_CODE = 1001;
        public static final int RESULT_CODE = 2001;
        public static final String SD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "videorecord" + File.separator + "recording.mp4";
        public static final String SD_IMAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "videorecord" + File.separator + "recordimage.jpg";
        public static final String SD_IMAGE_tre_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "videorecordtre" + File.separator + "recordtreimage.jpg";
        public static final String NAME = "videorecord";
        public final static int SIZE_1 = 640;
        public final static int SIZE_2 = 480;
        public final static String ACCEPT_INVITE ="1";
        public final static String GO_CALL ="2";
        public final static String RANDOM_VIDEO ="3";
        public final static String SYSYTEM_VIDEO ="4";


    }
    // *************************************************** 视频通话 ***************************************************

}
