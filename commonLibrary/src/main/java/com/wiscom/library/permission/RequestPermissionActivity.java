package com.wiscom.library.permission;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscom.library.R;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.Utils;

import java.util.List;
import java.util.ListIterator;

/**
 * 请求权限页面
 * Created by zhangdroid on 2017/5/19.
 */
public class RequestPermissionActivity extends FragmentActivity {
    private FrameLayout mFlRoot;
    // 默认申请权限提示页面
    private LinearLayout mLlDefault;
    private TextView mTvTitle;
    private TextView mTvDescription;
    private RecyclerView mRecyclerView;
    private Button mBtnSet;
    // 被拒绝的权限/引导用户设置页面
    private LinearLayout mLlRefused;
    private TextView mTvDialogTitle;
    private TextView mTvDialogMessage;
    private Button mBtnPositive;
    private Button mBtnNegative;

    // 请求权限类型常量
    public static final int PERMISSION_TYPE_SINGLE = 1;
    public static final int PERMISSION_TYPE_MUTI = 2;

    private static final int REQUEST_CODE_SINGLE = 1;
    private static final int REQUEST_CODE_MUTI = 2;
    public static final int REQUEST_CODE_AGAIN = 3;
    private static final int REQUEST_SETTING = 4;

    private PermissionParcelable mPermissionParcelable;
    private static PermissionCallback mCallback;
    private List<PermissonItem> mPermissionList;
    /**
     * 重新申请权限数组的索引
     */
    private int mRePermissionIndex;

    public static void setCallBack(PermissionCallback callBack) {
        RequestPermissionActivity.mCallback = callBack;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permission_dialog);
        setFinishOnTouchOutside(false);
        mFlRoot = (FrameLayout) findViewById(R.id.permission_root);
        mLlDefault = (LinearLayout) findViewById(R.id.permission_default);
        mTvTitle = (TextView) findViewById(R.id.permission_title);
        mTvDescription = (TextView) findViewById(R.id.permission_description);
        mRecyclerView = (RecyclerView) findViewById(R.id.permission_list);
        mBtnSet = (Button) findViewById(R.id.permission_set);
        mLlRefused = (LinearLayout) findViewById(R.id.permission_refused);
        mTvDialogTitle = (TextView) findViewById(R.id.permission_dialog_title);
        mTvDialogMessage = (TextView) findViewById(R.id.permission_dialog_message);
        mBtnNegative = (Button) findViewById(R.id.permission_dialog_negative);
        mBtnPositive = (Button) findViewById(R.id.permission_dialog_positive);
        // 获取传递的参数并设置
        mPermissionParcelable = LaunchHelper.getInstance().getParcelableExtra(this);
        if (null != mPermissionParcelable) {
            mPermissionList = mPermissionParcelable.permissonList;
            if (Utils.isListEmpty(mPermissionList)) {
                if (null != mCallback) {
                    mCallback.onFinished();
                }
                return;
            }
            if (mPermissionParcelable.type == PERMISSION_TYPE_SINGLE) {// 单个权限申请
                mFlRoot.setVisibility(View.GONE);
                requestPermission(new String[]{mPermissionList.get(0).permission}, REQUEST_CODE_SINGLE);
            } else {// 同时申请一组权限
                mFlRoot.setVisibility(View.VISIBLE);
                mLlDefault.setVisibility(View.VISIBLE);
                String title = mPermissionParcelable.title;
                if (!TextUtils.isEmpty(title)) {
                    mTvTitle.setText(title);
                } else {
                    mTvTitle.setText(getString(R.string.permission_dialog_title, DeviceUtil.getAppName(this)));
                }
                String description = mPermissionParcelable.description;
                if (!TextUtils.isEmpty(description)) {
                    mTvDescription.setText(description);
                } else {
                    mTvDescription.setText(getString(R.string.permission_dialog_msg, DeviceUtil.getAppName(this)));
                }
                // 显示需要开启的权限
                GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(gridLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                PermissionAdapter permissionAdapter = new PermissionAdapter(this, R.layout.item_permission, mPermissionList);
                mRecyclerView.setAdapter(permissionAdapter);
                // 点击开始请求权限
                mBtnSet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestPermission(getPermissions(), REQUEST_CODE_MUTI);
                    }
                });
            }
        }
    }

    private String[] getPermissions() {
        int size = mPermissionList.size();
        String[] permissions = new String[size];
        for (int i = 0; i < size; i++) {
            PermissonItem permissonItem = mPermissionList.get(i);
            if (null != permissonItem) {
                permissions[i] = permissonItem.permission;
            }
        }
        return permissions;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPermissionParcelable = null;
        mCallback = null;
        mPermissionList = null;
    }

    @Override
    public void onBackPressed() {
    }

    /**
     * 请求权限
     *
     * @param permissions
     * @param requestCode
     */
    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(RequestPermissionActivity.this, permissions, requestCode);
    }

    /**
     * 再次请求被拒绝的权限
     *
     * @param permission
     */
    private void requestPermissionAgain(final String permission) {
        String permissionName = getPermissionItem(permission).permissionName;
        String title = String.format(getString(R.string.permission_title), permissionName);
        String message = String.format(getString(R.string.permission_denied), permissionName, DeviceUtil.getAppName(this));
        showAlert(title, message, getString(R.string.permission_positive), getString(R.string.permission_negative),
                false, permission);
    }

    private void showAlert(String title, String message, String positive, String negative, final boolean gotoSetting, final String permission) {
        mLlDefault.setVisibility(View.GONE);
        mLlRefused.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(title)) {
            mTvDialogTitle.setText(title);
        }
        if (!TextUtils.isEmpty(message)) {
            mTvDialogMessage.setText(message);
        }
        if (!TextUtils.isEmpty(positive)) {
            mBtnPositive.setText(positive);
        }
        if (!TextUtils.isEmpty(negative)) {
            mBtnNegative.setText(negative);
        }
        mBtnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClose();
            }
        });
        mBtnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gotoSetting) {
                    // 打开应用详情页面，让用户设置权限
                    DeviceUtil.showAppDetail(RequestPermissionActivity.this, REQUEST_SETTING);
                } else {
                    requestPermission(new String[]{permission}, REQUEST_CODE_AGAIN);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_SINGLE:// 单个权限
                String permission = getPermissionItem(permissions[0]).permission;
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onGuarantee(permission, 0);
                } else {
                    onDeny(permission, 0);
                }
                finish();
                break;

            case REQUEST_CODE_MUTI:// 一组权限
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) { // 权限允许后，删除需要再次检查的权限
                        PermissonItem item = getPermissionItem(permissions[i]);
                        mPermissionList.remove(item);
                        onGuarantee(permissions[i], i);
                    } else { // 权限拒绝
                        onDeny(permissions[i], i);
                    }
                }
                if (mPermissionList.size() > 0) {// 用户拒绝了某个或多个权限，重新申请
                    requestPermissionAgain(mPermissionList.get(mRePermissionIndex).permission);
                } else {
                    onFinish();
                }
                break;

            case REQUEST_CODE_AGAIN:// 重新申请被拒绝的权限
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {// 重新申请后再次拒绝
                    String name = getPermissionItem(permissions[0]).permissionName;
                    String title = String.format(getString(R.string.permission_title), name);
                    String message = String.format(getString(R.string.permission_denied_with_set),
                            DeviceUtil.getAppName(this), name, DeviceUtil.getAppName(this));
                    showAlert(title, message, getString(R.string.permission_setting), getString(R.string.permission_reject),
                            true, null);
                    onDeny(permissions[0], 0);
                } else {
                    onGuarantee(permissions[0], 0);
                    if (mRePermissionIndex < mPermissionList.size() - 1) {
                        // 继续申请下一个被拒绝的权限
                        requestPermissionAgain(mPermissionList.get(++mRePermissionIndex).permission);
                    } else {
                        // 全部允许了
                        onFinish();
                    }
                }
                break;
        }
    }

    /**
     * 设置权限页面结果回调
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SETTING) {
            checkPermission();
            if (mPermissionList.size() > 0) {// 设置页面中仍然未开启
                mRePermissionIndex = 0;
                requestPermissionAgain(mPermissionList.get(mRePermissionIndex).permission);
            } else {// 没有未开启的权限
                onFinish();
            }
        }
    }

    /**
     * 检查当前需要请求的权限是否开启，若在设置页面中开启，移除
     */
    private void checkPermission() {
        ListIterator<PermissonItem> iterator = mPermissionList.listIterator();
        while (iterator.hasNext()) {
            int checkPermission = ContextCompat.checkSelfPermission(getApplicationContext(), iterator.next().permission);
            if (checkPermission == PackageManager.PERMISSION_GRANTED) {
                iterator.remove();
            }
        }
    }

    private void onFinish() {
        if (mCallback != null)
            mCallback.onFinished();
        finish();
    }

    private void onClose() {
        if (mCallback != null)
            mCallback.onClosed();
        finish();
    }

    private void onDeny(String permisson, int position) {
        if (mCallback != null)
            mCallback.onDenied(permisson, position);
    }

    private void onGuarantee(String permisson, int position) {
        if (mCallback != null)
            mCallback.onGuaranteed(permisson, position);
    }

    private PermissonItem getPermissionItem(String permission) {
        for (PermissonItem permissonItem : mPermissionList) {
            if (permissonItem.permission.equals(permission))
                return permissonItem;
        }
        return null;
    }

}
